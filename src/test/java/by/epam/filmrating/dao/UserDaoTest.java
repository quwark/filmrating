package by.epam.filmrating.dao;

import by.epam.filmrating.dao.imp.UserMysqlDao;
import by.epam.filmrating.domain.User;
import by.epam.filmrating.domain.UserRole;
import by.epam.filmrating.domain.UserStatus;
import by.epam.filmrating.exception.DaoException;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created by Yura on 15.06.2016.
 */
public class UserDaoTest {

    private static Connection connection;
    private static User user1 = new User();
    private static User user2 = new User();
    private static User user3 = new User();
    private static User user4 = new User();

    @BeforeClass
    public static void before() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/film_rating", "root", "root");
            connection.setAutoCommit(false);

            user1.setLogin("user1");
            user1.setPassword("user1");
            user1.setEmail("user1@mail.ru");
            user1.setRegistrationDate(new Date());
            user1.setRating(0);
            user1.setStatus(UserStatus.ACTIVE);
            user1.setRole(UserRole.USER);

            user2.setLogin("user2");
            user2.setPassword("user2");
            user2.setEmail("user2@mail.ru");
            user2.setRegistrationDate(new Date());
            user2.setRating(0);
            user2.setStatus(UserStatus.ACTIVE);
            user2.setRole(UserRole.USER);

            user3.setLogin("user3");
            user3.setPassword("user3");
            user3.setEmail("user3@mail.ru");
            user3.setRegistrationDate(new Date());
            user3.setRating(0);
            user3.setStatus(UserStatus.ACTIVE);
            user3.setRole(UserRole.USER);

            user4.setLogin("user4");
            user4.setPassword("user4");
            user4.setEmail("user4@mail.ru");
            user4.setRegistrationDate(new Date());
            user4.setRating(0);
            user4.setStatus(UserStatus.ACTIVE);
            user4.setRole(UserRole.USER);

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @AfterClass
    public static void after() {
        try {
            connection.rollback();
            connection.setAutoCommit(true);
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void createUserTest() {
        UserDao userDao = new UserMysqlDao(connection);
        userDao.create(user1);
        User currentUser = userDao.findById(user1.getId());
        Assert.assertEquals(user1.getId(), currentUser.getId());
    }

    @Test
    public void updateUserTest() {
        UserDao userDao = new UserMysqlDao(connection);
        userDao.create(user2);
        User currentUser = userDao.findById(user2.getId());
        Assert.assertEquals(user2.getId(), currentUser.getId());
        currentUser.setLogin("newLogin");
        userDao.update(currentUser);
        User updatedUser = userDao.findById(currentUser.getId());
        Assert.assertEquals(currentUser.getLogin(), updatedUser.getLogin());
    }

    @Test(expected = DaoException.class)
    public void testInsertDuplicateLogin() {
        UserDao userDao = new UserMysqlDao(connection);
        userDao.create(user3);
        userDao.create(user3);
    }

    @Test
    public void deleteUserTest() {
        UserDao userDao = new UserMysqlDao(connection);
        userDao.create(user4);
        userDao.delete(user4.getId());
        Assert.assertNull(userDao.findById(user4.getId()));
    }
}
