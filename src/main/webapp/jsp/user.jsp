<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 13.06.2016
  Time: 23:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="paging" uri="customTags" %>

<c:set var="loc" value="en_US"/>
<c:if test="${!(empty sessionScope.locale)}">
    <c:set var="loc" value="${sessionScope.locale}"/>
</c:if>
<fmt:setLocale value="${loc}" />
<fmt:setBundle basename="i18n.messages"/>

<fmt:message key="user.title" var="pageTitle" scope="request"/>
<fmt:message key="user.fields.login" var="loginLabel"/>
<fmt:message key="user.fields.email" var="emailLabel"/>
<fmt:message key="user.fields.registration_date" var="regDateLabel"/>
<fmt:message key="user.fields.rating" var="ratingLabel"/>
<fmt:message key="user.fields.status" var="statusLabel"/>
<fmt:message key="user.form.fields.button.label.active" var="statusActive"/>
<fmt:message key="user.fields.status.banned" var="statusBanned"/>
<fmt:message key="user.form.fields.button.label.active" var="buttonActive"/>
<fmt:message key="user.form.fields.button.label.ban" var="buttonBan"/>
<fmt:message key="user.form.fields.button.label.change_rating" var="buttonChangeRating"/>
<c:url value="/controller" var="changeStatusActionURL"/>
<c:url value="/controller" var="changeRatingActionURL"/>
<fmt:message key="site.breadscrumbs.home" var="homeLink"/>
<fmt:message key="site.breadscrumbs.users" var="usersLink"/>
<fmt:message key="page.pagination.previous" var="prevLabel"/>
<fmt:message key="page.pagination.next" var="nextLabel"/>

<jsp:include page="header.jsp"/>

<div class="row">
    <ol class="breadcrumb">
        <li><a href="<c:url value='/controller?command=showIndexPage'/>">${homeLink}</a></li>
        <li><a href="<c:url value='/controller?command=showAllUsers'/>">${usersLink}</a></li>
        <li class="active">${user.login}</li>
    </ol>
</div>

<div class="row">
    <div class="col-sm-offset-2 col-sm-8">
        <h2>${loginLabel}: ${user.login}</h2>
        <p>${emailLabel} ${user.email}</p>
        <p>${regDateLabel} <fmt:formatDate value="${user.registrationDate}" type="both"/></p>
        <c:if test="${not empty sessionScope.user && sessionScope.user.role == 'ADMIN'}">
            <form class="form-inline" action="${changeStatusActionURL}" method="post">
                <c:choose>
                    <c:when test="${user.status == 'ACTIVE'}">
                        <span>${statusActive}</span>
                    </c:when>
                    <c:when test="${user.status == 'BANNED'}">
                        <span>${statusBanned}</span>
                    </c:when>
                </c:choose>
                <input type="hidden" name="userId" value="${user.id}">
                <input type="hidden" name="command" value="changeUserStatus">
                <button type="submit" class="btn btn-warning">
                    <c:choose>
                        <c:when test="${user.status == 'ACTIVE'}">
                            ${buttonBan}
                        </c:when>
                        <c:when test="${user.status == 'BANNED'}">
                            ${buttonActive}
                        </c:when>
                    </c:choose>
                </button>
            </form>
        </c:if>
        <h3>Rating: ${user.rating}</h3>
    </div>
</div>
<div class="row">
    <div class="col-sm-offset-2 col-sm-8">
        <c:if test="${not empty sessionScope.user && sessionScope.user.role == 'ADMIN'}">
            <form class="form-inline" action="${changeRatingActionURL}" method="post">
                <div class="form-group">
                    <label for="userRating">${ratingLabel}</label>
                    <input type="number" value="${user.rating}" class="form-control" min="0" max="4000" id="userRating" name="userRating">
                </div>
                <div class="form-group">
                    <input type="hidden" name="userId" value="${user.id}">
                    <input type="hidden" name="command" value="changeUserRating">
                    <button type="submit" class="btn btn-warning">${buttonChangeRating}</button>
                </div>
            </form>
        </c:if>
    </div>
</div>
<hr/>
<c:if test="${not empty comments}">
    <c:forEach var="comment" items="${comments}">
        <hr/>
        <div class="row">
            <div class="col-sm-offset-2 col-sm-2">
                <div>Film:
                    <a href="<c:url value="/controller?command=showFilmInfo&id=${comment.film.id}"/>">
                ${comment.film.name} (${comment.film.year})
                    </a>
                </div>
                <sub><fmt:formatDate value="${comment.userComment.commentTime}" type="both"/></sub>
            </div>
            <div class="col-sm-6">
                ${comment.userComment.text}
            </div>
        </div>
    </c:forEach>
    <div class="row text-center">
        <c:url value="/controller?command=showUserInfo&id=${user.id}&page=##" var="link"/>
        <paging:paginator uri="${link}" total="${pagination.totalPages}" current="${pagination.currentPage}"
                          maxLinks="${pagination.maxLinks}" enableText="true"
                          prevText="${prevLabel}" nextText="${nextLabel}"/>
    </div>
</c:if>

<jsp:include page="footer.jsp"/>
