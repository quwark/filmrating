<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 11.06.2016
  Time: 15:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<c:set var="loc" value="en_US"/>
<c:if test="${!(empty sessionScope.locale)}">
    <c:set var="loc" value="${sessionScope.locale}"/>
</c:if>
<fmt:setLocale value="${loc}" />
<fmt:setBundle basename="i18n.messages"/>

<fmt:message key="add_film.title" var="pageTitle" scope="request"/>
<fmt:message key="add_film.form.fields.name.label" var="nameLabel"/>
<fmt:message key="add_film.form.fields.description.label" var="descriptionLabel"/>
<fmt:message key="add_film.form.fields.genres.label" var="genresLabel"/>
<fmt:message key="add_film.form.fields.genres.placeholder" var="genresPlaceholder"/>
<fmt:message key="add_film.form.fields.year.label" var="yearLabel"/>
<fmt:message key="add_film.form.fields.producer.label" var="producerLabel"/>
<fmt:message key="add_film.form.fields.producer.placeholder" var="producerPlaceholder"/>
<fmt:message key="add_film.form.fields.actors.label" var="actorsLabel"/>
<fmt:message key="add_film.form.fields.image.label" var="imgLabel"/>
<fmt:message key="add_film.form.fields.actors.placeholder" var="actorsPlaceholder"/>
<fmt:message key="add_film.form.fields.button.submit.label" var="buttonSubmitLabel"/>
<fmt:message key="site.breadscrumbs.add_film" var="addFilmLink"/>
<fmt:message key="site.breadscrumbs.home" var="homeLink"/>
<c:url value="/controller" var="addFilmActionFormURL"/>

<jsp:include page="header.jsp"/>

<div class="row">
    <ol class="breadcrumb">
        <li><a href="<c:url value='/controller?command=showIndexPage'/>">${homeLink}</a></li>
        <li class="active">${addFilmLink}</li>
    </ol>
</div>

<div class="row">
    <div class="col-sm-offset-2">
        <c:if test="${not empty sessionScope.message}">
            <c:choose>
                <c:when test="${sessionScope.message.type == 'success'}">
                    <c:set var="cssClass" value="alert alert-success" scope="request"/>
                </c:when>
                <c:when test="${sessionScope.message.type == 'error'}">
                    <c:set var="cssClass" value="alert alert-danger" scope="request"/>
                </c:when>
            </c:choose>
            <div class="${cssClass}">
                <fmt:message key="${sessionScope.message.message}"/>
                <c:remove var="message" scope="session" />
            </div>
        </c:if>
    </div>
</div>
<div class="row">
    <div class="col-sm-offset-2 col-sm-8">
        <form class="form-horizontal" action="${addFilmActionFormURL}" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">${nameLabel}</label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="name" id="name">
                </div>
                <c:if test="${not empty sessionScope.nameInvalid}">
                    <div class="col-sm-4">
                        <p class="text-error">
                            <fmt:message key="${sessionScope.nameInvalid}"/>
                            <c:remove var="nameInvalid" scope="session" />
                        </p>
                    </div>
                </c:if>
            </div>
            <div class="form-group">
                <label for="description" class="col-sm-2 control-label">${descriptionLabel}</label>
                <div class="col-sm-6">
                    <textarea class="form-control" name="description" id="description" maxlength="2000" rows="8"></textarea>
                </div>
                <c:if test="${not empty sessionScope.descriptionInvalid}">
                    <div class="col-sm-4">
                        <p class="text-error">
                            <fmt:message key="${sessionScope.descriptionInvalid}"/>
                            <c:remove var="descriptionInvalid" scope="session" />
                        </p>
                    </div>
                </c:if>
            </div>
            <div class="form-group">
                <label for="file" class="col-sm-2 control-label">${imgLabel}</label>
                <div class="col-sm-6">
                    <input type="file" name="file" id="file"/>
                </div>
            </div>
            <div class="form-group">
                <label for="genres" class="col-sm-2 control-label">${genresLabel}</label>
                <div class="col-sm-6">
                    <select class="chosen form-control" multiple id="genres" name="genre[]" class="" data-placeholder="${genresPlaceholder}">
                        <c:forEach var="genre" items="${genres}">
                            <option value="${genre.id}">${genre.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <c:if test="${not empty sessionScope.genresInvalid}">
                    <div class="col-sm-4">
                        <p class="text-error">
                            <fmt:message key="${sessionScope.genresInvalid}"/>
                            <c:remove var="genresInvalid" scope="session" />
                        </p>
                    </div>
                </c:if>
            </div>
            <div class="form-group">
                <label for="year" class="col-sm-2 control-label">${yearLabel}</label>
                <div class="col-sm-6">
                    <input type="number" class="form-control" min="${minYearValue}" max="${maxYearValue}" id="year" name="year">
                </div>
                <c:if test="${not empty sessionScope.yearInvalid}">
                    <div class="col-sm-4">
                        <p class="text-error">
                            <fmt:message key="${sessionScope.yearInvalid}"/>
                            <c:remove var="yearInvalid" scope="session" />
                        </p>
                    </div>
                </c:if>
            </div>
            <div class="form-group">
                <label for="producer" class="col-sm-2 control-label">${producerLabel}</label>
                <div class="col-sm-6">
                    <select class="chosen form-control" id="producer" name="producer" data-placeholder="${producerPlaceholder}">
                        <option value=""></option>
                        <c:forEach var= "person" items="${persons}">
                            <option value="${person.id}">${person.firstName} ${person.lastName}</option>
                        </c:forEach>
                    </select>
                </div>
                <c:if test="${not empty sessionScope.producerInvalid}">
                    <div class="col-sm-4">
                        <p class="text-error">
                            <fmt:message key="${sessionScope.producerInvalid}"/>
                            <c:remove var="producerInvalid" scope="session" />
                        </p>
                    </div>
                </c:if>
            </div>
            <div class="form-group">
                <label for="actors" class="col-sm-2 control-label">${actorsLabel}</label>
                <div class="col-sm-6">
                    <select  multiple class="chosen form-control" id="actors" name="actor[]" data-placeholder="${actorsPlaceholder}">
                        <c:forEach var="person" items="${persons}">
                            <option value="${person.id}">${person.firstName} ${person.lastName}</option>
                        </c:forEach>
                    </select>
                </div>
                <c:if test="${not empty sessionScope.actorsInvalid}">
                    <div class="col-sm-4">
                        <p class="text-error">
                            <fmt:message key="${sessionScope.actorsInvalid}"/>
                            <c:remove var="actorsInvalid" scope="session" />
                        </p>
                    </div>
                </c:if>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="hidden" name="command" value="addFilm">
                    <button type="submit" class="btn btn-success">${buttonSubmitLabel}</button>
                </div>
            </div>
        </form>
    </div>

</div>
<jsp:include page="footer.jsp"/>
