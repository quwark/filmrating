<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 19.06.2016
  Time: 17:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="loc" value="en_US"/>
<c:if test="${!(empty sessionScope.locale)}">
    <c:set var="loc" value="${sessionScope.locale}"/>
</c:if>
<fmt:setLocale value="${loc}" />
<fmt:setBundle basename="i18n.messages"/>

<fmt:message key="add_person.title" var="pageTitle" scope="request"/>
<fmt:message key="add_person.form.fields.first_name.label" var="firstNameLabel"/>
<fmt:message key="add_person.form.fields.last_name.label" var="lastNameLabel"/>
<fmt:message key="add_person.form.fields.birth_date.label" var="birthDateLabel"/>
<fmt:message key="add_person.form.fields.button.submit" var="buttonSubmitLabel"/>
<fmt:message key="site.breadscrumbs.edit_person" var="editPersonLink"/>
<fmt:message key="site.breadscrumbs.add_person" var="addPersonLink"/>
<fmt:message key="site.breadscrumbs.home" var="homeLink"/>

<jsp:include page="header.jsp"/>

<div class="row">
    <ol class="breadcrumb">
        <li><a href="<c:url value='/controller?command=showIndexPage'/>">${homeLink}</a></li>
        <c:choose>
            <c:when test="${person.id != null}">
                <li class="active">${editPersonLink}</li>
            </c:when>
            <c:when test="${person.id == null}">
                <li class="active">${addPersonLink}</li>
            </c:when>
        </c:choose>
    </ol>
</div>

<div class="row">
    <div class="col-sm-offset-2">
        <c:if test="${not empty sessionScope.message}">
            <c:choose>
                <c:when test="${sessionScope.message.type == 'success'}">
                    <c:set var="cssClass" value="alert alert-success" scope="request"/>
                </c:when>
                <c:when test="${sessionScope.message.type == 'error'}">
                    <c:set var="cssClass" value="alert alert-danger" scope="request"/>
                </c:when>
            </c:choose>
            <div class="${cssClass}">
                <fmt:message key="${sessionScope.message.message}"/>
                <c:remove var="message" scope="session" />
            </div>
        </c:if>
    </div>
</div>

<div class="row">
    <div>
        <form class="form-horizontal" action="<c:url value="/controller"/>" method="post">
            <div class="form-group">
                <label for="firstName" class="col-sm-2 control-label">${firstNameLabel}</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="firstName" id="firstName" value="${person.firstName}">
                </div>
                <c:if test="${not empty sessionScope.invalidFirstName}">
                    <div class="col-sm-4">
                        <p class="text-error">
                            <fmt:message key="${sessionScope.invalidFirstName}"/>
                            <c:remove var="invalidFirstName" scope="session" />
                        </p>
                    </div>
                </c:if>
            </div>
            <div class="form-group">
                <label for="lastName"  class="col-sm-2 control-label">${lastNameLabel}</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" name="lastName" id="lastName" value="${person.lastName}">
                </div>
                <c:if test="${not empty sessionScope.invalidLastName}">
                    <div class="col-sm-4">
                        <p class="text-error">
                            <fmt:message key="${sessionScope.invalidLastName}"/>
                            <c:remove var="invalidLastName" scope="session" />
                        </p>
                    </div>
                </c:if>
            </div>
            <div class="form-group">
                <label for="birthDate" class="col-sm-2 control-label">${birthDateLabel}</label>
                <div class="col-sm-4">
                    <fmt:formatDate value="${person.birthDate}" pattern="yyyy-MM-dd" var="date"/>
                    <input type="date" class="form-control" name="birthDate" id="birthDate" value="${date}">
                </div>
                <c:if test="${not empty sessionScope.birthDateInvalid}">
                    <div class="col-sm-4">
                        <p class="text-error">
                            <fmt:message key="${sessionScope.birthDateInvalid}"/>
                            <c:remove var="birthDateInvalid" scope="session" />
                        </p>
                    </div>
                </c:if>
            </div>
            <div class="form-group">
                <c:if test="${not empty person.id}">
                    <input type="hidden" name="personId" value="${person.id}">
                </c:if>
                <input type="hidden" name="command" value="savePerson">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success">${buttonSubmitLabel}</button>
                </div>
            </div>
        </form>
    </div>
</div>
<jsp:include page="footer.jsp"/>
