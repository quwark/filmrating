<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 14.06.2016
  Time: 14:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="loc" value="en_US"/>
<c:if test="${!(empty sessionScope.locale)}">
    <c:set var="loc" value="${sessionScope.locale}"/>
</c:if>
<fmt:setLocale value="${loc}" />
<fmt:setBundle basename="i18n.messages"/>

<fmt:message key="edit_film.title" var="title" scope="request"/>
<fmt:message key="edit_film.form.fields.name.label" var="nameLabel"/>
<fmt:message key="edit_film.form.fields.description.label" var="descriptionLabel"/>
<fmt:message key="edit_film.form.fields.genres.label" var="genresLabel"/>
<fmt:message key="edit_film.form.fields.year.label" var="yearLabel"/>
<fmt:message key="edit_film.form.fields.producer.label" var="producerLabel"/>
<fmt:message key="edit_film.form.fields.actors.label" var="actorsLabel"/>
<fmt:message key="edit_film.form.fields.image.label" var="imgLabel"/>
<fmt:message key="edit_film.form.fields.button.submit.label" var="buttonSubmitLabel"/>
<fmt:message key="site.breadscrumbs.edit_film" var="editFilmLink"/>
<fmt:message key="site.breadscrumbs.home" var="homeLink"/>

<c:url value="/controller" var="addFilmActionFormURL"/>

<jsp:include page="header.jsp"/>

<div class="row">
    <ol class="breadcrumb">
        <li><a href="<c:url value='/controller?command=showIndexPage'/>">${homeLink}</a></li>
        <li class="active">${editFilmLink}</li>
    </ol>
</div>

<div class="row">
    <div class="col-sm-offset-2">
        <c:if test="${not empty sessionScope.message}">
            <c:choose>
                <c:when test="${sessionScope.message.type == 'success'}">
                    <c:set var="cssClass" value="alert alert-success" scope="request"/>
                </c:when>
                <c:when test="${sessionScope.message.type == 'error'}">
                    <c:set var="cssClass" value="alert alert-danger" scope="request"/>
                </c:when>
            </c:choose>
            <div class="${cssClass}">
                <fmt:message key="${sessionScope.message.message}"/>
                <c:remove var="message" scope="session" />
            </div>
        </c:if>
    </div>
</div>
<div class="row">
    <div class="col-sm-offset-2 col-sm-10">
        <form class="form-horizontal" action="${addFilmActionFormURL}" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="name" class="col-sm-2 control-label">${nameLabel}</label>
                <div class="col-sm-6">
                    <input type="text" name="name" id="name" class="form-control" value="${film.film.name}">
                </div>
                <c:if test="${not empty sessionScope.nameInvalid}">
                    <div class="col-sm-4">
                        <p class="text-error">
                            <fmt:message key="${sessionScope.nameInvalid}"/>
                            <c:remove var="nameInvalid" scope="session" />
                        </p>
                    </div>
                </c:if>
            </div>
            <div class="form-group">
                <label for="description" class="col-sm-2 control-label">${descriptionLabel}</label>
                <div class="col-sm-6">
                    <textarea name="description" id="description" class="form-control" maxlength="2000" rows="8">${film.film.description}</textarea>
                </div>
                <c:if test="${not empty sessionScope.descriptionInvalid}">
                    <div class="col-sm-4">
                        <p>
                            <fmt:message key="${sessionScope.descriptionInvalid}"/>
                            <c:remove var="descriptionInvalid" scope="session" />
                        </p>
                    </div>
                </c:if>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-1 col-sm-8">
                    <img src="<c:url value='/images/${film.film.imgURL}'/>" class="img-responsive">
                </div>
            </div>
            <div class="form-group">
                <label for="file" class="col-sm-2 control-label">${imgLabel}</label>
                <div class="col-sm-6">
                    <input type="file" name="file" id="file"/>
                </div>
            </div>
            <div class="form-group">
                <label for="genres" class="col-sm-2 control-label">${genresLabel}</label>
                <div class="col-sm-6">
                    <select  multiple id="genres" name="genre[]" class="chosen form-control" >
                        <c:forEach var="genre" items="${genreMap}">
                            <option value="${genre.key.id}" ${genre.value == true ? 'selected' : ''}>${genre.key.name}</option>
                        </c:forEach>
                    </select>
                </div>
                <c:if test="${not empty sessionScope.genresInvalid}">
                    <div class="col-sm-4">
                        <p class="text-error">
                            <fmt:message key="${sessionScope.genresInvalid}"/>
                            <c:remove var="genresInvalid" scope="session" />
                        </p>
                    </div>
                </c:if>
            </div>
            <div class="form-group">
                <label for="year" class="col-sm-2 control-label">${yearLabel}</label>
                <div class="col-sm-6">
                    <input type="number" min="${minYearValue}" max="${maxYearValue}" class="form-control" id="year" name="year" value="${film.film.year}">
                </div>
                <c:if test="${not empty sessionScope.yearInvalid}">
                    <div class="col-sm-4">
                        <p class="text-error">
                            <fmt:message key="${sessionScope.yearInvalid}"/>
                            <c:remove var="yearInvalid" scope="session" />
                        </p>
                    </div>
                </c:if>
            </div>
            <div class="form-group">
                <label for="producer" class="col-sm-2 control-label">${producerLabel}</label>
                <div class="col-sm-6">
                    <select id="producer" name="producer" class="chosen form-control">
                        <c:forEach var="person" items="${personMap}">
                            <option value="${person.key.id}" ${person.key.id == film.producer.id ? 'selected' : ''}>
                                    ${person.key.firstName} ${person.key.lastName}</option>
                        </c:forEach>
                    </select>
                </div>
                <c:if test="${not empty sessionScope.producerInvalid}">
                    <div class="col-sm-4">
                        <p class="text-error">
                            <fmt:message key="${sessionScope.producerInvalid}"/>
                            <c:remove var="producerInvalid" scope="session" />
                        </p>
                    </div>
                </c:if>
            </div>
            <div class="form-group">
                <label for="actors" class="col-sm-2 control-label">${actorsLabel}</label>
                <div class="col-sm-6">
                    <select  multiple id="actors" name="actor[]" class="chosen form-control">
                        <c:forEach var="person" items="${personMap}">
                            <option value="${person.key.id}" ${person.value == true ? 'selected' : ''}>
                                    ${person.key.firstName} ${person.key.lastName}</option>
                        </c:forEach>
                    </select>
                </div>
                <c:if test="${not empty sessionScope.actorsInvalid}">
                    <div class="col-sm-4">
                        <p class="text-error">
                            <fmt:message key="${sessionScope.actorsInvalid}"/>
                            <c:remove var="actorsInvalid" scope="session" />
                        </p>
                    </div>
                </c:if>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="hidden" name="filmId" value="${film.film.id}">
                    <input type="hidden" name="command" value="editFilm">
                    <button type="submit" class="btn btn-success">${buttonSubmitLabel}</button>
                </div>

            </div>
        </form>
    </div>
</div>

<jsp:include page="footer.jsp"/>

