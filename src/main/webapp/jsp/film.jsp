<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 11.06.2016
  Time: 23:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="paging" uri="customTags" %>

<fmt:setBundle basename="i18n.messages"/>
<fmt:message key="page.pagination.previous" var="prevLabel"/>
<fmt:message key="page.pagination.next" var="nextLabel"/>
<fmt:message key="delete_comment.form.fields.button.submit" var="deleteCommentLabel"/>
<fmt:message key="film.info.description.label" var="decriptionLabel"/>
<fmt:message key="film.info.year.label" var="yearLabel"/>
<fmt:message key="film.info.genres.label" var="genresLabel"/>
<fmt:message key="film.info.producer.label" var="producerLabel"/>
<fmt:message key="site.breadscrumbs.films" var="filmsLink"/>
<fmt:message key="site.breadscrumbs.home" var="homeLink"/>
<fmt:message key="film.info.actors.label" var="actorsLabel"/>
<fmt:message key="film.info.rating.label" var="ratingLabel"/>
<fmt:message key="add_comment.form.fields.comment_text.label" var="commentLabel"/>
<fmt:message key="add_comment.form.fields.button.submit" var="buttonAddComment"/>
<fmt:message key="add_mark.form.fields.mark.label" var="userMarkLabel"/>
<fmt:message key="add_mark.form.fields.button.submit" var="buttonAddMark"/>

<jsp:include page="header.jsp"/>

<div class="row">
    <ol class="breadcrumb">
        <li><a href="<c:url value='/controller?command=showIndexPage'/>">${homeLink}</a></li>
        <li><a href="<c:url value='/controller?command=showAllFilms'/>">${filmsLink}</a></li>
        <li class="active">${film.film.name}</li>
    </ol>
</div>

<div class="row">
    <div class="col-sm-offset-2">
        <c:if test="${not empty sessionScope.message}">
            <c:choose>
                <c:when test="${sessionScope.message.type == 'success'}">
                    <c:set var="cssClass" value="alert alert-success" scope="request"/>
                </c:when>
                <c:when test="${sessionScope.message.type == 'error'}">
                    <c:set var="cssClass" value="alert alert-danger" scope="request"/>
                </c:when>
            </c:choose>
            <div class="${cssClass}">
                <fmt:message key="${sessionScope.message.message}"/>
                <c:remove var="message" scope="session"/>
            </div>
        </c:if>
    </div>
</div>

<div class="row">
    <div class="col-sm-offset-2">
        <h2>${film.film.name}</h2>
        <div class="row">
            <div class="col-sm-9">
                <img src="<c:url value='/images/${film.film.imgURL}'/>" alt="" class="img-responsive">
            </div>
        </div>
        <div><b>${decriptionLabel}</b>
            ${film.film.description}
        </div>
        <div>
            <b>${yearLabel} </b>${film.film.year}
        </div>
        <div>
            <b>${genresLabel}</b>
            <c:if test="${not empty film.genres}">
                <c:forEach var="genre" items="${film.genres}" varStatus="loop">
                    <c:url value="/controller?command=showAllFilms&genreId=${genre.id}" var="link"/>
                    <a href="${link}">${genre.name}</a><c:if test="${!loop.last}">, </c:if>
                </c:forEach>
            </c:if>
        </div>
        <div>
            <b>${producerLabel} </b>
            <a href="<c:url value='/controller?command=showPersonInfo&id=${film.producer.id}'/>">
                ${film.producer.firstName} ${film.producer.lastName}
            </a>
        </div>
        <div>
            <b>${actorsLabel}</b>
            <c:if test="${not empty film.actors}">
                <c:forEach var="actor" items="${film.actors}" varStatus="loop">
                    <span>
                        <a href="<c:url value='/controller?command=showPersonInfo&id=${actor.id}'/>">
                            ${actor.firstName} ${actor.lastName}<c:if test="${!loop.last}">, </c:if>
                        </a>
                    </span>
                </c:forEach>
            </c:if>
        </div>
    </div>
</div>

<c:if test="${not empty sessionScope.user &&
                sessionScope.user.role == 'USER'}">
    <div class="row">
        <div class="col-sm-offset-2">
            <div>
                <b>${ratingLabel} </b><span>${film.mark} (${film.userMarkCount})</span>
                <c:if test="${not empty sessionScope.user && not empty isMarkPutAvailable && isMarkPutAvailable == true}">
                    <c:url value="/controller" var="actionURL"/>

                    <form class="form-inline" action="${actionURL}" method="post">
                        <div class="form-group">
                            <label for="mark" class="control-label">${userMarkLabel}</label>
                            <select id="mark" name="userMark" class="form-control">
                                <c:forEach begin="${minMarkValue}" end="${maxMarkValue}" var="mark">
                                    <option value="${mark}">${mark}</option>
                                </c:forEach>
                            </select>
                            <c:if test="${not empty sessionScope.userMarkInvalid}">
                                <sub>
                                    <fmt:message key="${sessionScope.userMarkInvalid}"/>
                                    <c:remove var="userMarkInvalid" scope="session" />
                                </sub>
                            </c:if>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="command" value="addMark">
                            <input type="hidden" name="filmId" value="${film.film.id}">
                            <button type="submit" class="btn btn-success">${buttonAddMark}</button>
                        </div>
                    </form>
                </c:if>
            </div>
        </div>
    </div>
    <div class="row">
        <hr/>
        <div class="col-sm-offset-2">
            <c:url value="/controller" var="actionURL"/>
            <form class="form-horizontal" action="${actionURL}" method="post">
                <div class="form-group">
                    <label for="comment" class="col-sm-2">${commentLabel}</label>
                    <c:if test="${not empty sessionScope.commentTextInvalid}">
                        <div class="col-sm-2">
                            <p class="text-error">
                                <fmt:message key="${sessionScope.commentTextInvalid}"/>
                                <c:remove var="commentTextInvalid" scope="session"/>
                            </p>
                        </div>
                    </c:if>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                            <textarea id="comment" name="commentText" class="form-control col-sm-8" cols="140"
                                      maxlength="4000" rows="12"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10">
                        <input type="hidden" name="command" value="addComment">
                        <input type="hidden" name="filmId" value="${film.film.id}">
                        <button type="submit" class="btn btn-success pull-right">${buttonAddComment}</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</c:if>

<hr/>
<c:if test="${not empty comments}">
    <c:forEach var="comment" items="${comments}">
        <hr/>
        <div class="row">
            <div class="col-sm-offset-2 col-sm-2">
                <div class="row"><a
                        href="<c:url value="/controller?command=showUserInfo&id=${comment.user.id}"/>">${comment.user.login}</a>
                </div>
                <div class="row"><sub><fmt:formatDate value="${comment.userComment.commentTime}" type="both"/></sub>
                </div>
                <c:if test="${not empty sessionScope.user &&
                (sessionScope.user.role == 'ADMIN' || sessionScope.user.id == comment.user.id)}">
                    <div class="row">
                        <br/>
                        <form class="form-inline" action="<c:url value="/controller"/>" method="post">
                            <input type="hidden" name="commentId" value="${comment.userComment.id}">
                            <input type="hidden" name="command" value="deleteUserComment">
                            <div class="form-group">
                                <button type="submit" class="btn btn-danger">${deleteCommentLabel}</button>
                            </div>
                        </form>
                    </div>
                </c:if>
            </div>
            <div class="col-sm-8">
                    ${comment.userComment.text}
            </div>
        </div>
        <hr/>
    </c:forEach>
    <div class="row text-center">
        <c:url value="/controller?command=showFilmInfo&id=${film.film.id}&page=##" var="link"/>
        <paging:paginator uri="${link}" total="${pagination.totalPages}" current="${pagination.currentPage}"
                          maxLinks="${pagination.maxLinks}" enableText="true"
                          prevText="${prevLabel}" nextText="${nextLabel}"/>
    </div>
</c:if>

<jsp:include page="footer.jsp"/>