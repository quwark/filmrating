<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 10.06.2016
  Time: 18:02
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="loc" value="en_US"/>
<c:if test="${!(empty sessionScope.locale)}">
    <c:set var="loc" value="${sessionScope.locale}"/>
</c:if>
<fmt:setLocale value="${loc}" />
<fmt:setBundle basename="i18n.messages"/>

<fmt:message key="register.title" var="pageTitle" scope="request"/>
<fmt:message key="register.form.fields.login.label" var="loginLabel"/>
<fmt:message key="register.form.fields.login.placeholder" var="loginPlaceholder"/>
<fmt:message key="register.form.fields.password.label" var="passwordLabel"/>
<fmt:message key="register.form.fields.repeat_password.label" var="repeatPassword"/>
<fmt:message key="register.form.fields.email.label" var="emailLabel"/>
<fmt:message key="register.form.fields.email.placeholder" var="emailPlaceholder"/>
<fmt:message key="register.form.fields.button.submit.label" var="submitButtonLabel"/>
<fmt:message key="site.breadscrumbs.home" var="homeLink"/>
<fmt:message key="site.breadscrumbs.registration" var="registrationLink"/>
<c:url value="/controller" var="formActionURL"/>

<jsp:include page="header.jsp"/>

<div class="row">
    <ol class="breadcrumb">
        <li><a href="<c:url value='/controller?command=showIndexPage'/>">${homeLink}</a></li>
        <li class="active">${registrationLink}</li>
    </ol>
</div>

<div class="row">
    <div class="col-sm-offset-2">
        <c:if test="${not empty sessionScope.message}">
            <c:choose>
                <c:when test="${sessionScope.message.type == 'success'}">
                    <c:set var="cssClass" value="alert alert-success" scope="request"/>
                </c:when>
                <c:when test="${sessionScope.message.type == 'error'}">
                    <c:set var="cssClass" value="alert alert-danger" scope="request"/>
                </c:when>
            </c:choose>
            <div class="${cssClass}">
                <fmt:message key="${sessionScope.message.message}"/>
                <c:remove var="message" scope="session" />
            </div>
        </c:if>
    </div>
</div>
<div class="row">
    <form class="form-horizontal" action="${formActionURL}" method="post">
        <div class="form-group">
            <label for="login" class="col-sm-2 control-label">${loginLabel}</label>
            <div class="col-sm-4">
                <input type="text" class="form-control" name="login" id="login" placeholder="${loginPlaceholder}"
                       required pattern="^[a-zA-z0-9_]{4,12}$">
            </div>
            <c:if test="${not empty sessionScope.loginInvalid}">
                <div class="col-sm-4">
                    <p class="text-error">
                        <fmt:message key="${sessionScope.loginInvalid}"/>
                        <c:remove var="loginInvalid" scope="session" />
                    </p>
                </div>
            </c:if>
        </div>
        <div class="form-group">
            <label for="password" class="col-sm-2 control-label">${passwordLabel}</label>
            <div class="col-sm-4">
                <input type="password" class="form-control" name="password" id="password"
                       required pattern="^[a-zA-z0-9_]{4,12}">
            </div>
            <c:if test="${not empty sessionScope.passwordInvalid}">
                <div class="col-sm-4">
                    <p class="text-error">
                        <fmt:message key="${sessionScope.passwordInvalid}"/>
                        <c:remove var="passwordInvalid" scope="session" />
                    </p>
                </div>
            </c:if>
        </div>
        <div class="form-group">
            <label for="repeatPassword" class="col-sm-2 control-label">${repeatPassword}</label>
            <div class="col-sm-4">
                <input type="password" class="form-control" name="repeatPassword" id="repeatPassword"
                       required pattern="^[a-zA-z0-9_]{4,12}">
            </div>
            <c:if test="${not empty sessionScope.repeatPasswordInvalid}">
                <div class="col-sm-4">
                    <p class="text-error">
                        <fmt:message key="${sessionScope.repeatPasswordInvalid}"/>
                        <c:remove var="repeatPasswordInvalid" scope="session" />
                    </p>
                </div>
            </c:if>
        </div>
        <div class="form-group">
            <label for="email" class="col-sm-2 control-label">${emailLabel}</label>
            <div class="col-sm-4">
                <input type="email" class="form-control" name="email" id="email" placeholder="${emailPlaceholder}" required>
            </div>
            <c:if test="${not empty sessionScope.emailInvalid}">
                <div class="col-sm-4">
                    <p class="text-error">
                        <fmt:message key="${sessionScope.emailInvalid}"/>
                        <c:remove var="emailInvalid" scope="session" />
                    </p>
                </div>
            </c:if>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="hidden" name="command" value="registerUser">
                <button type="submit" class="btn btn-success">${submitButtonLabel}</button>
            </div>
        </div>
    </form>
</div>
<jsp:include page="footer.jsp"/>
