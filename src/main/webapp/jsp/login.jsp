<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 11.06.2016
  Time: 13:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="loc" value="en_US"/>
<c:if test="${!(empty sessionScope.locale)}">
    <c:set var="loc" value="${sessionScope.locale}"/>
</c:if>
<fmt:setLocale value="${loc}" />
<fmt:setBundle basename="i18n.messages"/>

<fmt:message key="login.title" var="pageTitle" scope="request"/>
<fmt:message key="login.form.fields.login.label" var="loginLabel"/>
<fmt:message key="login.form.fields.password.label" var="passwordLabel"/>
<fmt:message key="login.form.fields.button.submit.label" var="loginButton"/>
<fmt:message key="site.breadscrumbs.home" var="homeLink"/>
<fmt:message key="site.breadscrumbs.login" var="loginLink"/>
<fmt:message key="login.form.heading" var="loginFormHeading"/>

<c:url value="/controller" var="loginFormActionURL"/>
<jsp:include page="header.jsp"/>

<div class="row">
    <ol class="breadcrumb">
        <li><a href="<c:url value='/controller?command=showIndexPage'/>">${homeLink}</a></li>
        <li class="active">${loginLink}</li>
    </ol>
</div>

<div class="row">
    <div class="col-xs-offset-4 col-xs-4">
        <form class="form-signin" action="${loginFormActionURL}" method="post">
            <h1 class="form-signin-heading">${loginFormHeading}</h1>
            <div class="form-group">
                <label for="login">${loginLabel}</label>
                <input type="text" class="form-control" id="login" name="login">
            </div>
            <div class="form-group">
                <label for="password">${passwordLabel}</label>
                <input type="password" class="form-control" id="password" name="password">
            </div>
            <div class="form-group">
                <input type="hidden" name="command" value="login">
                <button type="submit" class="btn btn-success">${loginButton}</button>
            </div>
            <c:if test="${not empty errorLoginPassMessage}">
                <p><fmt:message key="${errorLoginPassMessage}"/></p>
            </c:if>
        </form>
    </div>
</div>
<jsp:include page="footer.jsp"/>