<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 21.06.2016
  Time: 17:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="paging" uri="customTags" %>

<c:set var="loc" value="en_US"/>
<c:if test="${!(empty sessionScope.locale)}">
    <c:set var="loc" value="${sessionScope.locale}"/>
</c:if>
<fmt:setLocale value="${loc}" />
<fmt:setBundle basename="i18n.messages"/>

<fmt:message key="show_films.title" var="pageTitle" scope="request"/>
<fmt:message key="show_films.form.fields.more.button" var="moreButtonLabel"/>
<fmt:message key="show_films.form.fields.edit.button" var="editButtonLabel"/>
<fmt:message key="show_films.form.fields.delete.button" var="deleteButtonLabel"/>
<fmt:message key="change_rating.form.fields.rating.label" var="ratingLabel"/>
<fmt:message key="page.pagination.previous" var="prevLabel"/>
<fmt:message key="page.pagination.next" var="nextLabel"/>
<fmt:message key="site.breadscrumbs.home" var="homeLink"/>
<fmt:message key="site.breadscrumbs.films" var="filmsLink"/>

<jsp:include page="header.jsp"/>
<div class="row">
    <ol class="breadcrumb">
        <li><a href="<c:url value='/controller?command=showIndexPage'/>">${homeLink}</a></li>
        <li class="active">${filmsLink}</li>
    </ol>
</div>
<div class="info">
    <div class="row">
        <div class="col-sm-offset-2">
            <c:if test="${not empty sessionScope.message}">
                <c:choose>
                    <c:when test="${sessionScope.message.type == 'success'}">
                        <c:set var="cssClass" value="alert alert-success" scope="request"/>
                    </c:when>
                    <c:when test="${sessionScope.message.type == 'error'}">
                        <c:set var="cssClass" value="alert alert-danger" scope="request"/>
                    </c:when>
                </c:choose>
                <div class="${cssClass}">
                    <fmt:message key="${sessionScope.message.message}"/>
                    <c:remove var="message" scope="session" />
                </div>
            </c:if>
        </div>
    </div>
    <div class="row">
        <c:if test="${not empty films}">
            <c:if test="${not empty genres}">
                <jsp:include page="sidebar.jsp"/>
            </c:if>
            <div class="col-xs-12 col-sm-10">
                <c:forEach items="${films}" var="film">
                    <div class="item col-xs-12 col-sm-10">
                        <div class="thumbnail">
                            <div class="caption">
                                <h3>
                                    <c:url value="/controller?command=showFilmInfo&id=${film.film.id}" var="link"/>
                                    <a href="${link}">
                                            ${film.film.name} (${film.film.year})
                                    </a>
                                </h3>
                            </div>
                            <img src="<c:url value='/images/${film.film.imgURL}'/>" alt="">
                            <p>${film.film.description}</p>
                                <c:set var="ratingColor" value="yellow" scope="page"/>
                                <c:choose>
                                    <c:when test="${film.mark < 4}">
                                        <c:set var="ratingColor" value="red" scope="page"/>
                                    </c:when>
                                    <c:when test="${film.mark >= 7}">
                                        <c:set var="ratingColor" value="green" scope="page"/>
                                    </c:when>
                                </c:choose>
                                <div class="col-sm-4 text-left">${ratingLabel}: <span class="${ratingColor}">${film.mark} (${film.userMarkCount}</span>)</div>
                                <div class="text-right">
                                    <a href="${link}" class="btn btn-success">${moreButtonLabel}</a>
                                    <c:if test="${not empty sessionScope.user && sessionScope.user.role == 'ADMIN'}">
                                        <c:url value="/controller?command=showEditFilmPage&filmId=${film.film.id}" var="editLink"/>
                                        <a class="btn btn-warning"
                                           href="${editLink}">
                                                ${editButtonLabel}
                                        </a>
                                        <form class="form-inline inline-block">
                                            <input type="hidden" name="filmId" value="${film.film.id}">
                                            <input type="hidden" name="command" value="deleteFilm">
                                            <button type="submit" class="btn btn-danger">${deleteButtonLabel}</button>
                                        </form>
                                    </c:if>
                                </div>
                        </div>
                    </div>
                </c:forEach>
            </div>
            <div class="row text-center">
                <c:choose>
                    <c:when test="${not empty genreId}">
                        <c:url value="/controller?command=showAllFilms&genreId=${genreId}&page=##" var="link"/>
                    </c:when>
                    <c:otherwise>
                        <c:url value="/controller?command=showAllFilms&page=##" var="link"/>

                    </c:otherwise>
                </c:choose>
                <paging:paginator uri="${link}" total="${pagination.totalPages}" current="${pagination.currentPage}"
                                  maxLinks="${pagination.maxLinks}" enableText="true"
                                  prevText="${prevLabel}" nextText="${nextLabel}"/>
            </div>
        </c:if>
    </div>
</div>
<jsp:include page="footer.jsp"/>