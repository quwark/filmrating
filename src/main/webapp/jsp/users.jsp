<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 13.06.2016
  Time: 22:50
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="paging" uri="customTags" %>

<c:set var="loc" value="en_US"/>
<c:if test="${!(empty sessionScope.locale)}">
    <c:set var="loc" value="${sessionScope.locale}"/>
</c:if>
<fmt:setLocale value="${loc}" />
<fmt:setBundle basename="i18n.messages"/>

<fmt:message key="users.title" var="pageTitle" scope="request"/>
<fmt:message key="users.fields.login" var="loginLabel"/>
<fmt:message key="users.fields.email" var="emailLabel"/>
<fmt:message key="users.fields.registration_date" var="regDateLabel"/>
<fmt:message key="users.fields.rating" var="ratingLabel"/>
<fmt:message key="users.fields.actions" var="actionsLabel"/>
<fmt:message key="page.pagination.previous" var="prevLabel"/>
<fmt:message key="page.pagination.next" var="nextLabel"/>
<fmt:message key="site.breadscrumbs.home" var="homeLink"/>
<fmt:message key="site.breadscrumbs.users" var="usersLink"/>

<jsp:include page="header.jsp"/>

<div class="row">
    <ol class="breadcrumb">
        <li><a href="<c:url value='/controller?command=showIndexPage'/>">${homeLink}</a></li>
        <li class="active">${usersLink}</li>
    </ol>
</div>

<div class="row">
    <div class="col-sm-offset-2">
        <c:if test="${not empty sessionScope.message}">
            <c:choose>
                <c:when test="${sessionScope.message.type == 'success'}">
                    <c:set var="cssClass" value="alert alert-success" scope="request"/>
                </c:when>
                <c:when test="${sessionScope.message.type == 'error'}">
                    <c:set var="cssClass" value="alert alert-danger" scope="request"/>
                </c:when>
            </c:choose>
            <div class="${cssClass}">
                <fmt:message key="${sessionScope.message.message}"/>
                <c:remove var="message" scope="session" />
            </div>
        </c:if>
    </div>
</div>

<div class="row">
    <c:if test="${not empty users}">
        <table class="table table-striped">
            <caption>Users</caption>
            <tr>
                <td>${loginLabel}</td>
                <td>${emailLabel}</td>
                <td>${regDateLabel}</td>
                <td>${ratingLabel}</td>
                <c:if test="${not empty sessionScope.user}">
                    <td>${actionsLabel}</td>
                </c:if>
            </tr>
            <c:forEach var="user" items="${users}">
                <c:url value="/controller?command=showUserInfo&id=${user.id}" var="currentUserInfoPage"/>
                <tr>
                    <td><a href="${currentUserInfoPage}">${user.login}</a></td>
                    <td>${user.email}</td>
                    <td><fmt:formatDate value="${user.registrationDate}" type="both"/></td>
                    <td>${user.rating}</td>
                    <c:if test="${not empty sessionScope.user}">
                        <td>
                            <div class="btn-group">
                                <a href="${currentUserInfoPage}" class="btn btn-success">More</a>
                            </div>
                        </td>
                    </c:if>
                </tr>
            </c:forEach>
        </table>
        </div>
        <div class="row text-center">
            <c:url value="/controller?command=showAllUsers&page=##" var="link"/>
            <paging:paginator uri="${link}" total="${pagination.totalPages}" current="${pagination.currentPage}"
                              maxLinks="${pagination.maxLinks}" enableText="true"
                              prevText="${prevLabel}" nextText="${nextLabel}"/>
        </div>
    </c:if>

<jsp:include page="footer.jsp"/>