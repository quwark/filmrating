<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 14.06.2016
  Time: 16:03
  To change this template use File | Settings | File Templates.
--%>
<div class="col-xs-12 col-sm-2 sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <h3 class="title">Genres</h3>
        <c:forEach items="${genres}" var="genre">
            <c:url value="/controller?command=showAllFilms&genreId=${genre.id}" var="link"/>
            <li><a href="${link}">${genre.name}</a></li>
        </c:forEach>
    </ul>
</div>