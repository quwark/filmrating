<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 21.06.2016
  Time: 12:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="loc" value="en_US"/>
<c:if test="${!(empty sessionScope.locale)}">
    <c:set var="loc" value="${sessionScope.locale}"/>
</c:if>
<fmt:setLocale value="${loc}" />
<fmt:setBundle basename="i18n.messages"/>

<fmt:message key="menu.navigation.label" var="navigationLabel"/>
<fmt:message key="menu.item.main" var="homeLink"/>
<fmt:message key="menu.item.films" var="filmsLink"/>
<fmt:message key="menu.item.persons" var="actorsLink"/>
<fmt:message key="menu.item.add_film" var="addFilmLink"/>
<fmt:message key="menu.item.users" var="usersLink"/>
<fmt:message key="menu.item.add_person" var="addPersonLink"/>
<fmt:message key="menu.item.add_person" var="addPersonLink"/>
<fmt:message key="menu.item.top" var="topLink"/>
<fmt:message key="menu.item.top_films" var="topFilmsLink"/>
<fmt:message key="menu.item.top_users" var="topUsersLink"/>
<fmt:message key="menu.item.login" var="loginLink"/>
<fmt:message key="menu.item.register" var="registerLink"/>
<fmt:message key="menu.item.logout" var="logoutLink"/>

<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#responsive-menu">
                <span class="sr-only">${navigationLabel}</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="<c:url value='/controller?command=showAllFilms'/>" class="navbar-brand">Film rating</a>
        </div>
        <div id="responsive-menu" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="<c:url value='/controller?command=showAllFilms'/>">${filmsLink}</a></li>
                <li><a href="<c:url value='/controller?command=showAllPersons'/>">${actorsLink}</a></li>
                <li><a href="<c:url value='/controller?command=showAllUsers'/>">${usersLink}</a></li>
                <c:if test="${not empty sessionScope.user && sessionScope.user.role == 'ADMIN'}">
                    <li><a href="<c:url value='/controller?command=addPersonPage'/>">${addPersonLink}</a></li>
                    <li><a href="<c:url value='/controller?command=addFilmPage'/>">${addFilmLink}</a></li>
                </c:if>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" >${topLink}<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="#">${topFilmsLink}</a></li>
                        <li><a href="#">${topUsersLink}</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <c:if test="${empty sessionScope.user}">
                    <li><a href="<c:url value='/controller?command=loginPage'/>">${loginLink}</a></li>
                    <li><a href="<c:url value='/controller?command=registrationPage'/>">${registerLink}</a></li>
                </c:if>
                <c:if test="${not empty sessionScope.user}">
                    <li><a href="<c:url value='/controller?command=logout'/>">${logoutLink}</a></li>
                </c:if>
                <li><a href="<c:url value='/controller?command=changeLocale&lang=ru_RU'/>">RUS</a></li>
                <li><a href="<c:url value='/controller?command=changeLocale&lang=en_US'/>">ENG</a></li>
            </ul>
        </div>
    </div>
</nav>