<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 28.06.2016
  Time: 10:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="loc" value="en_US"/>
<c:if test="${!(empty sessionScope.locale)}">
    <c:set var="loc" value="${sessionScope.locale}"/>
</c:if>
<fmt:setLocale value="${loc}" />
<fmt:setBundle basename="i18n.messages"/>
<fmt:message key="person.films.header" var="personFilmLabel"/>
<fmt:message key="person.birth_date.label" var="personBithDateLabel"/>
<fmt:message key="site.breadscrumbs.home" var="homeLink"/>
<fmt:message key="site.breadscrumbs.persons" var="personsLink"/>

<jsp:include page="header.jsp"/>

<div class="row">
    <ol class="breadcrumb">
        <li><a href="<c:url value='/controller?command=showIndexPage'/>">${homeLink}</a></li>
        <li><a href="<c:url value='/controller?command=showAllPersons'/>">${personsLink}</a></li>
        <li class="active">${person.firstName} ${person.lastName}</li>
    </ol>
</div>

<div class="row">
    <div class="col-sm-offset-2">
        <div class="row">
            <h1>${person.firstName} ${person.lastName}</h1>
            <p>${personBithDateLabel} <fmt:formatDate value="${person.birthDate}" type="date"/></p>
        </div>
        <div class="row">
            <h3>${personFilmLabel} ${person.firstName} ${person.lastName}</h3>
            <c:if test="${not empty films}">
                <ul>
                    <c:forEach items="${films}" var="film">
                        <c:url value="/controller?command=showFilmInfo&id=${film.id}" var="link"/>
                        <li><a href="${link}">${film.name} ${film.year}</a></li>
                    </c:forEach>
                </ul>
            </c:if>
        </div>
    </div>
</div>

<jsp:include page="footer.jsp"/>