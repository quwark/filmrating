<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 20.06.2016
  Time: 1:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="paging" uri="customTags" %>

<fmt:setBundle basename="i18n.messages"/>

<fmt:message key="persons.title" var="pageTitle" scope="request"/>
<fmt:message key="page.pagination.previous" var="prevLabel"/>
<fmt:message key="page.pagination.next" var="nextLabel"/>
<fmt:message key="persons.form.fields.edit.button" var="editButtonLabel"/>
<fmt:message key="persons.form.fields.delete.button" var="deleteButtonLabel"/>
<fmt:message key="persons.table.caption" var="personsTableCaption"/>
<fmt:message key="persons.first_name.label" var="firstNameLabel"/>
<fmt:message key="persons.last_name.label" var="lastNameLabel"/>
<fmt:message key="persons.birth_date.label" var="birthDateLabel"/>
<fmt:message key="persons.actions.label" var="actionsLabel"/>
<fmt:message key="persons.actions.more.label" var="moreLabel"/>
<fmt:message key="site.breadscrumbs.home" var="homeLink"/>
<fmt:message key="site.breadscrumbs.persons" var="personsLink"/>

<jsp:include page="header.jsp"/>

<div class="row">
    <ol class="breadcrumb">
        <li><a href="<c:url value='/controller?command=showIndexPage'/>">${homeLink}</a></li>
        <li class="active">${personsLink}</li>
    </ol>
</div>

<div class="row">
    <div class="col-sm-offset-2">
        <c:if test="${not empty sessionScope.message}">
            <c:choose>
                <c:when test="${sessionScope.message.type == 'success'}">
                    <c:set var="cssClass" value="alert alert-success" scope="request"/>
                </c:when>
                <c:when test="${sessionScope.message.type == 'error'}">
                    <c:set var="cssClass" value="alert alert-danger" scope="request"/>
                </c:when>
            </c:choose>
            <div class="${cssClass}">
                <fmt:message key="${sessionScope.message.message}"/>
                <c:remove var="message" scope="session" />
            </div>
        </c:if>
    </div>
</div>

<div class="row">
    <table class="table table-striped">
        <caption class="">${personsTableCaption}</caption>
        <tr>
            <td>${firstNameLabel}</td>
            <td>${lastNameLabel}</td>
            <td>${birthDateLabel}</td>
            <td>${actionsLabel}</td>
        </tr>
        <c:forEach var="person" items="${persons}">
            <tr>
                <td>${person.firstName}</td>
                <td>${person.lastName}</td>
                <td><fmt:formatDate value="${person.birthDate}" type="date"/></td>
                <td>
                    <a class="btn btn-success" href="<c:url value='/controller?command=showPersonInfo&id=${person.id}'/>">${moreLabel}</a>
                <c:if test="${not empty sessionScope.user && sessionScope.user.role == 'ADMIN'}">

                        <a href="<c:url value="/controller?command=showEditPersonPage&id=${person.id}"/>"
                           class="btn btn-warning">
                          ${editButtonLabel}
                        </a>
                        <form class="inline-block">
                            <input type="hidden" name="personId" value="${person.id}">
                            <input type="hidden" name="command" value="deletePerson">
                            <button type="submit" class="btn btn-danger">${deleteButtonLabel}</button>
                        </form>
                </c:if>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>
<div class="row text-center">
        <c:url value="/controller?command=showAllPersons&page=##" var="link"/>
        <paging:paginator uri="${link}" total="${pagination.totalPages}" current="${pagination.currentPage}"
                          maxLinks="${pagination.maxLinks}" enableText="true"
                          prevText="${prevLabel}" nextText="${nextLabel}"/>
</div>
