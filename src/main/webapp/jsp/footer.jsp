<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 15.06.2016
  Time: 1:09
  To change this template use File | Settings | File Templates.
--%>

<footer class="navbar-static-bottom row-fluid">
    <div class="navbar-inner">
        <div class="row">
            <p class="text-muted text-center">&copy; 2016 Film Rating, Inc.</p>
        </div>
    </div>
</footer>
</div>
</div>
<script type="text/javascript" src="../js/jquery-1.12.3.min.js"></script>
<script type="text/javascript" src="../chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" src="../js/custom.js"></script>
<script src="../js/bootstrap.js"></script>
<script src="../js/salvattore.js"></script>

</body>
</html>