<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 17.06.2016
  Time: 20:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error</title>
</head>
<body>
    <h1>${messageHeader}</h1>
    <p>${errorMessage}</p>
</body>
</html>
