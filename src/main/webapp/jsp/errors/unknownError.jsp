<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 28.06.2016
  Time: 10:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page isErrorPage="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="i18n.messages"/>
<html>
<head>
    <title><fmt:message key="unknown.error.title"/></title>
</head>
<body bgcolor="white">
<h3>
    <fmt:message key="unknown.error.header"/>
</h3>
<p>
    <fmt:message key="unknown.error.description"/>
</p>
<p>
    : ${pageContext.errorData.throwable.cause}
</p>

</body>
</html>