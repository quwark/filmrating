<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 17.06.2016
  Time: 20:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>404</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="<c:url value="/css/style.css"/>">
</head>


<body>
<div class="wrap">
    <div class="logo">
        <p>Forbidden</p>
        <img src="<c:url value="/image/403.png"/>"/>
        <div class="sub">
            <p><a href="<c:url value="/index.jsp"/>">Home</a></p>
        </div>
    </div>
</div>
</body>