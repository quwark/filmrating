<%--
  Created by IntelliJ IDEA.
  User: Yura
  Date: 14.06.2016
  Time: 7:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="paging" uri="customTags" %>

<jsp:include page="jsp/header.jsp"/>
<c:if test="${not empty genres}">
    <jsp:include page="jsp/sidebar.jsp"/>
</c:if>
<div id="info">
    <div class="col-xs-12 col-sm-10 masonry" data-columns>
        <div class="item">
            <div class="thumbnail">
                <img src="http://placehold.it/600x340" alt="">
                <div class="caption">
                    <h3><a>Название поста</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <button class="btn btn-success">More</button>
                    <button class="btn btn-warning">Edit</button>
                    <button class="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="thumbnail">
                <img src="http://placehold.it/600x340" alt="" class="image-responsive">
                <div class="caption">
                    <h3><a>Название поста</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <a href="" class="btn btn-success">More</a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="thumbnail">
                <img src="http://placehold.it/600x340" alt="" class="image-responsive">
                <div class="caption">
                    <h3><a>Название поста</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <a href="" class="btn btn-success">More</a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="thumbnail">
                <img src="http://placehold.it/600x340" alt="" class="image-responsive">
                <div class="caption">
                    <h3><a>Название поста</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <a href="" class="btn btn-success">More</a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="thumbnail">
                <img src="http://placehold.it/600x340" alt="">
                <div class="caption">
                    <h3><a>Название поста</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <a href="" class="btn btn-success">More</a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="thumbnail">
                <img src="http://placehold.it/600x340" alt="">
                <div class="caption">
                    <h3><a>Название поста</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <a href="" class="btn btn-success">More</a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="thumbnail">
                <img src="http://placehold.it/600x340" alt="">
                <div class="caption">
                    <h3><a>Название поста</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <a href="" class="btn btn-success">More</a>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="thumbnail">
                <img src="http://placehold.it/600x340" alt="">
                <div class="caption">
                    <h3><a>Название поста</a></h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    <a href="" class="btn btn-success">More</a>
                </div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="jsp/footer.jsp"/>