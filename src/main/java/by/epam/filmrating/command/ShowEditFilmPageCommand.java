package by.epam.filmrating.command;

import by.epam.filmrating.domain.Film;
import by.epam.filmrating.domain.Genre;
import by.epam.filmrating.domain.Person;
import by.epam.filmrating.domain.User;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.model.vo.FilmVO;
import by.epam.filmrating.service.FilmService;
import by.epam.filmrating.service.GenreService;
import by.epam.filmrating.service.PersonService;
import by.epam.filmrating.service.impl.FilmServiceImpl;
import by.epam.filmrating.service.impl.GenreServiceImpl;
import by.epam.filmrating.service.impl.PersonServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.RequestParamUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Yura on 14.06.2016.
 */
public class ShowEditFilmPageCommand implements ActionCommand {

    private static final String FILM_ID_PARAM_NAME = "filmId";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String filmId = requestContent.getParameter(FILM_ID_PARAM_NAME);
        Integer filmIdInt = RequestParamUtil.getAsInteger(filmId);

        FilmService filmService = new FilmServiceImpl();
        PersonService personService = new PersonServiceImpl();
        GenreService genreService = new GenreServiceImpl();
        Film currentFilm;

        try {
            if(filmIdInt != null && (currentFilm = filmService.getById(filmIdInt)) != null) {
                FilmVO filmVo = new FilmVO();
                filmVo.setFilm(currentFilm);
                List<Genre> genreList = genreService.getGenresByFilmId(filmIdInt);
                filmVo.setGenres(genreList);
                List<Person> actors = personService.getPersonByFilmAndRole(filmIdInt, 1);
                filmVo.setActors(actors);
                List<Person> producers = personService.getPersonByFilmAndRole(filmIdInt, 2);
                if(producers.size() != 0) {
                    filmVo.setProducer(producers.get(0));
                }

                Map<Person, Boolean> selectedPersonsMap = RequestParamUtil.getSelectedValueMap(personService.getAll(),
                        actors);
                Map<Genre, Boolean> selectedGenresMap = RequestParamUtil.getSelectedValueMap(genreService.getAll(),
                        genreList);

                int minYearValue = RequestParamUtil.getAsIntegerOrDefault(AppConfigManager.getInstance().getProperty(
                        AppConfigManager.FILM_YEAR_MIN_VALUE), 1930);
                int maxYearValue = RequestParamUtil.getAsIntegerOrDefault(AppConfigManager.getInstance().getProperty(
                        AppConfigManager.FILM_YEAR_MAX_VALUE), 2016);
                requestContent.setRequestAttribute("film", filmVo);
                requestContent.setRequestAttribute("personMap", selectedPersonsMap);
                requestContent.setRequestAttribute("genreMap", selectedGenresMap);
                requestContent.setRequestAttribute("minYearValue", minYearValue);
                requestContent.setRequestAttribute("maxYearValue", maxYearValue);
                return AppConfigManager.getInstance().getProperty(AppConfigManager.FILM_EDIT_PATH_PAGE);
            }
            return AppConfigManager.getInstance().getProperty(AppConfigManager.ERROR_PAGE_404);
        } catch (ServiceException e) {
            throw new CommandException("Can't show edit page", e);
        }
    }
}
