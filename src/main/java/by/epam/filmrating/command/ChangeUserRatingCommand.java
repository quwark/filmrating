package by.epam.filmrating.command;

import by.epam.filmrating.domain.User;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.exception.ValidationException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.service.UserService;
import by.epam.filmrating.service.impl.UserServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.RequestParamUtil;

/**
 * Created by Yura on 14.06.2016.
 */
public class ChangeUserRatingCommand implements ActionCommand {

    private static final String USER_ID_PARAM_NAME = "userId";
    private static final String USER_RATING_PARAM_NAME = "userRating";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String page = "/controller?command=showUserInfo&id=";
        String userId = requestContent.getParameter(USER_ID_PARAM_NAME);
        String userRating = requestContent.getParameter(USER_RATING_PARAM_NAME);

        try {
            UserService userService = new UserServiceImpl();
            userService.updateRating(RequestParamUtil.getAsInteger(userId), RequestParamUtil.getAsInteger(userRating));
        } catch (ServiceException e) {
            throw new CommandException("Can't change user rating", e);
        } catch (ValidationException e) {
            for(String key : e.getErrors().keySet()) {
                requestContent.setSessionAttribute(key, e.getErrors().get(key));
            }
        }
        page += userId;
        requestContent.setRedirect(true);
        return page;
    }
}
