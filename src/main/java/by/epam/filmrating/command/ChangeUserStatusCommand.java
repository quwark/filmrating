package by.epam.filmrating.command;

import by.epam.filmrating.domain.UserStatus;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.service.UserService;
import by.epam.filmrating.service.impl.UserServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.RequestParamUtil;

/**
 * Created by Yura on 14.06.2016.
 */
public class ChangeUserStatusCommand implements ActionCommand {

    private static final String USER_ID_PARAM_NAME = "userId";
    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String userId = requestContent.getParameter(USER_ID_PARAM_NAME);
        String page = "/controller?command=showUserInfo&id=";
        try {
            UserService userService = new UserServiceImpl();
            userService.changeUserStatus(RequestParamUtil.getAsInteger(userId), UserStatus.ACTIVE);
            requestContent.setRedirect(true);
            page += userId;
            return page;
        } catch (ServiceException e) {
            throw new CommandException("Can't change user status", e);
        }
    }
}
