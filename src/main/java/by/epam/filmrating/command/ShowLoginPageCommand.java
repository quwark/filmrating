package by.epam.filmrating.command;

import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.servlet.SessionRequestContent;

/**
 * Created by Yura on 11.06.2016.
 */
public class ShowLoginPageCommand implements ActionCommand {
    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        return AppConfigManager.getInstance().getProperty(AppConfigManager.LOGIN_PAGE_PATH);
    }
}
