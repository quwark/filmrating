package by.epam.filmrating.command;

import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.service.FilmService;
import by.epam.filmrating.service.impl.FilmServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.Message;
import by.epam.filmrating.util.RequestParamUtil;

/**
 * Created by Yura on 15.06.2016.
 */
public class DeleteFilmCommand implements ActionCommand {

    private static final String FILM_ID_PARAM_NAME = "filmId";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String filmId = requestContent.getParameter(FILM_ID_PARAM_NAME);
        String page = "/controller?command=showAllFilms";
        try {
            FilmService filmService = new FilmServiceImpl();
            filmService.delete(RequestParamUtil.getAsInteger(filmId));
            requestContent.setSessionAttribute("message", new Message("show_films.message.delete.success", "success"));
            requestContent.setRedirect(true);
            return page;
        } catch (ServiceException e) {
            throw new CommandException("Can't delete film", e);
        }
    }
}
