package by.epam.filmrating.command;

import by.epam.filmrating.domain.User;
import by.epam.filmrating.domain.UserRole;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.service.UserService;
import by.epam.filmrating.service.impl.UserServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.Pagination;
import by.epam.filmrating.util.PaginationBuilder;
import by.epam.filmrating.util.RequestParamUtil;

import java.util.List;

/**
 * Created by Yura on 13.06.2016.
 */
public class ShowAllUsersCommand implements ActionCommand {

    private static final String PAGE_PARAM_NAME = "page";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String page = requestContent.getParameter(PAGE_PARAM_NAME);
        try {
            int pageInt = RequestParamUtil.getAsIntegerOrDefault(page, 1);
            int recordsOnPage = RequestParamUtil.getAsInteger( AppConfigManager.getInstance().getProperty(
                    AppConfigManager.USER_RECORDS_ON_LIST));


            UserService userService = new UserServiceImpl();
            List<User> userList = userService.getByUserRole(UserRole.USER);
            userList.sort((o1, o2) -> -Integer.compare(o1.getRating(), o2.getRating()));
            Pagination<User> pagination = PaginationBuilder.getPaginationObject(userList, pageInt, recordsOnPage);
            requestContent.setRequestAttribute("pagination", pagination);
            requestContent.setRequestAttribute("users", pagination.getRecordsOnPage());
            return AppConfigManager.getInstance().getProperty(AppConfigManager.USERS_PAGE_PATH);
        } catch (ServiceException e) {
            throw new CommandException("Can't show all users", e);
        }
    }
}
