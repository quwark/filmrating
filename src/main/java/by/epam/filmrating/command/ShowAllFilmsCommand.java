package by.epam.filmrating.command;

import by.epam.filmrating.domain.Film;
import by.epam.filmrating.domain.Genre;
import by.epam.filmrating.domain.Person;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.PaginationException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.model.vo.FilmVO;
import by.epam.filmrating.service.FilmService;
import by.epam.filmrating.service.GenreService;
import by.epam.filmrating.service.PersonService;
import by.epam.filmrating.service.UserMarkService;
import by.epam.filmrating.service.impl.FilmServiceImpl;
import by.epam.filmrating.service.impl.GenreServiceImpl;
import by.epam.filmrating.service.impl.PersonServiceImpl;
import by.epam.filmrating.service.impl.UserMarkServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.Pagination;
import by.epam.filmrating.util.PaginationBuilder;
import by.epam.filmrating.util.RequestParamUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yura on 11.06.2016.
 */
public class ShowAllFilmsCommand implements ActionCommand {

    private static final String GENRE_ID_PARAM_NAME = "genreId";
    private static final String PAGE_PARAM_NAME = "page";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String genreId = requestContent.getParameter(GENRE_ID_PARAM_NAME);
        String pageNumber = requestContent.getParameter(PAGE_PARAM_NAME);
        try {
            FilmService filmService = new FilmServiceImpl();
            PersonService personService = new PersonServiceImpl();
            GenreService genreService = new GenreServiceImpl();
            UserMarkService userMarkService = new UserMarkServiceImpl();

            List<Film> films;
            Integer genreIdInt = RequestParamUtil.getAsInteger(genreId);
            if(genreIdInt != null && genreService.getById(genreIdInt) != null) {
                films = filmService.getFilmsByGenreId(genreIdInt);
            } else {
                films = filmService.getAll();
            }

            films.sort((o1, o2) -> -o1.getAddedDate().compareTo(o2.getAddedDate()));

            List<FilmVO> filmVOList = new ArrayList<>(films.size());
            for(Film film : films) {
                FilmVO filmVO = new FilmVO();
                filmVO.setFilm(film);
                List<Person> personList = personService.getPersonByFilmAndRole(film.getId(), 2);
                if(personList.size() != 0) {
                    filmVO.setProducer(personList.get(0));
                }
                personList = personService.getPersonByFilmAndRole(film.getId(), 1);
                filmVO.setActors(personList);
                List<Genre> genres = genreService.getGenresByFilmId(film.getId());
                filmVO.setGenres(genres);
                filmVO.setMark(userMarkService.getAverageMarkByFilmId(film.getId()));
                filmVO.setUserMarkCount(userMarkService.getUserMarksByFilmId(film.getId()).size());
                filmVOList.add(filmVO);
            }
            int pageNumberInt = RequestParamUtil.getAsIntegerOrDefault(pageNumber, 1);
            int countOnPage = RequestParamUtil.getAsInteger(AppConfigManager.getInstance().getProperty(
                    AppConfigManager.FILM_RECORDS_ON_LIST));


            Pagination<FilmVO> pagination = PaginationBuilder.getPaginationObject(filmVOList, pageNumberInt, countOnPage);
            requestContent.setRequestAttribute("films", pagination.getRecordsOnPage());
            requestContent.setRequestAttribute("pagination", pagination);
            if(genreIdInt != null) {
                requestContent.setRequestAttribute("genreId", genreId);
            }
            requestContent.setRequestAttribute("genres", genreService.getAll());
        } catch (ServiceException e) {
            throw new CommandException("Can't show all pages");
        }
        return AppConfigManager.getInstance().getProperty(AppConfigManager.SHOW_ALL_FILMS_PATH_PAGE);
    }
}
