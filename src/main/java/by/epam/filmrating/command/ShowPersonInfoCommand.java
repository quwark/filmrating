package by.epam.filmrating.command;

import by.epam.filmrating.domain.Film;
import by.epam.filmrating.domain.Person;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.service.FilmService;
import by.epam.filmrating.service.PersonService;
import by.epam.filmrating.service.impl.FilmServiceImpl;
import by.epam.filmrating.service.impl.PersonServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.RequestParamUtil;

import java.util.List;

/**
 * Created by Yura on 28.06.2016.
 */
public class ShowPersonInfoCommand implements ActionCommand {

    private static final String PERSON_ID_PARAM_NAME = "id";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String id = requestContent.getParameter(PERSON_ID_PARAM_NAME);

        Person person;
        Integer personId = RequestParamUtil.getAsInteger(id);
        try {
            PersonService personService = new PersonServiceImpl();
            FilmService filmService = new FilmServiceImpl();
            if(personId == null || (person = personService.getById(personId)) == null) {
                return AppConfigManager.getInstance().getProperty(AppConfigManager.ERROR_PAGE_404);
            }
            List<Film> personFilms = filmService.getFilmsByPersonId(personId);
            requestContent.setRequestAttribute("person", person);
            requestContent.setRequestAttribute("films", personFilms);
            return AppConfigManager.getInstance().getProperty(AppConfigManager.SHOW_PERSON_INFO_PATH_PAGE);
        } catch (ServiceException e) {
            throw new CommandException("Cant' show person info", e);
        }
    }
}
