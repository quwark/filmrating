package by.epam.filmrating.command;

import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.FilmAlreadyExistException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.exception.ValidationException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.model.form.FilmForm;
import by.epam.filmrating.service.FilmService;
import by.epam.filmrating.service.impl.FilmServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.Message;
import by.epam.filmrating.util.RequestParamUtil;

import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Yura on 14.06.2016.
 */
public class EditFilmCommand implements ActionCommand {

    private static final String FILM_ID_PARAM_NAME = "filmId";
    private static final String FILM_NAME_PARAM_NAME = "name";
    private static final String DESCRIPTION_PARAM_NAME = "description";
    private static final String GENRES_PARAM_NAME = "genre[]";
    private static final String YEAR_PARAM_NAME = "year";
    private static final String PRODUCER_PARAM_NAME = "producer";
    private static final String ACTOR_PARAM_NAME = "actor[]";
    private static final String FILE_PARAM_NAME = "file";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String page = "/controller?command=showEditFilmPage&filmId=";
        String filmId = requestContent.getParameter(FILM_ID_PARAM_NAME);
        String name = requestContent.getParameter(FILM_NAME_PARAM_NAME);
        String description = requestContent.getParameter(DESCRIPTION_PARAM_NAME);
        String[] genres = requestContent.getParameterValues(GENRES_PARAM_NAME);
        String year = requestContent.getParameter(YEAR_PARAM_NAME);
        String producer = requestContent.getParameter(PRODUCER_PARAM_NAME);
        String[] actors = requestContent.getParameterValues(ACTOR_PARAM_NAME);
        Part part = requestContent.getPart(FILE_PARAM_NAME);
        String fileName = null;
        if(part.getSize() != 0) {
            fileName = saveImageFile(part);
        }

        FilmForm filmForm = new FilmForm();
        filmForm.setId(RequestParamUtil.getAsInteger(filmId));
        filmForm.setName(RequestParamUtil.getAsString(name));
        filmForm.setDescription(RequestParamUtil.getAsString(description));
        filmForm.setGenreIds(RequestParamUtil.getAsIntegerList(genres, true));
        filmForm.setProducerId(RequestParamUtil.getAsInteger(producer));
        filmForm.setYear(RequestParamUtil.getAsInteger(year));
        filmForm.setActorIds(RequestParamUtil.getAsIntegerList(actors, true));
        filmForm.setImgURL(fileName);

        try {
            FilmService filmService = new FilmServiceImpl();
            filmService.update(filmForm);

            requestContent.setSessionAttribute("message", new Message("edit_film.message.success", "success"));
        } catch (ServiceException e) {
           throw new CommandException("Can't edit film", e);
        } catch (ValidationException e) {
            for(String key : e.getErrors().keySet()) {
                requestContent.setSessionAttribute(key, e.getErrors().get(key));
            }
            requestContent.setSessionAttribute("message", new Message("edit_film.message.fail", "error"));
        } catch (FilmAlreadyExistException e) {
            requestContent.setSessionAttribute("message", new Message("edit_film.message.film.exist", "error"));
        }
        requestContent.setRedirect(true);
        page += filmId;
        return page;
    }

    private String saveImageFile(Part part) throws CommandException {
        String defaultPath = AppConfigManager.getInstance().getProperty(AppConfigManager.DEFAULT_IMAGE_PATH);
        String filName = defaultPath;

        if(part != null) {
            if(part.getContentType().equalsIgnoreCase("image/jpeg") ||
                    part.getContentType().equalsIgnoreCase("image/jpg") ||
                    part.getContentType().equalsIgnoreCase("image/png") ||
                    part.getContentType().equalsIgnoreCase("image/gif") ||
                    part.getContentType().equalsIgnoreCase("image/bmp")) {

                String path = AppConfigManager.getInstance().getProperty(AppConfigManager.UPLOADS_DIR_PATH);
                String type = "JPEG";
                String typeVal = ".jpeg";
                if(part.getContentType().equalsIgnoreCase("image/jpeg")) {
                    type = "JPEG";
                    typeVal = ".jpeg";
                } else if(part.getContentType().equalsIgnoreCase("image/jpg")) {
                    type = "JPG";
                    typeVal = ".jpg";
                } else if(part.getContentType().equalsIgnoreCase("image/png")) {
                    type = "PNG";
                    typeVal = ".png";
                } else if(part.getContentType().equalsIgnoreCase("image/bmp")) {
                    type = "BMP";
                    typeVal = ".bmp";
                } else if(part.getContentType().equalsIgnoreCase("image/gif")) {
                    type = "GIF";
                    typeVal = ".gif";
                }

                InputStream inputStream = null;
                try {
                    DateFormat dateFormat = new SimpleDateFormat("yyMMdd_HHmmss");
                    String dTime = dateFormat.format(new Date());
                    File outputFile = new File(path + "/images/" + dTime + typeVal);
                    filName = "images/" + dTime + typeVal;

                    try(InputStream input = part.getInputStream()){
                        Files.copy(input, outputFile.toPath());
                    }
                } catch (IOException e) {
                    throw new CommandException("Can't create image file", e);
                }
            }
        }
        return filName;
    }
}
