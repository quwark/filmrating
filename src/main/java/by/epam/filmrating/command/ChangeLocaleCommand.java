package by.epam.filmrating.command;

import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.RequestParamUtil;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.Locale;

/**
 * Created by Yura on 24.06.2016.
 */
public class ChangeLocaleCommand implements ActionCommand {

    private static final String LOCALE_PARAM_NAME = "lang";
    private static final String LOCALE_SESSION_ATTR_NAME = "locale";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String locale = requestContent.getParameter(LOCALE_PARAM_NAME);

        String localeName = RequestParamUtil.getAsString(locale);
        if(localeName != null) {
            String[] param = localeName.split("_");
            Locale loc = new Locale(param[0], param[1]);
            requestContent.setSessionAttribute(LOCALE_SESSION_ATTR_NAME, loc);
        }
        return AppConfigManager.getInstance().getProperty(AppConfigManager.INDEX_PATH_PAGE);
    }
}
