package by.epam.filmrating.command;

import by.epam.filmrating.domain.User;
import by.epam.filmrating.domain.UserMark;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.exception.ValidationException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.model.form.UserMarkForm;
import by.epam.filmrating.service.UserMarkService;
import by.epam.filmrating.service.UserService;
import by.epam.filmrating.service.impl.UserMarkServiceImpl;
import by.epam.filmrating.service.impl.UserServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.RequestParamUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yura on 13.06.2016.
 */
public class AddUserMarkCommand implements ActionCommand {

    private static final String FILM_ID_PARAM_NAME = "filmId";
    private static final String USER_MARK_PARAM_NAME = "userMark";
    private static final String USER_SESSION_ATTR_NAME = "user";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String page = "/controller?command=showFilmInfo&id=";
        String filmId = requestContent.getParameter(FILM_ID_PARAM_NAME);
        String mark = requestContent.getParameter(USER_MARK_PARAM_NAME);
        User user = (User) requestContent.getSessionAttribute(USER_SESSION_ATTR_NAME);

        if(user == null) {
            return AppConfigManager.getInstance().getProperty(AppConfigManager.ERROR_PAGE_401);
        }

        UserMarkForm userMarkForm = new UserMarkForm();
        userMarkForm.setFilmId(RequestParamUtil.getAsInteger(filmId));
        userMarkForm.setUserId(user.getId());
        userMarkForm.setUserMark(RequestParamUtil.getAsInteger(mark));

        int minMarkCount = RequestParamUtil.getAsIntegerOrDefault(AppConfigManager.getInstance().getProperty(
                AppConfigManager.USER_MARK_CALCULATING_MIN_COUNT), 3);
        int markStep = RequestParamUtil.getAsIntegerOrDefault(AppConfigManager.getInstance().getProperty(
                AppConfigManager.USER_MARK_CALCULATING_STEP), 1);

        try {
            UserMarkService userMarkService = new UserMarkServiceImpl();
            userMarkService.add(userMarkForm, minMarkCount, markStep);
        } catch (ServiceException e) {
            throw new CommandException("Can't add user mark", e);
        } catch (ValidationException e) {
            for(String key : e.getErrors().keySet()) {
                requestContent.setSessionAttribute(key, e.getErrors().get(key));
            }
        }
        requestContent.setRedirect(true);
        page += filmId;
        return page;
    }
}
