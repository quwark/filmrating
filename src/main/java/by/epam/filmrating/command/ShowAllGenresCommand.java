package by.epam.filmrating.command;

import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.service.GenreService;
import by.epam.filmrating.service.impl.GenreServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;

/**
 * Created by Yura on 15.06.2016.
 */
public class ShowAllGenresCommand implements ActionCommand {

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        try {
            GenreService genreService = new GenreServiceImpl();
            requestContent.setRequestAttribute("genres", genreService.getAll());
            return AppConfigManager.getInstance().getProperty(AppConfigManager.SHOW_ALL_GENRES_PATH_PAGE);
        } catch (ServiceException e) {
            throw new CommandException("Can't get all genres", e);
        }
    }
}
