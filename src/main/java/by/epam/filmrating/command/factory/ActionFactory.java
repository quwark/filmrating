package by.epam.filmrating.command.factory;

import by.epam.filmrating.command.*;
import by.epam.filmrating.servlet.SessionRequestContent;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yura on 10.06.2016.
 */
public class ActionFactory {

    private static final String PARAM_NAME_COMMAND = "command";
    private Map<String, ActionCommand> commands;

    private ActionFactory() {
        commands = new HashMap<>();
        commands.put("registerUser", new RegisterUserCommand());
        commands.put("login", new LoginCommand());
        commands.put("logout", new LogoutCommand());
        commands.put("loginPage", new ShowLoginPageCommand());
        commands.put("registrationPage", new ShowRegistrationPageCommand());
        commands.put("addFilmPage", new ShowFilmAddPageCommand());
        commands.put("addFilm", new AddFilmCommand());
        commands.put("showAllFilms", new ShowAllFilmsCommand());
        commands.put("showFilmInfo", new ShowFilmInfoCommand());
        commands.put("addComment", new AddUserCommentCommand());
        commands.put("addMark", new AddUserMarkCommand());
        commands.put("showAllUsers", new ShowAllUsersCommand());
        commands.put("showUserInfo", new ShowUserInfoCommand());
        commands.put("changeUserStatus", new ChangeUserStatusCommand());
        commands.put("changeUserRating", new ChangeUserRatingCommand());
        commands.put("showEditFilmPage", new ShowEditFilmPageCommand());
        commands.put("editFilm", new EditFilmCommand());
        commands.put("deleteFilm", new DeleteFilmCommand());
        commands.put("deleteUserComment", new DeleteUserCommentCommand());
        commands.put("showAllGenres", new ShowAllGenresCommand());
        commands.put("addPersonPage", new ShowAddPersonPageCommand());
        commands.put("addPerson", new AddPersonCommand());
        commands.put("savePerson", new EditPersonCommand());
        commands.put("showEditPersonPage", new ShowPersonEditPageCommand());
        commands.put("showAddPersonPage", new ShowAddPersonPageCommand());
        commands.put("showAllPersons", new ShowAllPersonsCommand());
        commands.put("deletePerson", new DeletePersonCommand());
        commands.put("showIndexPage", new ShowIndexPageCommand());
        commands.put("changeLocale", new ChangeLocaleCommand());
        commands.put("showPersonInfo", new ShowPersonInfoCommand());
    }

    public ActionCommand defineCommand(SessionRequestContent requestContent) {
        String action = requestContent.getParameter(PARAM_NAME_COMMAND);
        ActionCommand current = commands.get(action);

        if(current == null) {
            current = new NoCommand();
        }
        return current;
    }

    public static ActionFactory getInstance() {
        return ActionFactorySingleton.INSTANCE;
    }

    private static final class ActionFactorySingleton {
        private static final ActionFactory INSTANCE = new ActionFactory();
    }
}
