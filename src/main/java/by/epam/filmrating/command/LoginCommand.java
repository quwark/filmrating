package by.epam.filmrating.command;

import by.epam.filmrating.domain.User;
import by.epam.filmrating.domain.UserStatus;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.service.UserService;
import by.epam.filmrating.service.impl.UserServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;

/**
 * Created by Yura on 11.06.2016.
 */
public class LoginCommand implements ActionCommand {

    private static final String LOGIN_PARAM_NAME = "login";
    private static final String PASSWORD_PARAM_NAME = "password";
    private static final String USER_SESSION_ATTR_NAME = "user";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String page = AppConfigManager.getInstance().getProperty(AppConfigManager.LOGIN_PAGE_PATH);
        String login = requestContent.getParameter(LOGIN_PARAM_NAME);
        String password = requestContent.getParameter(PASSWORD_PARAM_NAME);

        try {
            String salt = AppConfigManager.getInstance().getProperty(AppConfigManager.PASSWORD_SALT);
            UserService userService = new UserServiceImpl();
            if(userService.checkUser(login, password, salt)) {
                User user = userService.getByLogin(login);
                if(user.getStatus() == UserStatus.ACTIVE) {
                    requestContent.setSessionAttribute(USER_SESSION_ATTR_NAME, user);
                    requestContent.setRequestAttribute("user", user); //??????????
                    page = AppConfigManager.getInstance().getProperty(AppConfigManager.INDEX_PATH_PAGE); //????
                } else if(user.getStatus() == UserStatus.BANNED){
                    requestContent.setRequestAttribute("errorLoginPassMessage", "login.form.banned");
                }
            } else {
                requestContent.setRequestAttribute("errorLoginPassMessage", "login.form.failed");
            }
        } catch (ServiceException e) {
            throw new CommandException("Login error", e);
        }

        return page;
    }
}
