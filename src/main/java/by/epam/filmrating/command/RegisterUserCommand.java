package by.epam.filmrating.command;

import by.epam.filmrating.exception.*;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.model.form.UserForm;
import by.epam.filmrating.service.UserService;
import by.epam.filmrating.service.impl.UserServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.Message;
import by.epam.filmrating.util.RequestParamUtil;

/**
 * Created by Yura on 10.06.2016.
 */
public class RegisterUserCommand implements ActionCommand {

    private static final String LOGIN_PARAM_NAME = "login";
    private static final String PASSWORD_PARAM_NAME = "password";
    private static final String REPEAT_PASSWORD_PARAM_NAME = "repeatPassword";
    private static final String EMAIL_PARAM_NAME = "email";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String login = requestContent.getParameter(LOGIN_PARAM_NAME);
        String password = requestContent.getParameter(PASSWORD_PARAM_NAME);
        String repeatPassword = requestContent.getParameter(REPEAT_PASSWORD_PARAM_NAME);
        String email = requestContent.getParameter(EMAIL_PARAM_NAME);

        UserForm registerForm = new UserForm();
        registerForm.setLogin(RequestParamUtil.getAsString(login));
        registerForm.setPassword(RequestParamUtil.getAsString(password));
        registerForm.setRepeatPassword(RequestParamUtil.getAsString(repeatPassword));
        registerForm.setEmail(RequestParamUtil.getAsString(email));

        Message message = new Message("register.message.fail", "error");
        try {
            String salt = AppConfigManager.getInstance().getProperty(AppConfigManager.PASSWORD_SALT);
            UserService userService = new UserServiceImpl();
            userService.registerUser(registerForm, salt);
            message = new Message("register.message.success", "success");
        } catch (ServiceException e) {
            throw new CommandException("Can't register user", e);
        } catch (EmailAlreadyExistException e) {
            requestContent.setSessionAttribute("emailInvalid", "register.form.fields.email.exist");
        } catch (LoginAlreadyExistException e) {
            requestContent.setSessionAttribute("loginInvalid", "register.form.fields.login.exist");
        } catch (ValidationException e) {
            for(String key : e.getErrors().keySet()) {
                requestContent.setSessionAttribute(key, e.getErrors().get(key));
            }
        }
        requestContent.setRedirect(true);
        requestContent.setSessionAttribute("message", message);
        return AppConfigManager.getInstance().getProperty(AppConfigManager.REGISTRATION_PAGE_PATH);
    }
}
