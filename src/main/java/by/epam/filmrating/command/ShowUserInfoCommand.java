package by.epam.filmrating.command;

import by.epam.filmrating.domain.User;
import by.epam.filmrating.domain.UserComment;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.model.vo.CommentVO;
import by.epam.filmrating.service.FilmService;
import by.epam.filmrating.service.UserCommentService;
import by.epam.filmrating.service.UserService;
import by.epam.filmrating.service.impl.FilmServiceImpl;
import by.epam.filmrating.service.impl.UserCommentServiceImpl;
import by.epam.filmrating.service.impl.UserServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.Pagination;
import by.epam.filmrating.util.PaginationBuilder;
import by.epam.filmrating.util.RequestParamUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yura on 13.06.2016.
 */
public class ShowUserInfoCommand implements ActionCommand {

    private static final String USER_ID_PARAM_NAME = "id";
    private static final String PAGE_PARAM_NAME = "page";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String id = requestContent.getParameter(USER_ID_PARAM_NAME);
        String page = requestContent.getParameter(PAGE_PARAM_NAME);
        Integer userId = RequestParamUtil.getAsInteger(id);
        Integer pageNumber = RequestParamUtil.getAsIntegerOrDefault(page, 1);
        Integer countOnList = RequestParamUtil.getAsInteger(AppConfigManager.getInstance().getProperty(
                AppConfigManager.USERS_COMMENTS_RECORDS_ON_LIST));
        try {
            UserService userService = new UserServiceImpl();
            UserCommentService userCommentService = new UserCommentServiceImpl();
            FilmService filmService = new FilmServiceImpl();
            User user;
            if(userId != null && (user = userService.getById(userId)) != null) {
                List<UserComment> comments = userCommentService.getUserCommentsByUserId(userId);
                List<CommentVO> commentVOList = new ArrayList<>(comments.size());
                for(UserComment comment : comments) {
                    CommentVO commentVO = new CommentVO();
                    commentVO.setUserComment(comment);
                    commentVO.setFilm(filmService.getById(comment.getFilmId()));
                    commentVOList.add(commentVO);
                }
                Pagination<CommentVO> pagination = PaginationBuilder.getPaginationObject(commentVOList, pageNumber,
                        countOnList);
                requestContent.setRequestAttribute("pagination", pagination);
                requestContent.setRequestAttribute("comments", pagination.getRecordsOnPage());
                requestContent.setRequestAttribute("user", user);
                return AppConfigManager.getInstance().getProperty(AppConfigManager.USER_PAGE_PATH);
            }
            return AppConfigManager.getInstance().getProperty(AppConfigManager.ERROR_PAGE_404);
        } catch (ServiceException e) {
            throw new CommandException("Can't get user information");
        }
    }
}
