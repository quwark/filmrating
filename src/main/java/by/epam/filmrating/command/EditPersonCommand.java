package by.epam.filmrating.command;

import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.PersonAlreadyExistException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.exception.ValidationException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.model.form.PersonForm;
import by.epam.filmrating.service.PersonService;
import by.epam.filmrating.service.impl.PersonServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.Message;
import by.epam.filmrating.util.RequestParamUtil;

/**
 * Created by Yura on 19.06.2016.
 */
public class EditPersonCommand implements ActionCommand {

    private static final String PERSON_ID_PARAM_NAME = "personId";
    private static final String FIRST_NAME_PARAM_NAME = "firstName";
    private static final String LAST_NAME_PARAM_NAME = "lastName";
    private static final String BIRTH_DATE_PARAM_NAME = "birthDate";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String personId = requestContent.getParameter(PERSON_ID_PARAM_NAME);
        String firstName = requestContent.getParameter(FIRST_NAME_PARAM_NAME);
        String lastName = requestContent.getParameter(LAST_NAME_PARAM_NAME);
        String date = requestContent.getParameter(BIRTH_DATE_PARAM_NAME);

        if(RequestParamUtil.checkNull(firstName, lastName, date)) {
            return AppConfigManager.getInstance().getProperty(AppConfigManager.ERROR_PAGE_400);
        }

        PersonForm personForm = new PersonForm();
        personForm.setId(RequestParamUtil.getAsInteger(personId));
        personForm.setFirstName(RequestParamUtil.getAsString(firstName));
        personForm.setLastName(RequestParamUtil.getAsString(lastName));
        personForm.setBirthDate(RequestParamUtil.getAsDate(date, "yyyy-MM-dd"));

        PersonService personService = new PersonServiceImpl();
        try {
            personService.update(personForm);
            requestContent.setSessionAttribute("message", new Message("add_person.message.success", "success"));
            if(personForm.getId() == null) {
                return "/controller?command=showAllPersons";
            } else {
                requestContent.setRedirect(true);
                return "/controller?command=showEditPersonPage&id=" + personForm.getId();
            }
        } catch (ServiceException e) {
            throw new CommandException("Can't create person", e);
        } catch (ValidationException e) {
            for(String key : e.getErrors().keySet()) {
                requestContent.setSessionAttribute(key, e.getErrors().get(key));
            }
            requestContent.setSessionAttribute("message", new Message("add_person.message.fail", "success"));
        } catch (PersonAlreadyExistException e) {
            requestContent.setSessionAttribute("message", new Message("add_person.message.person_exist", "error"));
        }

        requestContent.setRedirect(true);
        if(personForm.getId() == null) {
            return "/controller?command=showAddPersonPage";
        } else {
            return "/controller?command=showEditPersonPage&id=" + personForm.getId();
        }
    }
}
