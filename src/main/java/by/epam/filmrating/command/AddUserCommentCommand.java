package by.epam.filmrating.command;

import by.epam.filmrating.domain.User;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.exception.ValidationException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.model.form.CommentForm;
import by.epam.filmrating.service.UserCommentService;
import by.epam.filmrating.service.impl.UserCommentServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.RequestParamUtil;

/**
 * Created by Yura on 12.06.2016.
 */
public class AddUserCommentCommand implements ActionCommand {

    private static final String COMMENT_TEXT_PARAM_NAME = "commentText";
    private static final String FILM_ID_PARAM_NAME = "filmId";
    private static final String USER_SESSION_ATTR_NAME = "user";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String page = "/controller?command=showFilmInfo&id=";
        String filmId = requestContent.getParameter(FILM_ID_PARAM_NAME);
        String text = requestContent.getParameter(COMMENT_TEXT_PARAM_NAME);
        User user = (User) requestContent.getSessionAttribute(USER_SESSION_ATTR_NAME);

        if(user == null) {
            return AppConfigManager.getInstance().getProperty(AppConfigManager.ERROR_PAGE_401);
        }

        CommentForm commentForm = new CommentForm();
        commentForm.setUserId(user.getId());
        commentForm.setFilmId(RequestParamUtil.getAsInteger(filmId));
        commentForm.setText(RequestParamUtil.getAsString(text));

        try {
            UserCommentService userCommentService = new UserCommentServiceImpl();
            userCommentService.add(commentForm);
        } catch (ServiceException e) {
            throw new CommandException("Can't add user comment", e);
        } catch (ValidationException e) {
            for(String key : e.getErrors().keySet()) {
                requestContent.setSessionAttribute(key, e.getErrors().get(key));
            }
        }
        requestContent.setRedirect(true);
        page += filmId;
        return page;
    }
}
