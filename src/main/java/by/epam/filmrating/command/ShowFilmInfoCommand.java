package by.epam.filmrating.command;

import by.epam.filmrating.domain.*;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.model.vo.CommentVO;
import by.epam.filmrating.model.vo.FilmVO;
import by.epam.filmrating.service.*;
import by.epam.filmrating.service.impl.*;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.Pagination;
import by.epam.filmrating.util.PaginationBuilder;
import by.epam.filmrating.util.RequestParamUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yura on 11.06.2016.
 */
public class ShowFilmInfoCommand implements ActionCommand {

    private static final String FILM_ID_PARAM_NAME = "id";
    private static final String PAGE_PARAM_NAME = "page";
    private static final String USER_SESSION_ATTR_NAME = "user";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String id = requestContent.getParameter(FILM_ID_PARAM_NAME);
        String page = requestContent.getParameter(PAGE_PARAM_NAME);

        FilmService filmService = new FilmServiceImpl();
        PersonService personService = new PersonServiceImpl();
        GenreService genreService = new GenreServiceImpl();
        UserCommentService userCommentService = new UserCommentServiceImpl();
        UserService userService = new UserServiceImpl();
        UserMarkService userMarkService = new UserMarkServiceImpl();

        Integer filmId = RequestParamUtil.getAsInteger(id);
        Integer pageNumber = RequestParamUtil.getAsIntegerOrDefault(page, 1);
        Film film;

        try {
            if(filmId != null && (film = filmService.getById(filmId)) != null) {
                FilmVO filmVO = new FilmVO();
                filmVO.setFilm(film);
                List<Person> personList = personService.getPersonByFilmAndRole(film.getId(), 2);
                if(personList.size() != 0) {
                    filmVO.setProducer(personList.get(0));
                }
                personList = personService.getPersonByFilmAndRole(film.getId(), 1);
                filmVO.setActors(personList);

                List<Genre> genres = genreService.getGenresByFilmId(film.getId());
                filmVO.setGenres(genres);

                List<UserComment> userComments = userCommentService.getUserCommentsByFilmId(filmId);
                List<CommentVO> commentVOList = new ArrayList<>(userComments.size());
                for(UserComment userComment : userComments) {
                    CommentVO commentVO = new CommentVO();
                    commentVO.setUserComment(userComment);
                    User user = userService.getById(userComment.getUserId());
                    commentVO.setUser(user);
                    commentVOList.add(commentVO);
                }

                List<UserMark> userMarkList = userMarkService.getUserMarksByFilmId(filmId);
                float filmMark = userMarkService.getAverageMarkByFilmId(filmId);
                filmVO.setUserMarkCount(userMarkList.size());
                filmVO.setMark(filmMark);

                boolean isAvailableToPutMark = false;
                User currentUser = (User) requestContent.getSessionAttribute(USER_SESSION_ATTR_NAME);
                if(currentUser != null) {
                    isAvailableToPutMark = userMarkService.isUserAvailableToPutMark(filmId, currentUser.getId());
                }
                commentVOList.sort((o1, o2) -> -o1.getUserComment().getCommentTime().
                        compareTo(o2.getUserComment().getCommentTime()));

                int commentsOnPage = RequestParamUtil.getAsInteger(AppConfigManager.getInstance().getProperty(
                        AppConfigManager.USER_COMMENTS_RECORDS_ON_LIST));
                Pagination<CommentVO> pagination = PaginationBuilder.getPaginationObject(commentVOList, pageNumber,
                        commentsOnPage);
                int minMarkValue = RequestParamUtil.getAsIntegerOrDefault(AppConfigManager.getInstance().getProperty(
                        AppConfigManager.MARK_MIN_VALUE), 1);
                int maxMarkValue = RequestParamUtil.getAsIntegerOrDefault(AppConfigManager.getInstance().getProperty(
                        AppConfigManager.MARK_MAX_VALUE), 10);
                requestContent.setRequestAttribute("film", filmVO);
                requestContent.setRequestAttribute("pagination", pagination);
                requestContent.setRequestAttribute("comments", pagination.getRecordsOnPage());
                requestContent.setRequestAttribute("isMarkPutAvailable", isAvailableToPutMark);
                requestContent.setRequestAttribute("minMarkValue", minMarkValue);
                requestContent.setRequestAttribute("maxMarkValue", maxMarkValue);
                return AppConfigManager.getInstance().getProperty(AppConfigManager.SHOW_FILM_INFO_PATH_PAGE);
            }
            return AppConfigManager.getInstance().getProperty(AppConfigManager.ERROR_PAGE_404);
        } catch (ServiceException e) {
            throw new CommandException("Can't show film info", e);
        }
    }
}
