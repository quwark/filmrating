package by.epam.filmrating.command;

import by.epam.filmrating.domain.User;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.servlet.SessionRequestContent;

/**
 * Created by Yura on 11.06.2016.
 */
public class LogoutCommand implements ActionCommand {

    private static final String USER_SESSION_ATTR_NAME = "user";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        User user = (User) requestContent.getSessionAttribute(USER_SESSION_ATTR_NAME);
        if(user != null) {
            requestContent.setInvalidateSession(true);
        }
        return AppConfigManager.getInstance().getProperty(AppConfigManager.LOGIN_PAGE_PATH);
    }
}
