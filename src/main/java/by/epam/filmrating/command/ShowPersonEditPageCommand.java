package by.epam.filmrating.command;

import by.epam.filmrating.domain.Person;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.service.PersonService;
import by.epam.filmrating.service.impl.PersonServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.RequestParamUtil;

/**
 * Created by Yura on 19.06.2016.
 */
public class ShowPersonEditPageCommand implements ActionCommand {

    private static final String PERSON_ID_PARAM_NAME = "id";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String id = requestContent.getParameter(PERSON_ID_PARAM_NAME);
        Integer personId = RequestParamUtil.getAsInteger(id);

        try {
            PersonService personService = new PersonServiceImpl();
            Person person;
            if(personId == null || (person = personService.getById(personId)) == null) {
                return AppConfigManager.getInstance().getProperty(AppConfigManager.ERROR_PAGE_404);
            }
            requestContent.setRequestAttribute("person", person);
            return AppConfigManager.getInstance().getProperty(AppConfigManager.SHOW_EDIT_PERSON_PATH_PAGE);
        } catch (ServiceException e) {
            throw new CommandException("Can't show person edit page", e);
        }
    }
}
