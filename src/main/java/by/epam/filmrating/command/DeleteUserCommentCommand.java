package by.epam.filmrating.command;

import by.epam.filmrating.domain.UserComment;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.service.FilmService;
import by.epam.filmrating.service.UserCommentService;
import by.epam.filmrating.service.impl.FilmServiceImpl;
import by.epam.filmrating.service.impl.UserCommentServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.Message;
import by.epam.filmrating.util.RequestParamUtil;

/**
 * Created by Yura on 15.06.2016.
 */
public class DeleteUserCommentCommand implements ActionCommand {

    private static final String COMMENT_ID_PARAM_NAME = "commentId";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String commentId = requestContent.getParameter(COMMENT_ID_PARAM_NAME);
        String page = "/controller?command=showFilmInfo&id=";
        try {
            Integer commentIdInt = RequestParamUtil.getAsInteger(commentId);
            UserCommentService userCommentService = new UserCommentServiceImpl();
            if(commentIdInt == null) {
                return AppConfigManager.getInstance().getProperty(AppConfigManager.ERROR_PAGE_400);
            }
            UserComment userComment = userCommentService.getById(commentIdInt);
            if(userComment == null) {
                throw new CommandException("Can't delete user comment");
            }
            page += userComment.getFilmId();
            userCommentService.delete(userComment.getId());
            requestContent.setSessionAttribute("message", new Message("delete_comment.message.delete.success",
                    "success"));
            requestContent.setRedirect(true);
            return page;
        } catch (ServiceException e) {
            throw new CommandException("Can't delete user comment", e);
        }
    }
}
