package by.epam.filmrating.command;

import by.epam.filmrating.domain.Genre;
import by.epam.filmrating.domain.Person;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.service.GenreService;
import by.epam.filmrating.service.PersonService;
import by.epam.filmrating.service.impl.GenreServiceImpl;
import by.epam.filmrating.service.impl.PersonServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.RequestParamUtil;

import java.util.List;

/**
 * Created by Yura on 11.06.2016.
 */
public class ShowFilmAddPageCommand implements ActionCommand {

    private static final String GENRES_REQUEST_ATTR_NAME = "genres";
    private static final String PERSONS_REQUEST_ATTR_NAME = "persons";


    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        try {
            GenreService genreService = new GenreServiceImpl();
            PersonService personService = new PersonServiceImpl();
            List<Genre> genres = genreService.getAll();
            List<Person> persons = personService.getAll();
            genres.sort((o1, o2) -> o1.getName().compareTo(o2.getName()));
            persons.sort((o1, o2) -> {
                int result = o1.getFirstName().compareTo(o2.getFirstName());
                if(result == 0) {
                    return o1.getLastName().compareTo(o2.getLastName());
                }
                return result;
            });
            int minYearValue = RequestParamUtil.getAsIntegerOrDefault(AppConfigManager.getInstance().getProperty(
                    AppConfigManager.FILM_YEAR_MIN_VALUE), 1);
            int maxYearValue = RequestParamUtil.getAsIntegerOrDefault(AppConfigManager.getInstance().getProperty(
                    AppConfigManager.FILM_YEAR_MAX_VALUE), 1);
            requestContent.setRequestAttribute(GENRES_REQUEST_ATTR_NAME, genres);
            requestContent.setRequestAttribute(PERSONS_REQUEST_ATTR_NAME, persons);
            requestContent.setRequestAttribute("minYearValue", minYearValue);
            requestContent.setRequestAttribute("maxYearValue", maxYearValue);
        } catch (ServiceException e) {
            throw new CommandException("Can't show add film page", e);
        }
        return AppConfigManager.getInstance().getProperty(AppConfigManager.FILM_ADD_PATH_PAGE);
    }
}
