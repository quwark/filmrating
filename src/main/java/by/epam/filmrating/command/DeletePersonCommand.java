package by.epam.filmrating.command;

import by.epam.filmrating.domain.Person;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.service.PersonService;
import by.epam.filmrating.service.impl.PersonServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.Message;
import by.epam.filmrating.util.RequestParamUtil;

/**
 * Created by Yura on 20.06.2016.
 */
public class DeletePersonCommand implements ActionCommand {

    private static final String PERSON_ID_PARAM_NAME = "personId";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String id = requestContent.getParameter(PERSON_ID_PARAM_NAME);
        Integer personId = RequestParamUtil.getAsInteger(id);
        String page = "/controller?command=showAllPersons";
        try {
            PersonService personService = new PersonServiceImpl();
            Person person;
            if(personId == null || (person = personService.getById(personId)) == null) {
                return AppConfigManager.getInstance().getProperty(AppConfigManager.ERROR_PAGE_400);
            }
            requestContent.setSessionAttribute("message", new Message("delete_person.message.delete.success",
                    "success"));
            personService.delete(personId);
            requestContent.setRedirect(true);
            return page;
        } catch (ServiceException e) {
            throw new CommandException("Can't delete person", e);
        }
    }
}
