package by.epam.filmrating.command;

import by.epam.filmrating.domain.Person;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.service.PersonService;
import by.epam.filmrating.service.impl.PersonServiceImpl;
import by.epam.filmrating.servlet.SessionRequestContent;
import by.epam.filmrating.util.Pagination;
import by.epam.filmrating.util.PaginationBuilder;
import by.epam.filmrating.util.RequestParamUtil;

import java.util.List;

/**
 * Created by Yura on 20.06.2016.
 */
public class ShowAllPersonsCommand implements ActionCommand {

    private static final String PAGE_PARAM_NAME = "page";

    @Override
    public String execute(SessionRequestContent requestContent) throws CommandException {
        String page = requestContent.getParameter(PAGE_PARAM_NAME);
        Integer pageNumber = RequestParamUtil.getAsIntegerOrDefault(page, 1);
        Integer countOnPage = RequestParamUtil.getAsInteger(AppConfigManager.getInstance().getProperty(
                AppConfigManager.PERSONS_RECORDS_ON_LIST));
        try {
            PersonService personService = new PersonServiceImpl();
            List<Person> persons = personService.getAll();

            Pagination<Person> pagination = PaginationBuilder.getPaginationObject(persons, pageNumber, countOnPage);
            requestContent.setRequestAttribute("pagination", pagination);
            requestContent.setRequestAttribute("persons", pagination.getRecordsOnPage());
            return AppConfigManager.getInstance().getProperty(AppConfigManager.SHOW_ALL_PERSONS_PATH_PAGE);
        } catch (ServiceException e) {
           throw new CommandException("Can't show all persons", e);
        }
    }
}
