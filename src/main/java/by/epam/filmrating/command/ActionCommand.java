package by.epam.filmrating.command;

import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.servlet.SessionRequestContent;

/**
 * Created by Yura on 10.06.2016.
 */
public interface ActionCommand {
    String execute(SessionRequestContent requestContent) throws CommandException;
}
