package by.epam.filmrating.model.vo;

import by.epam.filmrating.domain.Film;
import by.epam.filmrating.domain.User;
import by.epam.filmrating.domain.UserComment;

import java.util.Date;

/**
 * Created by Yura on 12.06.2016.
 */
public class CommentVO {

    private User user;
    private UserComment userComment;
    private Film film;

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserComment getUserComment() {
        return userComment;
    }

    public void setUserComment(UserComment userComment) {
        this.userComment = userComment;
    }
}
