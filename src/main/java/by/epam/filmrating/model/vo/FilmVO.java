package by.epam.filmrating.model.vo;

import by.epam.filmrating.domain.Film;
import by.epam.filmrating.domain.Genre;
import by.epam.filmrating.domain.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yura on 11.06.2016.
 */
public class FilmVO {

    private Film film;
    private List<Genre> genres;
    private Person producer;
    private List<Person> actors;
    private float mark;
    private int userMarkCount;

    public FilmVO() {
        genres = new ArrayList<>();
        actors = new ArrayList<>();
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public Person getProducer() {
        return producer;
    }

    public void setProducer(Person producer) {
        this.producer = producer;
    }

    public List<Person> getActors() {
        return actors;
    }

    public void setActors(List<Person> actors) {
        this.actors = actors;
    }

    public float getMark() {
        return mark;
    }

    public void setMark(float mark) {
        this.mark = mark;
    }

    public int getUserMarkCount() {
        return userMarkCount;
    }

    public void setUserMarkCount(int userMarkCount) {
        this.userMarkCount = userMarkCount;
    }
}
