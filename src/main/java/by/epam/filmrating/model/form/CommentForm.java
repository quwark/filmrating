package by.epam.filmrating.model.form;

/**
 * Created by Yura on 12.06.2016.
 */
public class CommentForm {

    private Integer filmId;
    private Integer userId;
    private String text;

    public Integer getFilmId() {
        return filmId;
    }

    public void setFilmId(Integer filmId) {
        this.filmId = filmId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
