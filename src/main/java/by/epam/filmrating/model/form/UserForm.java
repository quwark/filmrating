package by.epam.filmrating.model.form;

import by.epam.filmrating.service.validation.FormValidator;
import by.epam.filmrating.util.ValidationsUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yura on 10.06.2016.
 */
public class UserForm {

    private String login;
    private String password;
    private String repeatPassword;
    private String email;

    public UserForm() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
