package by.epam.filmrating.model.form;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yura on 11.06.2016.
 */
public class FilmForm {

    private Integer id;
    private String name;
    private String description;
    private List<Integer> genreIds;
    private Integer year;
    private Integer producerId;
    private List<Integer> actorIds;
    private String imgURL;

    public FilmForm() {
        genreIds = new ArrayList<>();
        actorIds = new ArrayList<>();
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Integer> getGenreIds() {
        return genreIds;
    }

    public void setGenreIds(List<Integer> genreIds) {
        this.genreIds = genreIds;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getProducerId() {
        return producerId;
    }

    public void setProducerId(Integer producerId) {
        this.producerId = producerId;
    }

    public List<Integer> getActorIds() {
        return actorIds;
    }

    public void setActorIds(List<Integer> actorIds) {
        this.actorIds = actorIds;
    }
}
