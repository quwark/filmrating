package by.epam.filmrating.model.form;

/**
 * Created by Yura on 17.06.2016.
 */
public class UserMarkForm {

    private Integer userId;
    private Integer filmId;
    private Integer userMark;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getFilmId() {
        return filmId;
    }

    public void setFilmId(Integer filmId) {
        this.filmId = filmId;
    }

    public Integer getUserMark() {
        return userMark;
    }

    public void setUserMark(Integer userMark) {
        this.userMark = userMark;
    }
}
