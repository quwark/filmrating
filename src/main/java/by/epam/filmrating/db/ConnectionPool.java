package by.epam.filmrating.db;

import by.epam.filmrating.exception.ConnectionPoolException;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Yura on 08.06.2016.
 */
public interface ConnectionPool {

    Connection getConnection() throws SQLException;
    void releaseConnection(Connection connection) throws SQLException;
    int getSize();
    void destroy();
}
