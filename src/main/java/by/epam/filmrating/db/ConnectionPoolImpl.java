package by.epam.filmrating.db;

import by.epam.filmrating.db.util.TimeWrapper;
import by.epam.filmrating.exception.ConnectionPoolException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Yura on 27.05.2016.
 */
public class ConnectionPoolImpl implements ConnectionPool {

    private static final Logger logger = Logger.getLogger(ConnectionPoolImpl.class);

    private String url;
    private String user;
    private String password;

    private int minSize;
    private int initialSize;
    private int maxSize;

    private long maxIdleTime;
    private long maxUseTime;
    private long cleaningInterval;

    private boolean released = false;

    private Cleaner cleaner;

    private BlockingQueue<TimeWrapper<Connection>> free;
    private BlockingQueue<TimeWrapper<Connection>> used;

    private Lock lock = new ReentrantLock();

    public ConnectionPoolImpl(String url, String user, String password, int minSize, int initialSize, int maxSize,
                              long maxIdleTime, long maxUseTime, long cleaningInterval) throws SQLException {
        this.url = url;
        this.user = user;
        this.password = password;
        setParameters(minSize, initialSize, maxSize, maxIdleTime, maxUseTime, cleaningInterval);

        free = new LinkedBlockingQueue<>(maxSize);
        used = new LinkedBlockingQueue<>(maxSize);
        init(initialSize);
        cleaner = new Cleaner(cleaningInterval);
        cleaner.start();
    }



    private void setParameters(int minSize, int initialSize, int maxSize, long maxIdleTime, long maxUseTime,
                               long cleaningInterval) {
        if(minSize < 0 || initialSize < 0 || maxSize < 0 || maxUseTime < 0 || maxIdleTime < 0 || cleaningInterval < 0) {
            throw new IllegalArgumentException("Negative values not accepted as pool parameters");
        }
        if(maxSize < minSize) {
            throw new IllegalArgumentException("Invalid minSize/maxSize values " + maxSize + '/' + maxSize);
        }
        if(initialSize < minSize || initialSize > maxSize ) {
            throw new IllegalArgumentException("Invalid initialSize value. Should be [" + minSize + ";" + maxSize + "]");
        }

        this.minSize = minSize;
        this.initialSize = initialSize;
        this.maxSize = maxSize;
        this.maxIdleTime = maxIdleTime;
        this.maxUseTime = maxUseTime;
        this.cleaningInterval = cleaningInterval;
    }

    private int init(int number) throws SQLException {
        if (number < 0)
            throw new IllegalArgumentException("Invalid number of items specified for initialization: " + number);
        if(number < minSize || number > maxSize) {
            throw new IllegalArgumentException("Invalid number of items specified for initialization: " + number +
                    ", minSize = " + minSize + ", maxSize = " + maxSize);
        }

        int created = 0;

        lock.lock();
        try {
            while(number > getSize()) {
                Connection conn = create();
                free.add(new TimeWrapper<>(conn, maxIdleTime));
                created++;
            }
        } finally {
            lock.unlock();
        }
        return created;
    }

    @Override
    public Connection getConnection() throws SQLException {
        if(released) {
            throw new IllegalStateException("Connection pool was released");
        }

        lock.lock();
        try {
            TimeWrapper<Connection> tw = null;
            Connection conn = null;
            if(free.size() > 0) {
                tw = free.poll();
                conn = tw.getObject();
            }else if(free.isEmpty()) {
                int totalConn = getSize();
                if(totalConn < maxSize) {
                    conn = create();
                }
            }
            if(conn != null) {
                used.add(new TimeWrapper<>(conn, maxUseTime));
            }
            return new ConnectionWrapper(conn, this);
        } finally {
            lock.unlock();
        }
    }

    private Connection create() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }

    @Override
    public void releaseConnection(Connection connection) throws SQLException {
        if(released) {
            throw new IllegalStateException("Connection pool was released");
        }
        if(connection == null) {
            return;
        }
        if(connection instanceof ConnectionWrapper) {
            connection = ((ConnectionWrapper) connection).getConnection();
        }
        lock.lock();
        try {
            Iterator<TimeWrapper<Connection>> it = used.iterator();
            while(it.hasNext()) {
                TimeWrapper<Connection> tw = it.next();
                if(tw.getObject() == connection) {
                    it.remove();
                    if(validate(connection)) {
                        free.add(new TimeWrapper<>(connection, maxIdleTime));
                    } else {
                        destroyConnection(connection);
                        init(minSize);
                    }
                    return;
                }
            }
            throw new IllegalArgumentException("Attempt to return item not belonging to pool");
        } finally {
            lock.unlock();
        }
    }

    protected void destroyConnection(Connection connection) {
        try {
            if(connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException e) {
            logger.error("Can't destroy connection correctly", e);
        }
    }

    @Override
    public void destroy() {
        lock.lock();
        try {
            if(cleaner != null) {
                cleaner.halt();
                cleaner = null;
            }
            for(TimeWrapper<Connection> tw : free) {
                destroyConnection(tw.getObject());
            }
            free.clear();
            for(TimeWrapper<Connection> tw : used) {
                destroyConnection(tw.getObject());
            }
            used.clear();
            released = true;
        } finally {
            lock.unlock();
        }
    }

    public int purgeUsed() {
        lock.lock();
        int count = 0;
        try {
            if(used.isEmpty()) {
                return count;
            }
            TimeWrapper<Connection> tw = null;
            Iterator<TimeWrapper<Connection>> it = used.iterator();
            while(it.hasNext()) {
                tw = it.next();
                if(tw.isExpired()) {
                    destroyConnection(tw.getObject());
                    it.remove();
                    count++;
                }
            }
            return count;
        } finally {
            lock.unlock();
        }
    }

    public int purgeFree() {
        lock.lock();
        int count = 0;
        try {
            if(free.isEmpty()) {
                return count;
            }
            TimeWrapper<Connection> tw = null;
            Iterator<TimeWrapper<Connection>> it = free.iterator();
            while(it.hasNext()) {
                tw = it.next();
                if(tw.isExpired()) {
                    destroyConnection(tw.getObject());
                    it.remove();
                    count++;
                }
            }
            return count;
        } finally {
            lock.unlock();
        }
    }



    private final class Cleaner extends Thread {

        private long interval;
        private volatile boolean stopped;

        public Cleaner(long interval) {
            this.interval = interval;
        }

        @Override
        public synchronized void start() {
            stopped = false;
            super.start();
        }

        @Override
        public void run() {
            while(!stopped) {
                lock.lock();
                try {
                    if(getSize() > 0 && !stopped) {
                        purgeUsed();
                        purgeFree();
                        init(minSize);
                    }
                } catch (SQLException e) {
                    logger.error("Can't clear expired connections correctly", e);
                } finally {
                    lock.unlock();
                }

                if(!stopped) {
                    try {
                        Thread.sleep(interval);
                    } catch (InterruptedException e) {
                       ;
                    }
                }
            }
        }

        public void halt() {
            stopped = true;
            this.interrupt();
        }
    }

    @Override
    public int getSize() {
        lock.lock();
        try {
            return free.size() + used.size();
        } finally {
            lock.unlock();
        }
    }

    protected boolean validate(Connection conn) {
        try {
            if(!conn.isClosed()) {
                if(!conn.getAutoCommit()) {
                    conn.rollback();
                    conn.setAutoCommit(true);
                }
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public long getMaxUseTime() {
        return maxUseTime;
    }

    public void setMaxUseTime(long maxUseTime) {
        this.maxUseTime = maxUseTime;
    }

    public long getMaxIdleTime() {
        return maxIdleTime;
    }

    public void setMaxIdleTime(long maxIdleTime) {
        this.maxIdleTime = maxIdleTime;
    }
}
