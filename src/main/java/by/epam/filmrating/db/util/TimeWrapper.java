package by.epam.filmrating.db.util;

/**
 * Created by Yura on 27.05.2016.
 */
public class TimeWrapper<T> {

    private final T obj;
    private long expiryTime;

    public TimeWrapper(T obj, long expiry) {
        this.obj = obj;
        if(expiry > 0) {
            this.expiryTime = System.currentTimeMillis() + expiry;
        }
    }

    public T getObject() {
        return obj;
    }

    public boolean isExpired() {
        return expiryTime > 0 && (System.currentTimeMillis() > expiryTime);
    }
}
