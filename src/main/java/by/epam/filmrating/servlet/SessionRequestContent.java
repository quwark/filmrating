package by.epam.filmrating.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.*;

/**
 * Created by Yura on 10.06.2016.
 */
public class SessionRequestContent {
    private Map<String, Object> requestAttributes;
    private Map<String, String[]> requestParameters;
    private Map<String, Object> sessionAttributes;
    private boolean invalidateSession;
    private boolean redirect;
    private List<Part> parts = new ArrayList<>();

    public SessionRequestContent() {
        requestAttributes = new HashMap<>();
        requestParameters = new HashMap<>();
        sessionAttributes = new HashMap<>();
    }

    public void extractValues(HttpServletRequest request) throws IOException, ServletException {
        Enumeration<String> attributeNames = request.getAttributeNames();
        while(attributeNames.hasMoreElements()) {
            String name = attributeNames.nextElement();
            requestAttributes.put(name, request.getAttribute(name));
        }

        Map<String, String[]> parametersMap = request.getParameterMap();
        Set<String> keySet = parametersMap.keySet();
        for(String key : keySet) {
            requestParameters.put(key, parametersMap.get(key));
        }

        HttpSession session = request.getSession(false);
        if(session != null) {
            Enumeration<String> sessionAttributeNames = session.getAttributeNames();
            while(sessionAttributeNames.hasMoreElements()) {
                String name = sessionAttributeNames.nextElement();
                sessionAttributes.put(name, session.getAttribute(name));
            }
        }
        if (request.getContentType() != null && request.getContentType().toLowerCase().contains("multipart/form-data")) {
            Iterator<Part> partIterator = request.getParts().iterator();
            while (partIterator.hasNext()) {
                parts.add(partIterator.next());
            }
        }
    }

    public Part getPart(String name) {
        Part part = null;
        for(Part p : parts) {
            if(p.getName().equals(name)) {
                part = p;
                break;
            }
        }
        return part;
    }

    public List<Part> getParts() {
        return parts;
    }

    public void insertAttributes(HttpServletRequest request) {
        Set<String> attributeNames = requestAttributes.keySet();
        for(String attrName : attributeNames) {
            request.setAttribute(attrName, requestAttributes.get(attrName));
        }

        if(!sessionAttributes.isEmpty() && !invalidateSession) {
            HttpSession session = request.getSession();
            Set<String> sessionAttrs = sessionAttributes.keySet();
            for(String sessionAttr : sessionAttrs) {
                session.setAttribute(sessionAttr, sessionAttributes.get(sessionAttr));
            }
        }else if(invalidateSession){
            HttpSession session = request.getSession(false);
            if(session != null) {
                session.invalidate();
            }
        }
    }

    public String[] getParameterValues(String name) {
        if(requestParameters.containsKey(name)) {
            return requestParameters.get(name);
        }
        return null;
    }

    public String getParameter(String name) {
        if(requestParameters.containsKey(name)) {
            return requestParameters.get(name)[0];
        }
        return null;
    }

    public void setRequestAttribute(String name, Object ob) {
        requestAttributes.put(name, ob);
    }

    public void setSessionAttribute(String name, Object ob) {
        sessionAttributes.put(name, ob);
    }

    public Object getSessionAttribute(String name) {
        if(sessionAttributes.containsKey(name)) {
            return sessionAttributes.get(name);
        }
        return null;
    }

    public boolean isInvalidateSession() {
        return invalidateSession;
    }

    public void setInvalidateSession(boolean invalidateSession) {
        this.invalidateSession = invalidateSession;
    }

    public void removeSessionAttribute(String name) {
        sessionAttributes.remove(name);
    }

    public void removeRequestAttribute(String name) {
        requestAttributes.remove(name);
    }

    public void setRedirect(boolean redirect) {
        this.redirect = redirect;
    }

    public boolean isRedirect() {
        return redirect;
    }

}
