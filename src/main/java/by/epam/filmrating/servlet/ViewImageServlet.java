package by.epam.filmrating.servlet;

import by.epam.filmrating.manager.AppConfigManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created by Yura on 26.06.2016.
 */
public class ViewImageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        OutputStream out = resp.getOutputStream();

        InputStream inputStream = null;
        try {
            String fileName = AppConfigManager.getInstance().getProperty(AppConfigManager.UPLOADS_DIR_PATH)
                    + req.getPathInfo();
            File outputFile = new File(fileName);
            if(outputFile.exists() && !outputFile.isDirectory()) {
                inputStream = new FileInputStream(outputFile);
                BufferedInputStream in = new BufferedInputStream(inputStream);
                int b;
                byte[] buffer = new byte[10240];
                while((b = in.read(buffer, 0, 10240)) != -1) {
                    out.write(buffer, 0, b);
                }
                in.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

        }
    }
}
