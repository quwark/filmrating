package by.epam.filmrating.servlet;

import by.epam.filmrating.command.ActionCommand;
import by.epam.filmrating.command.factory.ActionFactory;
import by.epam.filmrating.exception.CommandException;
import by.epam.filmrating.exception.DaoException;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Yura on 08.06.2016.
 */
@MultipartConfig
public class Controller extends HttpServlet {

    private static final Logger logger = Logger.getLogger(Controller.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    private void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String page = null;
        SessionRequestContent requestContent = new SessionRequestContent();
        try {
            ActionFactory actionFactory = ActionFactory.getInstance();
            requestContent.extractValues(req);
            ActionCommand command = actionFactory.defineCommand(requestContent);
            page = command.execute(requestContent);
        } catch (CommandException e) {
            logger.error("Command error", e);
            requestContent.setRequestAttribute("messageHeader", "Data base");
            requestContent.setRequestAttribute("errorMessage", e.getMessage());
            page = "/jsp/errors/error.jsp";
        } catch (DaoException e) {
            logger.error("Dao error", e);
            requestContent.setRequestAttribute("messageHeader", "Data base");
            requestContent.setRequestAttribute("errorMessage", e.getMessage());
            page = "/jsp/errors/error.jsp";
        }

        requestContent.insertAttributes(req);
        if(requestContent.isRedirect()) {
            resp.sendRedirect(resp.encodeRedirectURL(page));
        }
        else {
            RequestDispatcher requestDispatcher = req.getRequestDispatcher(page);
            requestDispatcher.forward(req, resp);
        }
    }
}
