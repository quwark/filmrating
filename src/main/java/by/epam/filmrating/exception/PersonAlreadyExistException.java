package by.epam.filmrating.exception;

/**
 * Created by Yura on 19.06.2016.
 */
public class PersonAlreadyExistException extends Exception {

    public PersonAlreadyExistException() {
    }

    public PersonAlreadyExistException(String message) {
        super(message);
    }

    public PersonAlreadyExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public PersonAlreadyExistException(Throwable cause) {
        super(cause);
    }

    public PersonAlreadyExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
