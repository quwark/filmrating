package by.epam.filmrating.exception;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yura on 13.06.2016.
 */
public class ValidationException extends Exception {

    private Map<String, String> errors = new HashMap<>();

    public ValidationException() {
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValidationException(Throwable cause) {
        super(cause);
    }

    public ValidationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    public void addError(String field, String message) {
        errors.put(field, message);
    }

    public void setErrors(Map<String, String> errors) {
        this.errors = errors;
    }
}
