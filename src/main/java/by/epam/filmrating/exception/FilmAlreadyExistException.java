package by.epam.filmrating.exception;

/**
 * Created by Yura on 28.06.2016.
 */
public class FilmAlreadyExistException extends Exception {
    public FilmAlreadyExistException() {
    }

    public FilmAlreadyExistException(String message) {
        super(message);
    }

    public FilmAlreadyExistException(String message, Throwable cause) {
        super(message, cause);
    }

    public FilmAlreadyExistException(Throwable cause) {
        super(cause);
    }

    public FilmAlreadyExistException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
