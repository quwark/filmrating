package by.epam.filmrating.util;

/**
 * Created by Yura on 10.06.2016.
 */
public class Message {
    private String message;
    private String type;

    public Message(String message, String type) {
        this.message = message;
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public String getType() {
        return type;
    }
}
