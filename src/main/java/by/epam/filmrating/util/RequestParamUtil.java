package by.epam.filmrating.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Yura on 17.06.2016.
 */
public class RequestParamUtil {

    private RequestParamUtil() {

    }

    public static String getAsStringOrDefault(String param, String defValue) {
        if(param == null || param.trim().isEmpty()) {
            return defValue;
        } else {
            return param.trim();
        }
    }

    public static String getAsString(String param) {
        return getAsStringOrDefault(param, null);
    }

    public static Integer getAsIntegerOrDefault(String param, Integer defValue) {
        try {
            return Integer.parseInt(param);
        } catch (NumberFormatException e) {
            return defValue;
        }
    }

    public static Integer getAsInteger(String param) {
        return getAsIntegerOrDefault(param, null);
    }

    public static Integer[] getAsIntegerArray(String[] array, boolean requiredNotNull) {
        if(array == null) {
            return null;
        }
        Integer[] intArray = new Integer[array.length];
        for(int i = 0; i < intArray.length; i++) {
            Integer value = getAsInteger(array[i]);
            if(value == null && requiredNotNull) {
                return null;
            }
            intArray[i] = value;
        }
        return intArray;
    }

    public static List<Integer> getAsIntegerList(String[] array, boolean requiredNotNull) {
        Integer[] intArray = getAsIntegerArray(array, requiredNotNull);
        if(intArray == null) {
            return new ArrayList<>();
        }
        List<Integer> list = Arrays.asList(intArray);
        if(!requiredNotNull) {
            list.removeIf((ob) -> (ob == null));
        }
        return list;
    }

    public static  <T> Map<T, Boolean> getSelectedValueMap(List<T> fullList, List<T> selectedList) {
        Map<T, Boolean> selectedValueMap = new HashMap<>();
        for(T value1 : fullList) {
            boolean isSelected = false;
            for(T value2 : selectedList) {
                if(value1.equals(value2)) {
                    isSelected = true;
                    break;
                }
            }
            selectedValueMap.put(value1, isSelected);
        }
        return selectedValueMap;
    }

    public static boolean checkNull(Object... params) {
        for(Object object : params) {
            if(object == null) {
                return true;
            }
        }
        return false;
    }

    public static Date getAsDate(String date, String pattern) {
        if(date == null || pattern == null) {
            return null;
        }
        try {
            SimpleDateFormat format = new SimpleDateFormat(pattern);
            return format.parse(date);
        } catch (ParseException e) {
            return null;
        }
    }
}
