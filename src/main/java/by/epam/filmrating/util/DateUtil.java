package by.epam.filmrating.util;


/**
 * Created by Yura on 06.06.2016.
 */
public class DateUtil {

    private DateUtil() {

    }

    public static java.util.Date convertFromSqlToUtilDate(java.sql.Date date) {
        if(date == null) {
            return null;
        }
        return new java.util.Date(date.getTime());
    }

    public static java.sql.Date convertFromUtilToSqlDate(java.util.Date date) {
        if(date == null) {
            return null;
        }
        return new java.sql.Date(date.getTime());
    }

    public static java.sql.Timestamp convertFromUtilDateToSqlTimestamp(java.util.Date date) {
        if(date == null) {
            return null;
        }
        return new java.sql.Timestamp(date.getTime());
    }

    public static java.util.Date convertFromTimestampToUtilDate(java.sql.Timestamp timestamp) {
        if(timestamp == null) {
            return null;
        }
        return new java.util.Date(timestamp.getTime());
    }
}
