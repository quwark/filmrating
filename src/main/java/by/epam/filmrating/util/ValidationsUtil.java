package by.epam.filmrating.util;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by Yura on 10.06.2016.
 */
public class ValidationsUtil {


    public static boolean validateString(String str, String regexp) {
        if(str == null) {
            return false;
        }
        Pattern pattern = Pattern.compile(regexp);
        return pattern.matcher(str).matches();
    }

    public static boolean checkInRangeInteger(Integer value, Integer min, Integer max) {
        if(value == null) {
            return false;
        }
        if(min == null && max == null) {
            return true;
        } else if(min == null) {
            return value <= max;
        } else if(max == null) {
            return value >= min;
        }
        return (value >= min && value <= max);
    }
}
