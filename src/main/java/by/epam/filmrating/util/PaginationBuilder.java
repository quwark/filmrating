package by.epam.filmrating.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yura on 15.06.2016.
 */
public class PaginationBuilder {

    public static <T> Pagination<T> getPaginationObject(List<T> list, int page, int count) {
        List<T> onPageList = new ArrayList<>();
        int listSize = list.size();
        if(page <= 0) {
            page = 1;
        }
        int from = (page - 1) * count;
        int totalPages = (int) Math.ceil(list.size() * 1.0 / count);

        if(from > listSize) {
            page = 1;
            from = 0;
        }

        if(from + count > listSize) {
            count = listSize - from;
        }

        for(int i = from; i < from + count; i++) {
            onPageList.add(list.get(i));
        }

        return new Pagination<T>(onPageList, page, totalPages);
    }
}
