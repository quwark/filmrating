package by.epam.filmrating.util;

import java.util.List;

/**
 * Created by Yura on 14.06.2016.
 */
public class Pagination<T> {

    private int currentPage;
    private int totalPages;
    private int maxLinks;
    private List<T> recordsOnPage;

    public Pagination(List<T> recordsOnPage, int currentPage, int totalPages, int maxLinks) {
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.maxLinks = maxLinks;
        this.recordsOnPage = recordsOnPage;
    }

    public Pagination(List<T> recordsOnPage, int currentPage, int totalPages) {
        this.currentPage = currentPage;
        this.totalPages = totalPages;
        this.maxLinks = 5;
        this.recordsOnPage = recordsOnPage;
    }

    public Pagination() {
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getMaxLinks() {
        return maxLinks;
    }

    public void setMaxLinks(int maxLinks) {
        this.maxLinks = maxLinks;
    }

    public List<T> getRecordsOnPage() {
        return recordsOnPage;
    }

    public void setRecordsOnPage(List<T> recordsOnPage) {
        this.recordsOnPage = recordsOnPage;
    }
}
