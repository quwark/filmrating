package by.epam.filmrating.manager;

import java.util.ResourceBundle;

/**
 * Created by Yura on 10.06.2016.
 */
public class AppConfigManager {

    private final static String BUNDLE_NAME = "config.config";
    private ResourceBundle resourceBundle;

    public static final String LOGIN_PAGE_PATH = "path.page.login";
    public static final String REGISTRATION_PAGE_PATH = "path.page.registration";
    public static final String TEST_PAGE_PATH = "path.page.test";
    public static final String USERS_PAGE_PATH = "path.page.users";
    public static final String USER_PAGE_PATH = "path.page.user";
    public static final String USER_MARK_CALCULATING_MIN_COUNT = "app.mark.calculating.min_count";
    public static final String USER_MARK_CALCULATING_STEP = "app.mark.calculating.step";
    public static final String FILM_RECORDS_ON_LIST = "app.films.records_on_list";
    public static final String USER_RECORDS_ON_LIST = "app.users.records_on_list";
    public static final String USER_COMMENTS_RECORDS_ON_LIST = "app.comments.records_on_list";
    public static final String PERSONS_RECORDS_ON_LIST = "app.persons.records_on_page";
    public static final String USERS_COMMENTS_RECORDS_ON_LIST = "app.user_comments.records_on_page";
    public static final String FILM_EDIT_PATH_PAGE = "path.page.film_edit";
    public static final String FILM_ADD_PATH_PAGE = "path.page.add_film";
    public static final String SHOW_ALL_FILMS_PATH_PAGE = "path.page.show_films";
    public static final String SHOW_ALL_PERSONS_PATH_PAGE = "path.page.show_persons";
    public static final String SHOW_FILM_INFO_PATH_PAGE = "path.page.show_film_info";
    public static final String SHOW_ALL_GENRES_PATH_PAGE = "path.page.show_genres";
    public static final String SHOW_ADD_PERSON_PATH_PAGE = "path.page.add_person";
    public static final String SHOW_EDIT_PERSON_PATH_PAGE = "path.page.edit_person";
    public static final String CONTROLLER_PATH = "path.controller";
    public static final String ERROR_PAGE_400 = "path.page.error.400";
    public static final String ERROR_PAGE_401 = "path.page.error.401";
    public static final String ERROR_PAGE_403 = "path.page.error.403";
    public static final String ERROR_PAGE_404 = "path.page.error.404";
    public static final String INDEX_PATH_PAGE = "path.page.index";
    public static final String MARK_MIN_VALUE = "app.mark.min.value";
    public static final String MARK_MAX_VALUE = "app.mark.max.value";
    public static final String FILM_YEAR_MIN_VALUE = "app.film.year.min.value";
    public static final String FILM_YEAR_MAX_VALUE = "app.film.year.max.value";
    public static final String UPLOADS_DIR_PATH = "app.uploads.dir.path";
    public static final String DEFAULT_IMAGE_PATH = "app.uploads.default.image.path";
    public static final String PASSWORD_SALT = "app.password.salt";
    public static final String SHOW_PERSON_INFO_PATH_PAGE = "path.page.person_info";

    private AppConfigManager() {
        resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
    }

    public static AppConfigManager getInstance() {
        return ConfigManagerSingleton.INSTANCES;
    }

    private static final class ConfigManagerSingleton {
        public static final AppConfigManager INSTANCES = new AppConfigManager();
    }

    public String getProperty(String key) {
        return (String)resourceBundle.getObject(key);
    }
}
