package by.epam.filmrating.manager;

import java.util.ResourceBundle;

/**
 * Created by Yura on 10.06.2016.
 */
public class JdbcConfigManager {

    private static final String BUNDLE_NAME = "config.jdbc";
    public ResourceBundle resourceBundle;

    public static final String DATABASE_DRIVER_NAME = "DATABASE_DRIVER_NAME";
    public static final String DATABASE_URL = "DATABASE_URL";
    public static final String DATABASE_USER = "DATABASE_USER";
    public static final String DATABASE_PASSWORD = "DATABASE_PASSWORD";
    public static final String POOL_MIN_SIZE = "POOL_MIN_SIZE";
    public static final String POOL_INITIAL_SIZE = "POOL_INITIAL_SIZE";
    public static final String POOL_MAX_SIZE = "POOL_MAX_SIZE";
    public static final String POOL_MAX_IDLE_TIME = "POOL_MAX_IDLE_TIME";
    public static final String POOL_MAX_USE_TIME = "POOL_MAX_USE_TIME";
    public static final String POOL_CLEARING_INTERVAL = "POOL_CLEARING_INTERVAL";

    private JdbcConfigManager() {
        resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
    }

    public static JdbcConfigManager getInstance() {
        return JdbcConfigManagerSingleton.INSTANCE;
    }

    private static final class JdbcConfigManagerSingleton {
        public static final JdbcConfigManager INSTANCE = new JdbcConfigManager();
    }

    public String getProperty(String key) {
        return (String) resourceBundle.getObject(key);
    }
}
