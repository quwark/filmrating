package by.epam.filmrating.manager;

import java.util.ResourceBundle;

/**
 * Created by Yura on 15.06.2016.
 */
public class SecurityManager {

    private ResourceBundle resourceBundle;
    private static final String BUNDLE_NAME = "config.security";

    public static final String ADMIN_ACCESS_COMMANDS = "ADMIN";
    public static final String USER_ACCESS_COMMANDS = "USER";
    public static final String GUEST_ACCESS_COMMANDS = "GUEST";

    private SecurityManager() {
        resourceBundle = ResourceBundle.getBundle(BUNDLE_NAME);
    }

    public static SecurityManager getInstance() {
        return SecurityManagerSingleton.INSTANCE;
    }

    private static final class SecurityManagerSingleton {
        private static final SecurityManager INSTANCE = new SecurityManager();
    }

    public String getProperty(String key) {
        return (String) resourceBundle.getObject(key);
    }
}
