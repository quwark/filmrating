package by.epam.filmrating.domain;

/**
 * Created by Yura on 07.06.2016.
 */
public enum UserStatus {
    ACTIVE,
    BANNED
}
