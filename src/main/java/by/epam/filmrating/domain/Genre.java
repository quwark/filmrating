package by.epam.filmrating.domain;

import java.io.Serializable;

/**
 * Created by Yura on 21.05.2016.
 */
public class Genre implements Serializable {

    private int id;
    private String name;

    public Genre() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int result = 7;
        result += 13 * result + id;
        result += 13 * result + (name == null ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof Genre)) {
            return false;
        }

        Genre genre = (Genre) obj;
        return (id == genre.id) &&
                (name == null ? genre.name == null : name.equals(genre.name));
    }

    @Override
    public String toString() {
        return "Genre{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
