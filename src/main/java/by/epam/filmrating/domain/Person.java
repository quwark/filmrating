package by.epam.filmrating.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Yura on 21.05.2016.
 */
public class Person implements Serializable {

    private int id;
    private String firstName;
    private String lastName;
    private Date birthDate;

    public Person() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public int hashCode() {
        int result = 7;
        result += 13 * result + id;
        result += 13 * result + (firstName == null ? 0 : firstName.hashCode());
        result += 13 * result + (lastName == null ? 0 : lastName.hashCode());
        result += 13 * result + (birthDate == null ? 0 : birthDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof Person)) {
            return false;
        }

        Person person = (Person) obj;
        return (id == person.id) &&
                (firstName == null ? person.firstName == null : firstName.equals(person.firstName)) &&
                (lastName == null ? person.lastName == null : lastName.equals(person.lastName)) &&
                (birthDate == null ? person.birthDate == null : birthDate.equals(person.birthDate));
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthDate=" + birthDate +
                '}';
    }
}
