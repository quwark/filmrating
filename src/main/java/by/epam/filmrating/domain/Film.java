package by.epam.filmrating.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Yura on 21.05.2016.
 */
public class Film implements Serializable {

    private int id;
    private String name;
    private String description;
    private String imgURL;
    private int year;
    private Date addedDate;

    public Film() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    @Override
    public int hashCode() {
        int result = 7;
        result += 13 * result + id;
        result += 13 * result + (name == null ? 0 : name.hashCode());
        result += 13 * result + (description == null ? 0 : description.hashCode());
        result += 13 * result + (imgURL == null ? 0 : imgURL.hashCode());
        result += 13 * result + year;
        result += 13 * result + (addedDate == null ? 0 : addedDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof Film)) {
            return false;
        }

        Film film = (Film) obj;
        return (id == film.id) &&
                (name == null ? film.name == null : name.equals(film.name)) &&
                (description == null ? film.description == null : description.equals(film.description)) &&
                (imgURL == null ? film.imgURL == null : imgURL.equals(film.imgURL)) &&
                (year == film.year) &&
                (addedDate == null ? film.addedDate == null : addedDate.equals(film.addedDate));
    }

    @Override
    public String toString() {
        return "Film{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", imgURL='" + imgURL + '\'' +
                ", year=" + year +
                ", addedDate=" + addedDate +
                '}';
    }
}
