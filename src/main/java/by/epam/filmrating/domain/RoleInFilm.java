package by.epam.filmrating.domain;

import java.io.Serializable;

/**
 * Created by Yura on 21.05.2016.
 */
public class RoleInFilm implements Serializable {

    private int id;
    private String name;

    public RoleInFilm() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int result = 7;
        result += 13 * result + id;
        result += 13 * result + (name == null ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof RoleInFilm)) {
            return false;
        }

        RoleInFilm roleInFilm = (RoleInFilm) obj;
        return (id == roleInFilm.id) &&
                (name == null ? roleInFilm.name == null : name.equals(roleInFilm.name));
    }

    @Override
    public String toString() {
        return "RoleInFilm{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
