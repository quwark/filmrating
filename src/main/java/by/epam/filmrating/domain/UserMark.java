package by.epam.filmrating.domain;

import java.io.Serializable;

/**
 * Created by Yura on 21.05.2016.
 */
public class UserMark implements Serializable {

    private int id;
    private int mark;
    private int userId;
    private int filmId;
    private boolean consider;

    public UserMark() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getFilmId() {
        return filmId;
    }

    public void setFilmId(int filmId) {
        this.filmId = filmId;
    }

    public boolean isConsider() {
        return consider;
    }

    public void setConsider(boolean consider) {
        this.consider = consider;
    }

    @Override
    public int hashCode() {
        int result = 7;
        result += 13 * result + id;
        result += 13 * result + mark;
        result += 13 * result + userId;
        result += 13 * result + filmId;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof UserMark)) {
            return false;
        }

        UserMark userMark = (UserMark) obj;
        return (id == userMark.id) &&
                (mark == userMark.mark) &&
                (userId == userMark.userId) &&
                (filmId == userMark.filmId);
    }

    @Override
    public String toString() {
        return "UserMark{" +
                "id=" + id +
                ", mark=" + mark +
                ", userId=" + userId +
                ", filmId=" + filmId +
                '}';
    }
}
