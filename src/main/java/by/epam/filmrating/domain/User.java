package by.epam.filmrating.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Yura on 21.05.2016.
 */
public class User implements Serializable {

    private int id;
    private String login;
    private String password;
    private String email;
    private UserRole role;
    private int rating;
    private UserStatus status;
    private Date registrationDate;

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    @Override
    public int hashCode() {
        int result = 7;
        result += 13 * result + id;
        result += 13 * result + (login == null ? 0 : login.hashCode());
        result += 13 * result + (password == null ? 0 : password.hashCode());
        result += 13 * result + (email == null ? 0 : email.hashCode());
        result += 13 * result + (role == null ? 0 : role.hashCode());
        result += 13 * result + rating;
        result += 13 * result + (status == null ? 0 : status.hashCode());
        result += 13 * result + (registrationDate == null ? 0 : registrationDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof User)) {
            return false;
        }

        User user = (User) obj;
        return (id == user.id) &&
                (login == null ? user.login == null : login.equals(user.login)) &&
                (password == null ? user.password == null : password.equals(user.password)) &&
                (email == null ? user.email == null : email.equals(user.email)) &&
                (role == user.role) &&
                (rating == user.rating) &&
                (status == user.status) &&
                (registrationDate == null ? user.registrationDate == null :
                        registrationDate.equals(user.registrationDate));
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", role=" + role +
                ", rating=" + rating +
                ", status=" + status +
                ", registrationDate=" + registrationDate +
                '}';
    }
}
