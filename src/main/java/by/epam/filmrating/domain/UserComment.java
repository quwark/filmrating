package by.epam.filmrating.domain;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Yura on 21.05.2016.
 */
public class UserComment implements Serializable {

    private int id;
    private String text;
    private Date commentTime;
    private int userId;
    private int filmId;

    public UserComment() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(Date commentTime) {
        this.commentTime = commentTime;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getFilmId() {
        return filmId;
    }

    public void setFilmId(int filmId) {
        this.filmId = filmId;
    }

    @Override
    public int hashCode() {
        int result = 7;
        result += 13 * result + id;
        result += 13 * result + (text == null ? 0 : text.hashCode());
        result += 13 * result + (commentTime == null ? 0 : commentTime.hashCode());
        result += 13 * result + userId;
        result += 13 * result + filmId;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(!(obj instanceof UserComment)) {
            return false;
        }

        UserComment userComment = (UserComment) obj;
        return (id == userComment.id) &&
                (text == null ? userComment.text == null : text.equals(userComment.text)) &&
                (commentTime == null ? userComment.commentTime == null : commentTime.equals(userComment.commentTime)) &&
                (userId == userComment.userId) &&
                (filmId == userComment.filmId);
    }

    @Override
    public String toString() {
        return "UserComment{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", commentTime=" + commentTime +
                ", userId=" + userId +
                ", filmId=" + filmId +
                '}';
    }
}
