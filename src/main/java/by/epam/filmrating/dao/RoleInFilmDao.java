package by.epam.filmrating.dao;

import by.epam.filmrating.domain.RoleInFilm;

/**
 * Interface RoleInFilmDao contains operations for entity Genre
 * @see by.epam.filmrating.domain.RoleInFilm
 *
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-06
 */

public interface RoleInFilmDao extends GenericDao<RoleInFilm, Integer> {
}
