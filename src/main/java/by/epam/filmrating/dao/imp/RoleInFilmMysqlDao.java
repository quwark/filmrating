package by.epam.filmrating.dao.imp;

import by.epam.filmrating.dao.RoleInFilmDao;
import by.epam.filmrating.dao.util.RowMapper;
import by.epam.filmrating.domain.RoleInFilm;
import by.epam.filmrating.exception.DaoException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Class RoleInFilmMysqlDao contains operations with entity RoleInFilm for MySQL database
 * @see by.epam.filmrating.domain.RoleInFilm
 * @see by.epam.filmrating.dao.util.RowMapper
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-06
 */
public class RoleInFilmMysqlDao extends AbstractDatabaseDao implements RoleInFilmDao {

    private static final String SQL_SELECT_ALL_ROLE_IN_FILMS = "SELECT * FROM role_in_film";
    private static final String SQL_SELECT_ROLE_IN_FILM_BY_ID = "SELECT * FROM role_in_film WHERE id = ?";
    private static final String SQL_INSERT_ROLE_IN_FILM = "INSERT INTO role_in_film(name) VALUES(?)";
    private static final String SQL_UPDATE_ROLE_IN_FILM = "UPDATE role_in_film SET name = ? WHERE id = ?";
    private static final String SQL_DELETE_ROLE_IN_FILM = "DELETE FROM role_in_film WHERE id = ?";

    private static final RoleInFilmRowMapper rowMapper = new RoleInFilmRowMapper();

    public RoleInFilmMysqlDao(Connection connection) {
        super(connection);
    }

    /**
     * This method find all roles in film from database
     * @return list of all roles in film
     * @throws DaoException
     */
    @Override
    public List<RoleInFilm> findAll() throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_ALL_ROLE_IN_FILMS, rowMapper);
        } catch (SQLException e) {
            throw new DaoException("Can't select all records from table role_in_film", e);
        }
    }

    /**
     * This method find role in film in database by primary key
     * @param id primary key
     * @return role in film with this primary key or null if doesn't exist
     * @throws DaoException
     */
    @Override
    public RoleInFilm findById(Integer id) throws DaoException {
        try {
            return jdbcTemplate.executeFindOne(SQL_SELECT_ROLE_IN_FILM_BY_ID, rowMapper, id);
        } catch (SQLException e) {
            throw new DaoException("Can't select a record from table role_in_film with id: " + id, e);
        }
    }

    /**
     * This method create new role in film in database
     * @param entity object to save
     * @throws DaoException
     */
    @Override
    public void create(RoleInFilm entity) throws DaoException {
        try {
            int id = jdbcTemplate.executeInsertQuery(SQL_INSERT_ROLE_IN_FILM, entity.getName());
            entity.setId(id);
        } catch (SQLException e) {
            throw new DaoException("Can't create new record in table role_in_film", e);
        }
    }

    /**
     * This method update role in film in database
     * @param entity object to update
     * @throws DaoException
     */
    @Override
    public void update(RoleInFilm entity) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_UPDATE_ROLE_IN_FILM, entity.getName(), entity.getId());
        } catch (SQLException e) {
            throw new DaoException("Can't update a record in role_in_film with id: " + entity.getId(), e);
        }
    }

    /**
     * This method delete role in film in database by primary key
     * @param id primary key of entity that need to delete
     * @throws DaoException
     */
    @Override
    public void delete(Integer id) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_DELETE_ROLE_IN_FILM, id);
        } catch (SQLException e) {
            throw new DaoException("Can't delete a record from table role_in_film with id: " + id, e);
        }
    }

    protected static class RoleInFilmRowMapper implements RowMapper<RoleInFilm> {

        @Override
        public RoleInFilm mapRow(ResultSet rs) throws SQLException {
            RoleInFilm roleInFilm = new RoleInFilm();
            roleInFilm.setId(rs.getInt(1));
            roleInFilm.setName(rs.getString(2));
            return roleInFilm;
        }
    }
}
