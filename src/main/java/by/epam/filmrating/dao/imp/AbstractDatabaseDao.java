package by.epam.filmrating.dao.imp;

import by.epam.filmrating.dao.util.JdbcTemplate;

import java.sql.Connection;

/**
 * Class AbstractDatabaseDao contains fields that necessary for work with database
 *  *
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-06
 *
 * @see java.sql.Connection
 * @see by.epam.filmrating.dao.util.JdbcTemplate
 */
public abstract class AbstractDatabaseDao {


    protected Connection connection;
    protected JdbcTemplate jdbcTemplate;

    public AbstractDatabaseDao(Connection connection) {
        this.connection = connection;
        this.jdbcTemplate = new JdbcTemplate(connection);
    }
}
