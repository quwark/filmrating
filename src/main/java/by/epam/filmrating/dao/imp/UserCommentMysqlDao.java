package by.epam.filmrating.dao.imp;

import by.epam.filmrating.dao.UserCommentDao;
import by.epam.filmrating.dao.UserDao;
import by.epam.filmrating.dao.util.RowMapper;
import by.epam.filmrating.domain.User;
import by.epam.filmrating.domain.UserComment;
import by.epam.filmrating.exception.DaoException;
import by.epam.filmrating.util.DateUtil;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Class UserCommentMysqlDao contains operations with entity UserComment for MySQL database
 * @see by.epam.filmrating.domain.UserComment
 * @see by.epam.filmrating.dao.util.RowMapper
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-06
 */
public class UserCommentMysqlDao extends AbstractDatabaseDao implements UserCommentDao {

    private static final String SQL_SELECT_ALL_USER_COMMENTS = "SELECT * FROM user_comment";
    private static final String SQL_SELECT_USER_COMMENT_BY_ID = "SELECT * FROM user_comment WHERE id = ?";
    private static final String SQL_INSERT_USER_COMMENT = "INSERT INTO user_comment(user_data_id, film_id, text, " +
            "comment_time) VALUES(?, ?, ?, ?)";
    private static final String SQL_UPDATE_USER_COMMENT = "UPDATE user_comment SET user_data_id = ?, film_id = ?, " +
            "text = ?, comment_time = ? WHERE id = ?";
    private static final String SQL_DELETE_BY_ID = "DELETE FROM user_comment WHERE id = ?";
    private static final String SQL_SELECT_USER_COMMENTS_BY_USER_ID = "SELECT * FROM user_comment WHERE " +
            "user_data_id = ?";
    private static final String SQL_SELECT_USER_COMMENTS_BY_FILM_ID = "SELECT * FROM user_comment WHERE film_id = ?";
    private static final String SQL_SELECT_USER_COMMENTS_BY_FILM_AND_USER_ID = "SELECT * FROM user_comment WHERE " +
            "film_id = ? AND user_data_id = ?";
    private static final String SQL_DELETE_USER_COMMENTS_BY_FILM_ID = "DELETE FROM user_comment WHERE film_id = ?";
    private static final String SQL_DELETE_USER_COMMENTS_BY_USER_ID = "DELETE FROM user_comment WHERE user_data_id = ?";
    private static final String SQL_DELETE_USER_COMMENTS_BY_FILM_AND_USER_ID = "DELETE FROM user_comment WHERE " +
            "film_id = ? AND user_data_id = ?";


    private static final UserCommentRowMapper rowMapper = new UserCommentRowMapper();

    public UserCommentMysqlDao(Connection connection) {
        super(connection);
    }

    /**
     * This method find all user comments in database
     * @return list of all user comments
     * @throws DaoException
     */
    @Override
    public List<UserComment> findAll() throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_ALL_USER_COMMENTS, rowMapper);
        } catch (SQLException e) {
            throw new DaoException("Can't select all records from table user_comment", e);
        }
    }

    /**
     * This method find user comment in database by primary ket
     * @param id primary key
     * @return user comment with this primary key or null if doesn't exist
     * @throws DaoException
     */
    @Override
    public UserComment findById(Integer id) throws DaoException {
        try {
            return jdbcTemplate.executeFindOne(SQL_SELECT_USER_COMMENT_BY_ID, rowMapper, id);
        } catch (SQLException e) {
            throw new DaoException("Can't select a record from table user_comment with id: " + id, e);
        }
    }


    /**
     * This method create new user comment in database
     * @param entity object to save
     * @throws DaoException
     */
    @Override
    public void create(UserComment entity) throws DaoException {
        try {
            int id = jdbcTemplate.executeInsertQuery(SQL_INSERT_USER_COMMENT, entity.getUserId(), entity.getFilmId(),
                    entity.getText(), DateUtil.convertFromUtilDateToSqlTimestamp(entity.getCommentTime()));
            entity.setId(id);
        } catch (SQLException e) {
            throw new DaoException("Can't create new record in table user_comment", e);
        }
    }

    /**
     * This method update user comment in database
     * @param entity object to update
     * @throws DaoException
     */
    @Override
    public void update(UserComment entity) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_UPDATE_USER_COMMENT, entity.getUserId(), entity.getFilmId(),
                    entity.getText(), DateUtil.convertFromUtilDateToSqlTimestamp(entity.getCommentTime()), entity.getId());
        } catch (SQLException e) {
            throw new DaoException("Can't update a record in table user_comment with id: " + entity.getId(), e);
        }
    }

    /**
     * This method delete user comment from database by primary key
     * @param id primary key of entity that need to delete
     * @throws DaoException
     */
    @Override
    public void delete(Integer id) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_DELETE_BY_ID, id);
        } catch (SQLException e) {
            throw new DaoException("Can't delete a record from table user_comment with id: " + id, e);
        }
    }

    /**
     * This method find user comments in database by userId
     * @param userId primary key of user
     * @return list of user comments belong to user with userId
     * @throws DaoException
     */
    @Override
    public List<UserComment> findUserCommentsByUser(int userId) throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_USER_COMMENTS_BY_USER_ID, rowMapper, userId);
        } catch (SQLException e) {
            throw new DaoException("Can't select user comments with userId: " + userId, e);
        }
    }

    /**
     * This method find user comments in database by filmId
     * @param filmId  primary key of film
     * @return list of user comments belong to film with filmId
     * @throws DaoException
     */
    @Override
    public List<UserComment> findUserCommentsByFilm(int filmId) throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_USER_COMMENTS_BY_FILM_ID, rowMapper, filmId);
        } catch (SQLException e) {
            throw new DaoException("Can't select user comments with filmId: " + filmId, e);
        }
    }

    /**
     * This method find user comments in database by filmId and userId
     * @param filmId primary key of film
     * @param userId primary key of user
     * @return list of user comments belong to film with filmId and userId
     * @throws DaoException
     */
    @Override
    public List<UserComment> findUserCommentsByFilmAndUser(int filmId, int userId) throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_USER_COMMENTS_BY_FILM_AND_USER_ID, rowMapper);
        } catch (SQLException e) {
            throw new DaoException("Can't select user comments with filmId: " + filmId + " and userId: " + userId, e);
        }
    }

    /**
     * This method delete user comments in database by filmId
     * @param filmId primary key of film to delete all comments that belong to this film
     * @throws DaoException
     */
    @Override
    public void deleteAllUserCommentsFromFilm(int filmId) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_DELETE_USER_COMMENTS_BY_FILM_ID, filmId);
        } catch (SQLException e) {
            throw new DaoException("Can't remove user comments from film with id: " + filmId, e);
        }
    }

    /**
     * This method delete user comments in database by userId
     * @param userId primary key of user to delete all comments that belong to this user
     * @throws DaoException
     */
    @Override
    public void deleteAllUserCommentsByUser(int userId) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_DELETE_USER_COMMENTS_BY_USER_ID, userId);
        } catch (SQLException e) {
            throw new DaoException("Can't remove user comments from user with id: " + userId, e);
        }
    }

    /**
     * This method delete user comments in database by filmId and userId
     * @param filmId primary key of film
     * @param userId primary key of user
     * @throws DaoException
     */
    @Override
    public void deleteAllUserCommentsByFilmAndUser(int filmId, int userId) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_DELETE_USER_COMMENTS_BY_FILM_AND_USER_ID, filmId, userId);
        } catch (SQLException e) {
            throw new DaoException("Can't remove user comment from film with id: " + filmId + " and userId: " +
                    userId, e);
        }
    }


    protected static class UserCommentRowMapper implements RowMapper<UserComment> {

        @Override
        public UserComment mapRow(ResultSet rs) throws SQLException {
            UserComment userComment = new UserComment();
            userComment.setId(rs.getInt(1));
            userComment.setText(rs.getString(2));
            Date date = DateUtil.convertFromTimestampToUtilDate(rs.getTimestamp(3));
            userComment.setCommentTime(date);
            userComment.setUserId(rs.getInt(4));
            userComment.setFilmId(rs.getInt(5));
            return userComment;
        }
    }
}
