package by.epam.filmrating.dao.imp;

import by.epam.filmrating.dao.UserDao;
import by.epam.filmrating.dao.util.RowMapper;
import by.epam.filmrating.domain.User;
import by.epam.filmrating.domain.UserRole;
import by.epam.filmrating.domain.UserStatus;
import by.epam.filmrating.exception.DaoException;
import by.epam.filmrating.util.DateUtil;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Class UserMysqlDao contains operations with entity User for MySQL database
 * @see by.epam.filmrating.domain.User
 * @see by.epam.filmrating.dao.util.RowMapper
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-07
 */

public class UserMysqlDao extends AbstractDatabaseDao implements UserDao {

    private static final String SQL_SELECT_ALL_USERS = "SELECT * FROM user_data";
    private static final String SQL_SELECT_USER_BY_ID = "SELECT * FROM user_data WHERE id = ?";
    private static final String SQL_INSERT_USER = "INSERT INTO user_data(login, password, email, role, rating," +
            " status, registered) VALUES (?, ?, ?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE_USER = "UPDATE user_data SET login = ?, password = ?, email = ?, role = ?," +
            "rating = ?, status = ?, registered = ? WHERE id = ?";
    private static final String SQL_DELETE_USER_BY_ID = "DELETE FROM user_data WHERE id = ?";
    private static final String SQL_SELECT_USER_BY_LOGIN = "SELECT * FROM user_data WHERE login = ?";
    private static final String SQL_SELECT_USER_BY_EMAIL = "SELECT * FROM user_data WHERE email = ?";
    private static final String SQL_SELECT_USERS_BY_ROLE = "SELECT * FROM user_data WHERE role = ?";
    private static final String SQL_SELECT_USERS_BY_STATUS = "SELECT * FROM user_data WHERE status = ?";

    private static final UserRowMapper rowMapper = new UserRowMapper();

    public UserMysqlDao(Connection connection) {
        super(connection);
    }

    /**
     * This method find all users in database
     * @return list of all users
     * @throws DaoException
     */
    @Override
    public List<User> findAll() throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_ALL_USERS, rowMapper);
        } catch (SQLException e) {
            throw new DaoException("Can't select all records from table user_data", e);
        }
    }

    /**
     * This method find user by primary key
     * @param id primary key
     * @return user with this primary key or null if doesn't exist
     * @throws DaoException
     */
    @Override
    public User findById(Integer id) throws DaoException {
        try {
           return jdbcTemplate.executeFindOne(SQL_SELECT_USER_BY_ID, rowMapper, id);
        } catch (SQLException e) {
            throw new DaoException("Can't select a record from table user_data with id: " + id, e);
        }
    }

    /**
     * This method create new user in database
     * @param entity object to save
     * @throws DaoException
     */
    @Override
    public void create(User entity) throws DaoException {
        try {
            int id = jdbcTemplate.executeInsertQuery(SQL_INSERT_USER, entity.getLogin(), entity.getPassword(),
                    entity.getEmail(), entity.getRole().ordinal(), entity.getRating(), entity.getStatus().ordinal(),
                    DateUtil.convertFromUtilDateToSqlTimestamp(entity.getRegistrationDate()));
            entity.setId(id);
        } catch (SQLException e) {
            throw new DaoException("Can't create new record in table user_data", e);
        }
    }

    /**
     * This method update user in database
     * @param entity object to update
     * @throws DaoException
     */
    @Override
    public void update(User entity) throws DaoException {
        try {
            jdbcTemplate.executeInsertQuery(SQL_UPDATE_USER, entity.getLogin(), entity.getPassword(),
                    entity.getEmail(), entity.getRole().ordinal(), entity.getRating(), entity.getStatus().ordinal(),
                    DateUtil.convertFromUtilDateToSqlTimestamp(entity.getRegistrationDate()), entity.getId());
        } catch (SQLException e) {
            throw new DaoException("Can't update a record in table user_data with id: " + entity.getId(), e);
        }
    }

    /**
     * This method delete user from database by primary key
     * @param id primary key of entity that need to delete
     * @throws DaoException
     */
    @Override
    public void delete(Integer id) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_DELETE_USER_BY_ID, id);
        } catch (SQLException e) {
            throw new DaoException("Can't delete a record from table user_data with id: " + id, e);
        }
    }

    /**
     * This method find user in database by login
     * @param login user login to find user
     * @return user with this login or null if doesn't exits
     * @throws DaoException
     */
    @Override
    public User findByLogin(String login) throws DaoException {
        try {
            return jdbcTemplate.executeFindOne(SQL_SELECT_USER_BY_LOGIN, rowMapper, login);
        } catch (SQLException e) {
            throw new DaoException("Can't select a record from table user_data with login: " + login, e);
        }
    }

    /**
     * This method find user in database by email
     * @param email user email to find user
     * @return user with this email or null if doesn't exits
     * @throws DaoException
     */
    @Override
    public User findByEmail(String email) throws DaoException {
        try {
            return jdbcTemplate.executeFindOne(SQL_SELECT_USER_BY_EMAIL, rowMapper, email);
        } catch (SQLException e) {
            throw new DaoException("Can't select a record from table user_data with email: " + email, e);
        }
    }

    /**
     * This method find users in database by role
     * @param role role of user
     * @return list of users with this role
     * @throws DaoException
     */
    @Override
    public List<User> findByRole(UserRole role) throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_USERS_BY_ROLE, rowMapper, role.ordinal());
        } catch (SQLException e) {
            throw new DaoException("Can't select records from table user_data by role: " + role, e);
        }
    }

    /**
     * This method find users in database by status
     * @param status status of user
     * @return list of users with this status
     * @throws DaoException
     */
    @Override
    public List<User> findByStatus(UserStatus status) throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_USERS_BY_STATUS, rowMapper, status.ordinal());
        } catch (SQLException e) {
            throw new DaoException("Can't select records from table user_data status" + status, e);
        }
    }

    protected static class UserRowMapper implements RowMapper<User> {

        @Override
        public User mapRow(ResultSet rs) throws SQLException {
            User user = new User();
            user.setId(rs.getInt(1));
            user.setLogin(rs.getString(2));
            user.setPassword(rs.getString(3));
            user.setEmail(rs.getString(4));

            int role = rs.getInt(5);
            UserRole[] roles = UserRole.values();
            if(role >= 0 && role < roles.length) {
                user.setRole(roles[role]);
            }

            user.setRating(rs.getInt(6));
            byte status = rs.getByte(7);
            UserStatus[] statuses = UserStatus.values();
            if(status >= 0 && status < statuses.length) {
                user.setStatus(statuses[status]);
            }

            Date date = DateUtil.convertFromTimestampToUtilDate(rs.getTimestamp(8));
            user.setRegistrationDate(date);
            return user;
        }
    }
}
