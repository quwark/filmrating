package by.epam.filmrating.dao.imp;

import by.epam.filmrating.dao.GenreDao;
import by.epam.filmrating.dao.util.RowMapper;
import by.epam.filmrating.domain.Genre;
import by.epam.filmrating.exception.DaoException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Class GenreMysqlDao contains operations with entity Genre for MySQL database
 * @see by.epam.filmrating.domain.Genre
 * @see by.epam.filmrating.dao.util.RowMapper
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-06
 */
public class GenreMysqlDao extends AbstractDatabaseDao implements GenreDao {

    private static final String SQL_SELECT_ALL_GENRES = "SELECT * FROM genre";
    private static final String SQL_SELECT_GENRE_BY_ID = "SELECT * FROM genre WHERE id = ?";
    private static final String SQL_INSERT_GENRE = "INSERT INTO genre(name) VALUES(?)";
    private static final String SQL_UPDATE_GENRE = "UPDATE genre SET name = ? WHERE id = ?";
    private static final String SQL_DELETE_GENRE = "DELETE FROM genre WHERE id = ?";
    private static final String SQL_SELECT_ALL_GENRES_BY_FILM_ID = "SELECT * FROM genre INNER JOIN genre_in_film ON " +
            "genre.id = genre_in_film.genre_id WHERE genre_in_film.film_id = ?";
    private static final GenreRowMapper rowMapper = new GenreRowMapper();

    public GenreMysqlDao(Connection connection) {
        super(connection);
    }

    /**
     * This method find all genres in database
     * @return list of all genres
     * @throws DaoException
     */
    @Override
    public List<Genre> findAll() throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_ALL_GENRES, rowMapper);
        } catch (SQLException e) {
            throw new DaoException("Can't select all records from table genre", e);
        }
    }

    /**
     * This method find  genre in database by primary key
     * @param id primary key
     * @return genre with this primary key or null if doesn't exist
     * @throws DaoException
     */
    @Override
    public Genre findById(Integer id) throws DaoException {
        try {
            return jdbcTemplate.executeFindOne(SQL_SELECT_GENRE_BY_ID, rowMapper, id);
        } catch (SQLException e) {
            throw new DaoException("Can't select a record from table genre with id: " + id, e);
        }
    }

    /**
     * This method create new genre in database
     * @param entity object to save
     * @throws DaoException
     */
    @Override
    public void create(Genre entity) throws DaoException {
        try {
            int id = jdbcTemplate.executeInsertQuery(SQL_INSERT_GENRE, entity.getName());
            entity.setId(id);
        } catch (SQLException e) {
            throw new DaoException("Can't create new record in table genre", e);
        }
    }

    /**
     * This method update genre in database
     * @param entity object to update
     * @throws DaoException
     */
    @Override
    public void update(Genre entity) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_UPDATE_GENRE, entity.getName(), entity.getId());
        } catch (SQLException e) {
            throw new DaoException("Can't update a record in genre film with id: " + entity.getId(), e);
        }
    }

    /**
     * This method delete genre in database by primary key
     * @param id primary key of entity that need to delete
     * @throws DaoException
     */
    @Override
    public void delete(Integer id) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_DELETE_GENRE, id);
        } catch (SQLException e) {
            throw new DaoException("Can't delete a record in table genre with id: " + id, e);
        }
    }

    /**
     * This method find genres in database by filmId
     * @param filmId primary key of film
     * @return
     * @throws DaoException
     */
    @Override
    public List<Genre> findGenresByFilm(int filmId) throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_ALL_GENRES_BY_FILM_ID, rowMapper, filmId);
        } catch (SQLException e) {
            throw new DaoException("Can't select all genres from film with id: " + filmId, e);
        }
    }

    protected static class GenreRowMapper implements RowMapper<Genre> {

        @Override
        public Genre mapRow(ResultSet rs) throws SQLException {
            Genre genre = new Genre();
            genre.setId(rs.getInt(1));
            genre.setName(rs.getString(2));
            return genre;
        }
    }
}
