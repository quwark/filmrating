package by.epam.filmrating.dao.imp;

import by.epam.filmrating.dao.PersonDao;
import by.epam.filmrating.dao.util.RowMapper;
import by.epam.filmrating.domain.Person;
import by.epam.filmrating.exception.DaoException;
import by.epam.filmrating.util.DateUtil;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Class PersonMysqlDao contains operations with entity Person for MySQL database
 * @see by.epam.filmrating.domain.Person
 * @see by.epam.filmrating.dao.util.RowMapper
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-06
 */
public class PersonMysqlDao extends AbstractDatabaseDao implements PersonDao {

    private static final String SQL_SELECT_ALL_PERSONS = "SELECT * FROM person";
    private static final String SQL_SELECT_PERSON_BY_ID = "SELECT * FROM person WHERE id = ?";
    private static final String SQL_INSERT_PERSON = "INSERT INTO person(first_name, last_name, birth_date) " +
            "VALUES (?, ?, ?)";
    private static final String SQL_UPDATE_PERSON = "UPDATE person SET first_name = ?, last_name = ?, birth_date = ? " +
            "WHERE id = ?";
    private static final String SQL_DELETE_PERSON = "DELETE FROM person WHERE id = ?";
    private static final String SQL_SELECT_PERSON_BY_FILM_ID = "SELECT * FROM person INNER JOIN person_in_film " +
            "ON person.id = person_in_film.person_id WHERE person_in_film.film_id = ?";
    private static final String SQL_SELECT_PERSON_BY_FILM_AND_ROLE_ID = "SELECT * FROM person INNER JOIN" +
            " person_in_film ON person.id = person_in_film.person_id WHERE person_in_film.film_id = ? AND " +
            "person_in_film.role_in_film_id = ?";
    private static final String SQL_SELECT_PERSON_BY_FIRST_AND_LAST_NAME = "SELECT * FROM person WHERE first_name = ?" +
            " AND last_name = ?";

    private static final PersonRowMapper rowMapper = new PersonRowMapper();

    public PersonMysqlDao(Connection connection) {
        super(connection);
    }

    /**
     * This method find all persons in database
     * @return list of all persons
     * @throws DaoException
     */
    @Override
    public List<Person> findAll() throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_ALL_PERSONS, rowMapper);
        } catch (SQLException e) {
            throw new DaoException("Can't select all records from table person", e);
        }
    }

    /**
     * This method find person in database by primary key
     * @param id primary key
     * @return person with this primary key or null if doesn't exist
     * @throws DaoException
     */
    @Override
    public Person findById(Integer id) throws DaoException {
        try {
            return jdbcTemplate.executeFindOne(SQL_SELECT_PERSON_BY_ID, rowMapper, id);
        } catch (SQLException e) {
            throw new DaoException("Can't select a record from table person with id: " + id, e);
        }
    }

    /**
     * This method create new person in database
     * @param entity object to save
     * @throws DaoException
     */
    @Override
    public void create(Person entity) throws DaoException {
        try {
            int id = jdbcTemplate.executeInsertQuery(SQL_INSERT_PERSON, entity.getFirstName(), entity.getLastName(),
                    DateUtil.convertFromUtilToSqlDate(entity.getBirthDate()));
            entity.setId(id);
        } catch (SQLException e) {
            throw new DaoException("Can't create new record in table person", e);
        }
    }

    /**
     * This method update person in database
     * @param entity object to update
     * @throws DaoException
     */
    @Override
    public void update(Person entity) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_UPDATE_PERSON, entity.getFirstName(), entity.getLastName(),
                    DateUtil.convertFromUtilToSqlDate(entity.getBirthDate()), entity.getId());
        } catch (SQLException e) {
            throw new DaoException("Can't update a record in person with id: " + entity.getId(), e);
        }
    }

    /**
     * This method delete person in database by primary key
     * @param id primary key of entity that need to delete
     * @throws DaoException
     */
    @Override
    public void delete(Integer id) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_DELETE_PERSON, id);
        } catch (SQLException e) {
            throw new DaoException("Can't delete a record in table person with id: " + id, e);
        }
    }

    /**
     * This method find all persons in database by filmId
     * @param filmId primary key of film to select persons that connected with this film
     * @return list of persons that belong to film with this filmId
     * @throws DaoException
     */
    @Override
    public List<Person> findPersonsByFilm(int filmId) throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_PERSON_BY_FILM_ID, rowMapper, filmId);
        } catch (SQLException e) {
            throw new DaoException("Can't select all persons from film with id: " + filmId, e);
        }
    }

    /**
     * This method find all persons in database by filmId and roleId
     * @param filmId primary key of film to select persons that connected with this film
     * @param roleId primary key of person role
     * @return list of persons that belong to this filmId and have this roleId
     * @throws DaoException
     */
    @Override
    public List<Person> findPersonByFilmAndRole(int filmId, int roleId) throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_PERSON_BY_FILM_AND_ROLE_ID, rowMapper, filmId, roleId);
        } catch (SQLException e) {
            throw new DaoException("Can't select persons with roleId: " + roleId + "from film with id: " + filmId, e);
        }
    }

    /**
     * This method find a person that have this firstName and lastName
     * @param firstName first name of person
     * @param lastName second name of person
     * @return person with this first and last name or null if doesn't exist
     * @throws DaoException
     */
    @Override
    public Person findByFirstAndLastName(String firstName, String lastName) throws DaoException {
        try {
            return jdbcTemplate.executeFindOne(SQL_SELECT_PERSON_BY_FIRST_AND_LAST_NAME, rowMapper, firstName, lastName);
        } catch (SQLException e) {
            throw new DaoException("Can't select a record from table person with firsName: " + firstName +
                    " and lastName: " + lastName, e);
        }
    }

    protected static class PersonRowMapper implements RowMapper<Person> {

        @Override
        public Person mapRow(ResultSet rs) throws SQLException {
            Person person = new Person();
            person.setId(rs.getInt(1));
            person.setFirstName(rs.getString(2));
            person.setLastName(rs.getString(3));
            Date date = DateUtil.convertFromSqlToUtilDate(rs.getDate(4));
            person.setBirthDate(date);
            return person;
        }
    }
}
