package by.epam.filmrating.dao.imp;

import by.epam.filmrating.dao.UserMarkDao;
import by.epam.filmrating.dao.util.RowMapper;
import by.epam.filmrating.domain.UserMark;
import by.epam.filmrating.exception.DaoException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Class UserMarkMysqlDao contains operations with entity UserMark for MySQL database
 * @see by.epam.filmrating.domain.UserMark
 * @see by.epam.filmrating.dao.util.RowMapper
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-07
 */
public class UserMarkMysqlDao extends AbstractDatabaseDao implements UserMarkDao {

    private static final String SQL_SELECT_ALL_USER_MARKS = "SELECT * FROM user_mark";
    private static final String SQL_SELECT_USER_MARK_BY_ID = "SELECT * FROM user_mark WHERE id = ?";
    private static final String SQL_INSERT_USER_MARK = "INSERT INTO user_mark(user_data_id, film_id, mark, consider) " +
            "VALUES (?, ?, ?, ?)";
    private static final String SQL_UPDATE_USER_MARK = "UPDATE user_mark SET user_data_id = ?, film_id = ?, mark = ?," +
            " consider = ? WHERE id = ?";
    private static final String SQL_DELETE_USER_MARK_BY_ID = "DELETE FROM user_mark WHERE id = ?";
    private static final String SQL_SELECT_USER_MARK_BY_USER_ID = "SELECT * FROM user_mark WHERE user_data_id = ?";
    private static final String SQL_SELECT_USER_MARK_BY_FILM_ID = "SELECT * FROM user_mark WHERE film_id = ?";
    private static final String SQL_SELECT_USER_MARK_BY_FILM_AND_USER_ID = "SELECT * FROM user_mark WHERE film_id = ? " +
            "AND user_data_id = ?";
    private static final String SQL_DELETE_USER_MARKS_BY_USER_ID = "DELETE FROM user_mark WHERE user_data_id = ?";
    private static final String SQL_DELETE_USER_MARKS_BY_FILM_ID = "DELETE FROM user_mark WHERE film_id = ?";
    private static final String SQL_DELETE_USER_MARK_BY_FILM_AND_USER_ID = "DELETE FROM user_mark WHERE film_id = ? " +
            "AND user_data_id = ?";

    private static final UserMarkRowMapper rowMapper = new UserMarkRowMapper();

    public UserMarkMysqlDao(Connection connection) {
        super(connection);
    }

    /**
     * This method find all user marks in database
     * @return list of all user marks
     * @throws DaoException
     */
    @Override
    public List<UserMark> findAll() throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_ALL_USER_MARKS, rowMapper);
        } catch (SQLException e) {
            throw new DaoException("Can't select all records from table user_mark", e);
        }
    }

    /**
     * This method find user mark in database by primary key
     * @param id primary key
     * @return user mark with this primary key or null if doesn't exist
     * @throws DaoException
     */
    @Override
    public UserMark findById(Integer id) throws DaoException {
        try {
            return jdbcTemplate.executeFindOne(SQL_SELECT_USER_MARK_BY_ID, rowMapper, id);
        } catch (SQLException e) {
            throw new DaoException("Can't select a record from table user_mark with id: " + id, e);
        }
    }

    /**
     * This method create new user mark in database
     * @param entity object to save
     * @throws DaoException
     */
    @Override
    public void create(UserMark entity) throws DaoException {
        try {
            int id = jdbcTemplate.executeInsertQuery(SQL_INSERT_USER_MARK, entity.getUserId(), entity.getFilmId(),
                    entity.getMark(), (entity.isConsider() ? 1 : 0));
            entity.setId(id);
        } catch (SQLException e) {
            throw new DaoException("Can't create new record in table user_mark", e);
        }
    }

    /**
     * This method update user mark in database
     * @param entity object to update
     * @throws DaoException
     */
    @Override
    public void update(UserMark entity) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_UPDATE_USER_MARK, entity.getUserId(), entity.getFilmId(),
                    entity.getMark(),(entity.isConsider() ? 1 : 0), entity.getId());
        } catch (SQLException e) {
            throw new DaoException("Can't update a record in table user_mark with id: " + entity.getId(), e);
        }
    }

    /**
     * This method delete user mark in database by primary key
     * @param id primary key of entity that need to delete
     * @throws DaoException
     */
    @Override
    public void delete(Integer id) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_DELETE_USER_MARK_BY_ID, id);
        } catch (SQLException e) {
            throw new DaoException("Can't delete a record from table user_data with id: " + id, e);
        }
    }

    /**
     * This method find user marks in database by userId
     * @param userId primary key of user to select user marks that belong to this user
     * @return list if user marks that belong to user with userId
     * @throws DaoException
     */
    @Override
    public List<UserMark> findUserMarksByUser(int userId) throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_USER_MARK_BY_USER_ID, rowMapper, userId);
        } catch (SQLException e) {
            throw new DaoException("Can't select user marks with userId: " + userId, e);
        }
    }

    /**
     * This method find user marks in database by filmId
     * @param filmId primary key of film to select user marks that belong to this film
     * @return list if user marks that belong to film with filmId
     * @throws DaoException
     */
    @Override
    public List<UserMark> findUserMarksByFilm(int filmId) throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_USER_MARK_BY_FILM_ID, rowMapper, filmId);
        } catch (SQLException e) {
            throw new DaoException("Can't select user marks with filmId: " + filmId, e);
        }
    }

    /**
     * This method find user mark in database by filmId and userId
     * @param filmId primary key of film
     * @param userId primary key of user
     * @return list if user marks that belong to user with userId and film with filmId
     * @throws DaoException
     */
    @Override
    public UserMark findUserMarkByFilmAndUser(int filmId, int userId) throws DaoException {
        try {
            return jdbcTemplate.executeFindOne(SQL_SELECT_USER_MARK_BY_FILM_AND_USER_ID, rowMapper, filmId, userId);
        } catch (SQLException e) {
            throw new DaoException("Can't select user mark with filmId: " + filmId + " and userId: " + userId, e);
        }
    }

    /**
     * This method delete user marks in database by filmId
     * @param filmId primary key of film to delete all users marks that belong this film
     * @throws DaoException
     */
    @Override
    public void deleteAllUserMarksByFilm(int filmId) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_DELETE_USER_MARKS_BY_FILM_ID, filmId);
        } catch (SQLException e) {
            throw new DaoException("Can't remove user marks from film with id: " + filmId, e);
        }
    }

    /**
     * This method delete user marks in database by userId
     * @param userId primary key of user to delete all users marks that belong this user
     * @throws DaoException
     */
    @Override
    public void deleteAllUserMarksByUser(int userId) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_DELETE_USER_MARKS_BY_USER_ID, userId);
        } catch (SQLException e) {
            throw new DaoException("Can't remove user marks from user with id: " + userId, e);
        }
    }

    /**
     * This method delete user mark in database by filmId and userId
     * @param filmId primary key of film
     * @param userId primary key of user
     * @throws DaoException
     */
    @Override
    public void deleteUserMarkByFilmAndUser(int filmId, int userId) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_DELETE_USER_MARK_BY_FILM_AND_USER_ID, filmId, userId);
        } catch (SQLException e) {
            throw new DaoException("Can't remove user mark from film with id: " + filmId + " and userId: " +
                    userId, e);
        }
    }

    protected static class UserMarkRowMapper implements RowMapper<UserMark> {

        @Override
        public UserMark mapRow(ResultSet rs) throws SQLException {
            UserMark userMark = new UserMark();
            userMark.setId(rs.getInt(1));
            userMark.setMark(rs.getInt(2));
            userMark.setUserId(rs.getInt(3));
            userMark.setFilmId(rs.getInt(4));
            userMark.setConsider(rs.getByte(5) == 1);
            return userMark;
        }
    }
}
