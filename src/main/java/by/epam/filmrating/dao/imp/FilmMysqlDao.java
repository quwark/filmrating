package by.epam.filmrating.dao.imp;

import by.epam.filmrating.dao.FilmDao;
import by.epam.filmrating.dao.util.RowMapper;
import by.epam.filmrating.domain.Film;
import by.epam.filmrating.exception.DaoException;
import by.epam.filmrating.util.DateUtil;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Class FilmMysqlDao contains operations with entity Film for MySQL database
 * @see by.epam.filmrating.domain.Film
 * @see by.epam.filmrating.dao.util.RowMapper
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-06
 */
public class FilmMysqlDao extends AbstractDatabaseDao implements FilmDao {

    private static final String SQL_SELECT_ALL_FILMS = "SELECT * FROM film";
    private static final String SQL_SELECT_FILM_BY_ID = "SELECT * FROM film WHERE id = ?";
    private static final String SQL_INSERT_FILM = "INSERT INTO film(name, description, img_url, year, added)" +
            " VALUES(?, ?, ?, ?, ?)";
    private static final String SQL_UPDATE_FILM = "UPDATE film SET name = ?, description = ?, img_url = ?, year = ?, " +
            "added = ? WHERE id = ?";
    private static final String SQL_DELETE_FILM_BY_ID = "DELETE FROM film WHERE id = ?";
    private static final String SQL_INSERT_GENRE_IN_FILM = "INSERT INTO genre_in_film(film_id, genre_id) VALUES(?, ?)";
    private static final String SQL_REMOVE_ALL_GENRE_FROM_FILM = "DELETE FROM genre_in_film WHERE film_id = ?";
    private static final String SQL_REMOVE_GENRE_FROM_FILM = "DELETE FROM genre_in_film WHERE film_id = ? AND " +
            "genre_id = ?";
    private static final String SQL_SELECT_FILM_BY_GENRE_ID = "SELECT * FROM film INNER JOIN genre_in_film ON\n" +
            "film.id = genre_in_film.film_id WHERE genre_in_film.genre_id = ?";
    private static final String SQL_INSERT_PERSON_IN_FILM = "INSERT INTO person_in_film(role_in_film_id, film_id, " +
            "person_id) VALUES(?, ?, ?)";
    private static final String SQL_REMOVE_PERSON_FROM_FILM = "DELETE FROM person_in_film WHERE film_id = ? AND " +
            "person_id = ?";
    private static final String SQL_REMOVE_PERSON_FROM_FILM_WITH_ROLE = "DELETE FROM person_in_film WHERE " +
            "role_in_film_id = ? AND film_id = ? AND person_id = ?";
    private static final String SQL_REMOVE_ALL_PERSONS_FROM_FILM = "DELETE FROM person_in_film WHERE film_id = ?";
    private static final String SQL_SELECT_ALL_FILMS_BY_PERSON_ID = "SELECT * FROM film INNER JOIN person_in_film ON " +
            "film.id = person_in_film.film_id WHERE person_in_film.person_id = ?";
    private static final String SQL_SELECT_ALL_FILMS_BY_PERSON_AND_ROLE_ID = "SELECT * FROM film INNER JOIN " +
            "person_in_film ON film.id = person_in_film.film_id WHERE person_in_film.person_id = ? AND " +
            "person_in_film.role_in_film_id = ?";
    private static final String SQL_SELECT_FILM_BY_NAME_AND_YEAR = "SELECT * FROM film WHERE name = ? AND year = ?";


    private static final FilmRowMapper rowMapper = new FilmRowMapper();

    public FilmMysqlDao(Connection connection) {
        super(connection);
    }

    /**
     * This method find all films in database
     * @return list of all films
     * @throws DaoException
     */
    @Override
    public List<Film> findAll() throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_ALL_FILMS, rowMapper);
        } catch (SQLException e) {
            throw new DaoException("Can't select all records from table film", e);
        }
    }

    /**
     * This method find film in database by primary key
     * @param id primary key
     * @return film with thi primary key or null if doesn't exist
     * @throws DaoException
     */
    @Override
    public Film findById(Integer id) throws DaoException {
        try {
            return jdbcTemplate.executeFindOne(SQL_SELECT_FILM_BY_ID, rowMapper, id);
        } catch (SQLException e) {
            throw new DaoException("Can't select a record from table film with id: " + id, e);
        }
    }

    /**
     * This method create new film in database
     * @param entity object to save
     * @throws DaoException
     */
    @Override
    public void create(Film entity) throws DaoException {
        try {
            int id = jdbcTemplate.executeInsertQuery(SQL_INSERT_FILM, entity.getName(), entity.getDescription(),
                    entity.getImgURL(), entity.getYear(),
                    DateUtil.convertFromUtilDateToSqlTimestamp(entity.getAddedDate()));
            entity.setId(id);
        } catch (SQLException e) {
            throw new DaoException("Can't create new record in table film", e);
        }
    }

    /**
     * This method update films in database
     * @param entity object to update
     * @throws DaoException
     */
    @Override
    public void update(Film entity) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_UPDATE_FILM, entity.getName(), entity.getDescription(),
                    entity.getImgURL(), entity.getYear(),
                    DateUtil.convertFromUtilDateToSqlTimestamp(entity.getAddedDate()),
                    entity.getId());
        } catch (SQLException e) {
            throw new DaoException("Can't update a record in table film with id: " + entity.getId(), e);
        }
    }

    /**
     * This method delete film in database by primary key
     * @param id primary key of entity that need to delete
     * @throws DaoException
     */
    @Override
    public void delete(Integer id) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_DELETE_FILM_BY_ID, id);
        } catch (SQLException e) {
            throw new DaoException("Can't delete a record in table film with id: " + id, e);
        }
    }

    /**
     * This method add genres to film in database
     * @param filmId primary key of film to add genres
     * @param genres list of genres primary keys for adding to film
     * @throws DaoException
     */
    @Override
    public void addGenresToFilm(int filmId, List<Integer> genres) throws DaoException {
        try(PreparedStatement ps = connection.prepareStatement(SQL_INSERT_GENRE_IN_FILM)) {
            for(Integer genre : genres) {
                ps.setInt(1, filmId);
                ps.setInt(2, genre);
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DaoException("Can't add genres to film with filmId: " + filmId, e);
        }
    }

    /**
     * This method remove genres from film in database
     * @param filmId primary key of film to remove genres
     * @throws DaoException
     */
    @Override
    public void removeAllGenresFromFilm(int filmId) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_REMOVE_ALL_GENRE_FROM_FILM, filmId);
        } catch (SQLException e) {
            throw new DaoException("Can't remove all genres from film with filmId: " + filmId, e);
        }
    }

    /**
     * This method remove genre from film in database
     * @param filmId primary key of film to remove genre
     * @param genreId primary key of genre to remove
     * @throws DaoException
     */
    @Override
    public void removeGenreFromFilm(int filmId, int genreId) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_REMOVE_GENRE_FROM_FILM, filmId, genreId);
        } catch (SQLException e) {
            throw new DaoException("Can't remove genre with id: " + genreId + " from film with filmId: " + filmId, e);
        }
    }

    /**
     * This method fin films by genre in database
     * @param genreId primary key of genre to select films that have this key
     * @return list of films that have this genreId
     * @throws DaoException
     */
    @Override
    public List<Film> findByGenre(int genreId) throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_FILM_BY_GENRE_ID, rowMapper, genreId);
        } catch (SQLException e) {
            throw new DaoException("Can't select all films with genreId: " + genreId, e);
        }
    }

    /**
     * This method add person to film in database
     * @param filmId primary key of film to add person
     * @param personId primary key of person
     * @param roleId primary key of person role in this film
     * @throws DaoException
     */
    @Override
    public void addPersonToFilm(int filmId, int personId, int roleId) throws DaoException {
        try {
            jdbcTemplate.executeInsertQuery(SQL_INSERT_PERSON_IN_FILM, roleId, filmId, personId);
        } catch (SQLException e) {
            throw new DaoException("Can't add person with id: " + personId + " and roleId " + roleId +
                    " to film with id: " + filmId, e);
        }
    }

    /**
     * This method add person to film in database
     * @param filmId primary key of film to add persons
     * @param persons list of primary keys of persons
     * @param roleId primary key of person role in this film
     * @throws DaoException
     */
    @Override
    public void addPersonsToFilm(int filmId, List<Integer> persons, int roleId) throws DaoException {
        try(PreparedStatement ps = connection.prepareStatement(SQL_INSERT_PERSON_IN_FILM)) {
            for(Integer personId : persons) {
                ps.setInt(1, roleId);
                ps.setInt(2, filmId);
                ps.setInt(3, personId);
                ps.addBatch();
            }
            ps.executeBatch();
        } catch (SQLException e) {
            throw new DaoException("Can't add persons with roleId " + roleId + " to film with id: " + filmId, e);
        }
    }

    /**
     * This method remove person from film in database
     * @param filmId primary key of film to remove person
     * @param personId primary key of person
     * @throws DaoException
     */
    @Override
    public void removePersonFromFilm(int filmId, int personId) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_REMOVE_PERSON_FROM_FILM, filmId, personId);
        } catch (SQLException e) {
            throw new DaoException("Can't remove person with id: " + personId + " from film with filmId: " + filmId, e);
        }
    }

    /**
     * This method remove person from film by role in database
     * @param filmId primary key of film to remove person
     * @param personId primary key of person
     * @param roleId primary key of person role
     * @throws DaoException
     */
    @Override
    public void removePersonFromFilm(int filmId, int personId, int roleId) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_REMOVE_PERSON_FROM_FILM_WITH_ROLE, roleId, filmId, personId);
        } catch (SQLException e) {
            throw new DaoException("Can't remove person with id: " + personId + " and roleId: " + roleId +
                    " from film with filmId: " + filmId, e);
        }
    }

    /**
     * This method remove persons from film in database
     * @param filmId  primary key of film to remove all persons
     * @throws DaoException
     */
    @Override
    public void removeAllPersonsFromFilm(int filmId) throws DaoException {
        try {
            jdbcTemplate.executeUpdateQuery(SQL_REMOVE_ALL_PERSONS_FROM_FILM, filmId);
        } catch (SQLException e) {
            throw new DaoException("Can't remove all persons from film with filmId: " + filmId, e);
        }
    }

    /**
     * This method find films in database by personId
     * @param personId primary key of person to select all films with this person
     * @return list of films with this personId
     * @throws DaoException
     */
    @Override
    public List<Film> findByPerson(int personId) throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_ALL_FILMS_BY_PERSON_ID, rowMapper, personId);
        } catch (SQLException e) {
            throw new DaoException("Can't select films with personId: " + personId, e);
        }
    }

    /**
     * This method find films in database by personId and roleId
     * @param personId primary key of person to select all films with this person
     * @param roleId primary key of person role
     * @return list of films with this personId and roleId
     * @throws DaoException
     */
    @Override
    public List<Film> findByPersonAndRole(int personId, int roleId) throws DaoException {
        try {
            return jdbcTemplate.executeFind(SQL_SELECT_ALL_FILMS_BY_PERSON_AND_ROLE_ID, rowMapper, personId, roleId);
        } catch (SQLException e) {
            throw new DaoException("Can't select films with personId: " + personId, e);
        }
    }

    @Override
    public Film getFilmByNameAndYear(String name, int year) throws DaoException {
        try {
            return jdbcTemplate.executeFindOne(SQL_SELECT_FILM_BY_NAME_AND_YEAR, rowMapper, name, year);
        } catch (SQLException e) {
            throw new DaoException("Can't select films with name: " + name + "and year: " + year, e);
        }
    }


    protected static class FilmRowMapper implements RowMapper<Film> {

        @Override
        public Film mapRow(ResultSet rs) throws SQLException {
            Film film = new Film();
            film.setId(rs.getInt(1));
            film.setName(rs.getString(2));
            film.setDescription(rs.getString(3));
            film.setImgURL(rs.getString(4));
            film.setYear(rs.getInt(5));
            Date date = DateUtil.convertFromTimestampToUtilDate(rs.getTimestamp(6));
            film.setAddedDate(date);
            return film;
        }
    }
}
