package by.epam.filmrating.dao;

import by.epam.filmrating.domain.UserMark;
import by.epam.filmrating.exception.DaoException;

import java.util.List;

/**
 * Interface UserMarkDao contains operations for entity User
 * @see by.epam.filmrating.domain.UserMark
 *
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-06
 */
public interface UserMarkDao extends GenericDao<UserMark, Integer> {

    /**
     * This method select user marks that have this userId
     * @param userId primary key of user to select user marks that belong to this user
     * @return list of user marks that belong to this user
     * @throws DaoException
     */
    List<UserMark> findUserMarksByUser(int userId) throws DaoException;

    /**
     * This method select user marks that have this filmId
     * @param filmId primary key of film to select user marks that belong to this film
     * @return list of user marks that belong to this film
     * @throws DaoException
     */
    List<UserMark> findUserMarksByFilm(int filmId) throws DaoException;

    /**
     * This method select user mark that have this filmId and userId
     * @param filmId primary key of film
     * @param userId primary key of user
     * @return entity that belong to this film and user or null if doesn't exits
     * @throws DaoException
     */
    UserMark findUserMarkByFilmAndUser(int filmId, int userId) throws DaoException;

    /**
     * This method delete user marks that have this filmId
     * @param filmId primary key of film to delete all users marks that belong this film
     * @throws DaoException
     */
    void deleteAllUserMarksByFilm(int filmId) throws DaoException;

    /**
     * This method delete user marks that have this userId
     * @param userId primary key of user to delete all users marks that belong this user
     * @throws DaoException
     */
    void deleteAllUserMarksByUser(int userId) throws DaoException;

    /**
     * This method delete user comments that belong to this film and user
     * @param filmId primary key of film
     * @param userId primary key of user
     * @throws DaoException
     */
    void deleteUserMarkByFilmAndUser(int filmId, int userId) throws DaoException;
}
