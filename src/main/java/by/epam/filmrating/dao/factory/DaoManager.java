package by.epam.filmrating.dao.factory;

import by.epam.filmrating.dao.*;
import by.epam.filmrating.exception.DaoException;

/**
 * Created by Yura on 08.06.2016.
 */
public interface DaoManager {

    FilmDao getFilmDao();
    GenreDao getGenreDao();
    PersonDao getPersonDao();
    RoleInFilmDao getRoleInFilmDao();
    UserCommentDao getUserCommentDao();
    UserDao getUserDao();
    UserMarkDao getUserMarkDao();

    void beginTransaction() throws DaoException;
    void commit() throws DaoException;
    void rollback() throws DaoException;
    void endTransaction() throws DaoException;
    void close();

}
