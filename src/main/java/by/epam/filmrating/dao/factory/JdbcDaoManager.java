package by.epam.filmrating.dao.factory;

import by.epam.filmrating.dao.*;
import by.epam.filmrating.dao.imp.*;
import by.epam.filmrating.db.ConnectionPool;
import by.epam.filmrating.exception.DaoException;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by Yura on 08.06.2016.
 */
public class JdbcDaoManager implements DaoManager {

    private static final Logger logger = Logger.getLogger(JdbcDaoManager.class);
    private ConnectionPool connectionPool;
    private Connection connection;

    private FilmDao filmDao;
    private GenreDao genreDao;
    private PersonDao personDao;
    private RoleInFilmDao roleInFilmDao;
    private UserCommentDao userCommentDao;
    private UserDao userDao;
    private UserMarkDao userMarkDao;

    public JdbcDaoManager(ConnectionPool connectionPool) {
        this.connectionPool = connectionPool;
    }

    protected Connection getConnection() {
        if(connection == null) {
            try {
                connection = connectionPool.getConnection();
            } catch (SQLException e) {
                throw new DaoException("Can't get instance of connection from db pool", e);
            }
        }
        return connection;
    }

    @Override
    public FilmDao getFilmDao() {
        if(filmDao == null) {
            filmDao = new FilmMysqlDao(getConnection());
        }
        return filmDao;
    }

    @Override
    public GenreDao getGenreDao() {
        if(genreDao == null) {
            genreDao = new GenreMysqlDao(getConnection());
        }
        return genreDao;
    }

    @Override
    public PersonDao getPersonDao() {
        if(personDao == null) {
            personDao = new PersonMysqlDao(getConnection());
        }
        return personDao;
    }

    @Override
    public RoleInFilmDao getRoleInFilmDao() {
        if(roleInFilmDao == null) {
            roleInFilmDao = new RoleInFilmMysqlDao(getConnection());
        }
        return roleInFilmDao;
    }

    @Override
    public UserCommentDao getUserCommentDao() {
        if(userCommentDao == null) {
            userCommentDao = new UserCommentMysqlDao(getConnection());
        }
        return userCommentDao;
    }

    @Override
    public UserDao getUserDao() {
        if(userDao == null) {
            userDao = new UserMysqlDao(getConnection());
        }
        return userDao;
    }

    @Override
    public UserMarkDao getUserMarkDao() {
        if(userMarkDao == null) {
            userMarkDao = new UserMarkMysqlDao(getConnection());
        }
        return userMarkDao;
    }

    @Override
    public void beginTransaction() throws DaoException {
        try {
            getConnection().setAutoCommit(false);
        } catch (SQLException e) {
            logger.error("Can't begin transaction", e);
            throw new DaoException("Can't begin transaction", e);
        }
    }

    @Override
    public void commit() throws DaoException {
        try {
            getConnection().commit();
        } catch (SQLException e) {
            logger.error("Can't commit transaction", e);
            throw new DaoException("Can't commit transaction", e);
        }
    }

    @Override
    public void rollback() throws DaoException {
        try {
            getConnection().rollback();
        } catch (SQLException e) {
            logger.error("Can't rollback transaction", e);
            throw new DaoException("Can't rollback transaction", e);
        }
    }

    @Override
    public void endTransaction() throws DaoException {
        try {
            getConnection().setAutoCommit(true);
        } catch (SQLException e) {
            logger.error("Can't end transaction", e);
            throw new DaoException("Can't end transaction", e);
        }
    }

    @Override
    public void close() {
        try {
            connectionPool.releaseConnection(connection);
        } catch (SQLException e) {
            logger.error("Can't release connection", e);
        }
    }
}
