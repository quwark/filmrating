package by.epam.filmrating.dao.factory;

import by.epam.filmrating.db.ConnectionPool;
import by.epam.filmrating.db.ConnectionPoolImpl;
import by.epam.filmrating.exception.ConnectionPoolException;
import by.epam.filmrating.manager.JdbcConfigManager;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by Yura on 08.06.2016.
 */
public class DaoFactory {

    private static ConnectionPool connectionPool;
    private static ThreadLocal<DaoManager> daoManagerThreadLocal = new ThreadLocal<DaoManager>() {
        @Override
        protected DaoManager initialValue() {
            return new JdbcDaoManager(connectionPool);
        }
    };

    private static ThreadLocal<Boolean> isUsed = new ThreadLocal<Boolean>() {
        @Override
        protected Boolean initialValue() {
            return Boolean.FALSE;
        }
    };

    public static DaoManager createDaoManager() {
        if(connectionPool == null) {
            synchronized (DaoFactory.class) {
                if(connectionPool == null) {
                    init();
                }
            }
        }
        isUsed.set(Boolean.TRUE);
        return daoManagerThreadLocal.get();
    }

    public static void closeCurrentDaoManager() {
        if(isUsedDaoManager()) {
            isUsed.set(Boolean.FALSE);
            daoManagerThreadLocal.get().close();
            daoManagerThreadLocal.remove();
        }
    }

    public static void destroy() {
        if(connectionPool != null) {
            connectionPool.destroy();
            connectionPool = null;
        }
    }

    public static boolean isUsedDaoManager() {
        return isUsed.get();
    }

    public static void init() throws ConnectionPoolException {
        if(connectionPool != null) {
            return;
        }
        try {
            JdbcConfigManager config = JdbcConfigManager.getInstance();

            Class.forName(config.getProperty(JdbcConfigManager.DATABASE_DRIVER_NAME));
            String url = config.getProperty(JdbcConfigManager.DATABASE_URL);
            String user = config.getProperty(JdbcConfigManager.DATABASE_USER);
            String password = config.getProperty(JdbcConfigManager.DATABASE_PASSWORD);

            Integer minSize = Integer.parseInt(config.getProperty(JdbcConfigManager.POOL_MIN_SIZE));
            Integer maxSize = Integer.parseInt(config.getProperty(JdbcConfigManager.POOL_MAX_SIZE));
            Integer initialSize = Integer.parseInt(config.getProperty(JdbcConfigManager.POOL_INITIAL_SIZE));
            Long idleTime = Long.parseLong(config.getProperty(JdbcConfigManager.POOL_MAX_IDLE_TIME));
            Long useTime = Long.parseLong(config.getProperty(JdbcConfigManager.POOL_MAX_USE_TIME));
            Long clearingInterval = Long.parseLong(config.getProperty(JdbcConfigManager.POOL_CLEARING_INTERVAL));

            connectionPool = new ConnectionPoolImpl(url, user, password, minSize, initialSize, maxSize, idleTime,
                    useTime, clearingInterval);
        } catch (Exception e) {
            throw new ConnectionPoolException("Can't start connection pool", e);
        }
    }
}
