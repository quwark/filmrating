package by.epam.filmrating.dao;

import by.epam.filmrating.domain.User;
import by.epam.filmrating.domain.UserRole;
import by.epam.filmrating.domain.UserStatus;
import by.epam.filmrating.exception.DaoException;

import java.util.List;

/**
 * Interface UserDao contains operations for entity User
 * @see by.epam.filmrating.domain.User
 *
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-06
 */
public interface UserDao extends GenericDao<User, Integer> {

    /**
     * This method select user that have this login
     * @param login user login to find user
     * @return user that has this login or null if doesn't exists
     * @throws DaoException
     */
    User findByLogin(String login) throws DaoException;

    /**
     * This method select user that have this email
     * @param email user email to find user
     * @return user that has this email or null if doesn't exists
     * @throws DaoException
     */
    User findByEmail(String email) throws DaoException;

    /**
     * This method select users that have this role
     * @param role role of user
     * @see by.epam.filmrating.domain.UserRole
     * @return list of users that have thi role
     * @throws DaoException
     */
    List<User> findByRole(UserRole role) throws DaoException;

    /**
     * This method select users that have this status
     * @param status status of user
     * @see by.epam.filmrating.domain.UserStatus
     * @return list of users that have this status
     * @throws DaoException
     */
    List<User> findByStatus(UserStatus status) throws DaoException;
}
