package by.epam.filmrating.dao;

import by.epam.filmrating.exception.DaoException;

import java.io.Serializable;
import java.util.List;


/**
 * Interface GenericDao contains main operations with entities
 * @param <T> is object that represent entity
 * @param <K> is object that represent primary key
 *
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-06
 */
public interface GenericDao<T, K extends Serializable> {

    /**
     * This method select all entities
     * @return list of all items
     * @throws DaoException
     */
    List<T> findAll() throws DaoException;

    /**
     * This method select entity by primary key
     * @param id primary key
     * @return entity that has this primary key or null if doesn't exist
     * @throws DaoException
     */
    T findById(K id) throws DaoException;

    /**
     * This method create new entity
     * @param entity object to save
     * @throws DaoException
     */
    void create(T entity) throws DaoException;

    /**
     * This method update entity
     * @param entity object to update
     * @throws DaoException
     */
    void update(T entity) throws DaoException;

    /**
     * This method delete entity by primary key
     * @param id primary key of entity that need to delete
     * @throws DaoException
     */
    void delete(K id) throws DaoException;
}
