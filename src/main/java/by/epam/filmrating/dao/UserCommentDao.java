package by.epam.filmrating.dao;

import by.epam.filmrating.domain.UserComment;
import by.epam.filmrating.exception.DaoException;

import java.util.List;

/**
 * Interface UserCommentDao contains operations for entity UserComment
 * @see by.epam.filmrating.domain.UserComment
 *
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-06
 */
public interface UserCommentDao extends GenericDao<UserComment, Integer> {

    /**
     * This method select user comments that have this userId
     * @param userId primary key of user
     * @return list of user comments that belong to this user
     * @throws DaoException
     */
    List<UserComment> findUserCommentsByUser(int userId) throws DaoException;

    /**
     * This method select user comments that have this filmId
     * @param filmId  primary key of film
     * @return list of user comments that belong to this film
     * @throws DaoException
     */
    List<UserComment> findUserCommentsByFilm(int filmId) throws DaoException;

    /**
     * This method select user comments that have this filmId and userId
     * @param filmId primary key of film
     * @param userId primary key of user
     * @return list of user comments that belong to this film and user
     * @throws DaoException
     */
    List<UserComment> findUserCommentsByFilmAndUser(int filmId, int userId) throws DaoException;

    /**
     * This method delete user comments that have this filmId
     * @param filmId primary key of film to delete all comments that belong to this film
     * @throws DaoException
     */
    void deleteAllUserCommentsFromFilm(int filmId) throws DaoException;

    /**
     * This method delete user comments that have this userId
     * @param userId primary key of user to delete all comments that belong to this user
     * @throws DaoException
     */
    void deleteAllUserCommentsByUser(int userId) throws DaoException;

    /**
     * This method delete all user comments that belong to this film and user
     * @param filmId primary key of film
     * @param userId primary key of user
     * @throws DaoException
     */
    void deleteAllUserCommentsByFilmAndUser(int filmId, int userId) throws DaoException;
}
