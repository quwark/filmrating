package by.epam.filmrating.dao;

import by.epam.filmrating.domain.Film;
import by.epam.filmrating.exception.DaoException;

import java.util.List;

/**
 * Interface FilmDao contains operations for entity Film
 * @see by.epam.filmrating.domain.Film
 *
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-06
 */
public interface FilmDao extends GenericDao<Film, Integer> {

    /**
     * This method add to film, that has this filmId, genres
     * @param filmId primary key of film to add genres
     * @param genres list of genres primary keys for adding to film
     * @throws DaoException
     */
    void addGenresToFilm(int filmId, List<Integer> genres) throws DaoException;

    /**
     * This method remove from film, that has this filmId, all genres
     * @param filmId primary key of film to remove genres
     * @throws DaoException
     */
    void removeAllGenresFromFilm(int filmId) throws DaoException;

    /**
     * This method remove from film, that has this filmId, genre with genreId
     * @param filmId primary key of film to remove genre
     * @param genreId primary key of genre to remove
     * @throws DaoException
     */
    void removeGenreFromFilm(int filmId, int genreId) throws DaoException;

    /**
     * This method select films, that have this genreId
     * @param genreId primary key of genre to select films that have this key
     * @return list of films that have this primary key
     * @throws DaoException
     */
    List<Film> findByGenre(int genreId) throws DaoException;

    /**
     * This method add person with personId to film with filmId and roleId
     * @param filmId primary key of film to add person
     * @param personId primary key of person
     * @param roleId primary key of person role in this film
     * @throws DaoException
     */
    void addPersonToFilm(int filmId, int personId, int roleId) throws DaoException;

    /**
     * This method add persons to film with filmId and roleId
     * @param filmId primary key of film to add persons
     * @param persons list of primary keys of persons
     * @param roleId primary key of person role in this film
     * @throws DaoException
     */
    void addPersonsToFilm(int filmId, List<Integer> persons, int roleId) throws DaoException;

    /**
     * This method remove person with personId from film with filmId
     * @param filmId primary key of film to remove person
     * @param personId primary key of person
     * @throws DaoException
     */
    void removePersonFromFilm(int filmId, int personId) throws DaoException;

    /**
     * This method remove person with personId and roleId from film with filmId
     * @param filmId primary key of film to remove person
     * @param personId primary key of person
     * @param roleId primary key of person role
     * @throws DaoException
     */
    void removePersonFromFilm(int filmId, int personId, int roleId) throws DaoException;

    /**
     * This method remove persons from film with filmId
     * @param filmId  primary key of film to remove all persons
     * @throws DaoException
     */
    void removeAllPersonsFromFilm(int filmId) throws DaoException;

    /**
     * This method select films with personId
     * @param personId primary key of person to select all films with this person
     * @return list of films with this person primary key
     * @throws DaoException
     */
    List<Film> findByPerson(int personId) throws DaoException;

    /**
     * This method select films with personId and roleId
     * @param personId primary key of person to select all films with this person
     * @param roleId primary key of person role
     * @return list of films with this person and role primary keys
     * @throws DaoException
     */
    List<Film> findByPersonAndRole(int personId, int roleId) throws DaoException;

    /**
     * This method select film with name and year
     * @param name film name
     * @param year film year
     * @return film with this name anf year or null if doesn't exist
     * @throws DaoException
     */
    Film getFilmByNameAndYear(String name, int year) throws DaoException;
}
