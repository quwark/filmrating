package by.epam.filmrating.dao;

import by.epam.filmrating.domain.Genre;
import by.epam.filmrating.exception.DaoException;

import java.util.List;

/**
 * Interface GenreDao contains operations for entity Genre
 * @see by.epam.filmrating.domain.Genre
 *
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-06
 */
public interface GenreDao extends GenericDao<Genre, Integer> {

    /**
     * This method select genres that have this filmId
     * @param filmId primary key of film
     * @return list of genres that current film includes
     * @throws DaoException
     */
    List<Genre> findGenresByFilm(int filmId) throws DaoException;
}
