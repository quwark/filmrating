package by.epam.filmrating.dao;

import by.epam.filmrating.domain.Person;
import by.epam.filmrating.exception.DaoException;

import java.util.List;

/**
 * Interface PersonDao contains operations for entity Genre
 * @see by.epam.filmrating.domain.Person
 *
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-06
 */
public interface PersonDao extends GenericDao<Person, Integer> {
    /**
     * This method select persons that have this filmId
     * @param filmId primary key of film to select persons that connected with this film
     * @return list of persons that
     * @throws DaoException
     */
    List<Person> findPersonsByFilm(int filmId) throws DaoException;

    /**
     * This method select persons that have this filmId and roleId
     * @param filmId primary key of film to select persons that connected with this film
     * @param roleId primary key of person role
     * @return list of persons that have this film and role primary keys
     * @throws DaoException
     */
    List<Person> findPersonByFilmAndRole(int filmId, int roleId) throws DaoException;

    /**
     * This method select person that have this fistName and lastName
     * @param firstName first name of person
     * @param lastName second name of person
     * @return person that have this first and last name or null if doesn't exist
     * @throws DaoException
     */
    Person findByFirstAndLastName(String firstName, String lastName) throws DaoException;
}
