package by.epam.filmrating.dao.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yura on 06.06.2016.
 */
public class JdbcTemplate {

    private Connection conn;

    public JdbcTemplate(Connection connection) {
        this.conn = connection;
    }

    public int executeInsertQuery(String query, Object... params) throws SQLException {
        int result = 0;
        try(PreparedStatement ps = conn.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS)) {
            int index = 1;
            for(Object param : params) {
                ps.setObject(index++, param);
            }

            ps.executeUpdate();

            try(ResultSet rs = ps.getGeneratedKeys()) {
                if(rs.next()) {
                    result = rs.getInt(1);
                }
            }
        }
        return result;
    }

    public int executeUpdateQuery(String query, Object... params) throws SQLException {
        int result = 0;
        try(PreparedStatement ps = conn.prepareStatement(query)) {
            int index = 1;
            for(Object param : params) {
                ps.setObject(index++, param);
            }
            result = ps.executeUpdate();
        }
        return result;
    }

    public <T> List<T> executeFind(String query, RowMapper<T> mapper, Object... params) throws SQLException {
        List<T> list = new ArrayList<>();
        try(PreparedStatement ps = conn.prepareStatement(query)) {
            int index = 1;
            for(Object param : params) {
                ps.setObject(index++,  param);
            }

            try(ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    list.add(mapper.mapRow(rs));
                }
            }
        }
        return list;
    }

    public <T> T executeFindOne(String query, RowMapper<T> mapper, Object... params) throws SQLException {
        List<T> list = executeFind(query, mapper, params);
        if(list.size() == 0) {
            return null;
        }
        return list.get(0);
    }
}
