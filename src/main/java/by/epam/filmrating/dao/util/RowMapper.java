package by.epam.filmrating.dao.util;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Interface RowMapper contain method for mapping entity with ResultSet
 * @see java.sql.ResultSet
 * @param <T> is object that represent entity
 *
 * @author  Dulchevski Yury
 * @version 1.0
 * @since   2016-06-06
 */
public interface RowMapper<T> {

    /**
     *
     * @param rs ResultSet after select operation
     * @return entity with mapped fields
     * @throws SQLException
     */
    T mapRow(ResultSet rs) throws SQLException;
}
