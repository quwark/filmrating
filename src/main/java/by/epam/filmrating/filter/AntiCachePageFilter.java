package by.epam.filmrating.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created by Yura on 26.06.2016.
 */
public class AntiCachePageFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

    }

    @Override
    public void destroy() {

    }
}
