package by.epam.filmrating.filter;

import by.epam.filmrating.domain.User;
import by.epam.filmrating.domain.UserRole;
import by.epam.filmrating.domain.UserStatus;
import by.epam.filmrating.manager.AppConfigManager;
import by.epam.filmrating.manager.SecurityManager;
import by.epam.filmrating.util.RequestParamUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

/**
 * Created by Yura on 15.06.2016.
 */
public class AuthFilter implements Filter {

    private static final String USER_SESSION_ATTR_NAME = "user";
    private static final String ACTION_PARAM_NAME = "command";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {

        String commandParam = servletRequest.getParameter(ACTION_PARAM_NAME);
        String command = RequestParamUtil.getAsString(commandParam);
        if(command == null) {
            String page = AppConfigManager.getInstance().getProperty(AppConfigManager.ERROR_PAGE_400);
            servletRequest.getRequestDispatcher(page).forward(servletRequest, servletResponse);
        } else {

                HttpServletResponse response = (HttpServletResponse) servletResponse;
                response.setHeader("Pragma", "No-cache");
                response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
                response.setDateHeader("Expires", -1);

            HttpSession session = ((HttpServletRequest)servletRequest).getSession(false);
            String role = null;
            if(session == null) {
                role = UserRole.GUEST.name();
            } else {
                User user = (User) session.getAttribute(USER_SESSION_ATTR_NAME);
                if(user == null) {
                    role = UserRole.GUEST.name();
                } else {
                    role = user.getRole().name();
                }
            }

            Set<String> accessedCommands = getAccessedCommands(role);

            if(accessedCommands.contains(command)) {
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                String page = AppConfigManager.getInstance().getProperty(AppConfigManager.INDEX_PATH_PAGE);
                servletRequest.getRequestDispatcher(page).forward(servletRequest, servletResponse);
            }
        }
    }

    private Set<String> getAccessedCommands(String role) {
        SecurityManager securityManager = SecurityManager.getInstance();
        String value = securityManager.getProperty(role);
        String[] commands = value.split(",");
        Set<String> accessedCommands = new HashSet<>();
        for(String command : commands) {
            accessedCommands.add(command.trim());
        }
        return accessedCommands;
    }

    @Override
    public void destroy() {

    }
}
