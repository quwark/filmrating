package by.epam.filmrating.filter;

import javax.servlet.*;
import java.io.IOException;

/**
 * Created by Yura on 10.06.2016.
 */
public class CharacterEncodingFilter implements Filter {

    private static final String PARAM_NAME_ENCODING = "encoding";
    private String encoding;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        encoding = filterConfig.getInitParameter(PARAM_NAME_ENCODING);
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        String requestEncoding = servletRequest.getCharacterEncoding();
        if(encoding != null && !encoding.equalsIgnoreCase(requestEncoding)) {
            servletRequest.setCharacterEncoding(encoding);
            servletResponse.setCharacterEncoding(encoding);
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    /*
    * String lang = servletRequest.getParameter("lang");
            if(lang != null) {
                ((HttpServletResponse)servletResponse).addCookie(new Cookie("lang", lang));
                String[] param = lang.split("_");
                Locale locale = new Locale(param[0], param[1]);
                MessageManager.INSTANCE.changeLocale(locale);
                servletRequest.setAttribute("locale", locale);

            }*/

    @Override
    public void destroy() {
        encoding = null;
    }
}
