package by.epam.filmrating.listener;

import by.epam.filmrating.dao.factory.DaoFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Created by Yura on 28.06.2016.
 */
public class MyServletContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        DaoFactory.init();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        DaoFactory.destroy();
    }
}
