package by.epam.filmrating.listener;

import by.epam.filmrating.dao.factory.DaoFactory;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

/**
 * Created by Yura on 08.06.2016.
 */
public class MyServletRequestListener implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
        DaoFactory.closeCurrentDaoManager();

    }

    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {

    }
}
