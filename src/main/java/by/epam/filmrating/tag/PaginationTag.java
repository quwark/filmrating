package by.epam.filmrating.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.io.Writer;

/**
 * Created by Yura on 14.06.2016.
 */
public class PaginationTag extends TagSupport {

    private String uri;
    private int total;
    private int current;
    private int maxLinks = 10;
    private boolean enableText;
    private String prevText = "Previous";
    private String nextText = "Next";

    @Override
    public int doStartTag() throws JspException {
        Writer out = pageContext.getOut();

        boolean lastPage = current == total;
        int startPage = Math.max(current - maxLinks / 2, 1);
        int endPage = startPage + maxLinks;
        if(endPage > total + 1) {
            int diff = endPage - total;
            startPage -= diff - 1;
            if(startPage < 1) {
                startPage = 1;
            }
            endPage = total + 1;
        }

        try {
            out.write("<ul class=\"pagination pagination-list\">");

            if(current > 1 && enableText) {
                out.write(buildLink(current - 1, prevText, "pagination-prev"));
            }

            for(int i = startPage; i < endPage; i++) {
                if(i == current) {
                    out.write(buildLink(i, String.valueOf(i), "active"));
                } else {
                    out.write(buildLink(i));
                }
            }

            if(!lastPage && enableText) {
                out.write(buildLink(current + 1, nextText, "pagination-next pagination-last"));
            }
            out.write("</ul>");
        } catch (IOException e) {
            throw new JspException("Error in pagination tag", e);
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }

    private String buildLink(int page) {
        return buildLink(page, String.valueOf(page), null);
    }

    private String buildLink(int page, String text, String cssClass) {
        StringBuilder link = new StringBuilder("<li");
        if(cssClass != null) {
            link.append(" class=\"");
            link.append(cssClass);
            link.append("\"");
        }
        link.append(">");
        link.append("<a href=\"");
        link.append(uri.replace("##", String.valueOf(page)));
        link.append("\">");
        link.append(text);
        link.append("</a></li>");

        return link.toString();
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public void setMaxLinks(int maxLinks) {
        this.maxLinks = maxLinks;
    }

    public void setEnableText(boolean enableText) {
        this.enableText = enableText;
    }

    public void setPrevText(String prevText) {
        this.prevText = prevText;
    }

    public void setNextText(String nextText) {
        this.nextText = nextText;
    }
}
