package by.epam.filmrating.service;

import by.epam.filmrating.domain.Person;
import by.epam.filmrating.exception.PersonAlreadyExistException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.exception.ValidationException;
import by.epam.filmrating.model.form.PersonForm;

import java.util.List;

/**
 * Created by Yura on 11.06.2016.
 */
public interface PersonService {

    Person getById(int id) throws ServiceException;
    List<Person> getAll() throws ServiceException;
    List<Person> getPersonsByFilmId(int filmId) throws ServiceException;
    List<Person> getPersonByFilmAndRole(int filmId, int roleId) throws ServiceException;
    void create(PersonForm personForm) throws ServiceException, ValidationException, PersonAlreadyExistException;
    void update(PersonForm personForm) throws ServiceException, ValidationException, PersonAlreadyExistException;
    void delete(int id) throws ServiceException;
}
