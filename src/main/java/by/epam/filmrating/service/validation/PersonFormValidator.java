package by.epam.filmrating.service.validation;

import by.epam.filmrating.model.form.PersonForm;
import by.epam.filmrating.util.ValidationsUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yura on 19.06.2016.
 */
public class PersonFormValidator implements FormValidator<PersonForm> {

    private static final String NAME_REGEXP = "[а-яА-Яa-zA-Z\\-]{2,50}";

    @Override
    public Map<String, String> validate(PersonForm form) {
        Map<String, String> errors = new HashMap<>();
        if(form.getFirstName() == null) {
            errors.put("invalidFirstName", "add_person.form.fields.first_name.required");
        } else if(!ValidationsUtil.validateString(form.getFirstName(), NAME_REGEXP)) {
            errors.put("invalidFirstName", "add_person.form.fields.first_name.invalid");
        }
        if(form.getLastName() == null) {
            errors.put("invalidLastName", "add_person.form.fields.last_name.required");
        } else if(!ValidationsUtil.validateString(form.getLastName(), NAME_REGEXP)) {
            errors.put("invalidLastName", "add_person.form.fields.last_name.invalid");
        }

        if(form.getBirthDate() == null) {
            errors.put("birthDateInvalid", "add_person.form.fields.birth_date.required");
        }
        return errors;
    }
}
