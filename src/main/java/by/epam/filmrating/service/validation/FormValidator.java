package by.epam.filmrating.service.validation;

import java.util.Map;

/**
 * Created by Yura on 10.06.2016.
 */
public interface FormValidator<T> {

    Map<String, String> validate(T form);
}
