package by.epam.filmrating.service.validation;

import by.epam.filmrating.model.form.UserMarkForm;
import by.epam.filmrating.util.ValidationsUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yura on 17.06.2016.
 */
public class UserMarkFormValidator implements FormValidator<UserMarkForm> {
    @Override
    public Map<String, String> validate(UserMarkForm form) {
        Map<String, String> errors = new HashMap<>();
        if(form.getUserMark() == null) {
            errors.put("userMarkInvalid", "add_mark.form.fields.mark.required");
        } else if(!ValidationsUtil.checkInRangeInteger(form.getUserMark(), 1, 10)) {
            errors.put("userMarkInvalid", "add_mark.form.fields.mark.invalid");
        }
        return errors;
    }
}
