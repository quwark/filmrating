package by.epam.filmrating.service.validation;

import by.epam.filmrating.model.form.UserForm;
import by.epam.filmrating.util.ValidationsUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yura on 13.06.2016.
 */
public class UserFormValidator implements FormValidator<UserForm> {

    private static final String LOGIN_REGEXP = "^[a-zA-z0-9_]{4,12}$";
    private static final String PASSWORD_REGEXP = "^[a-zA-z0-9_]{4,12}$";
    private static final String EMAIL_REGEXP = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)" +
            "*(\\.[A-Za-z]{2,})$";

    @Override
    public Map<String, String> validate(UserForm form) {
        Map<String, String> errors = new HashMap<>();
        if(form.getLogin() == null) {
            errors.put("loginInvalid", "register.form.fields.login.required");
        } else if(!ValidationsUtil.validateString(form.getLogin(), LOGIN_REGEXP)) {
            errors.put("loginInvalid", "register.form.fields.login.invalid");
        }

        if(form.getPassword() == null) {
            errors.put("passwordInvalid", "register.form.fields.password.required");
        } else if(!ValidationsUtil.validateString(form.getPassword(), PASSWORD_REGEXP)) {
            errors.put("passwordInvalid", "register.form.fields.password.invalid");
        } else if(!form.getPassword().equals(form.getRepeatPassword())) {
            errors.put("passwordInvalid", "register.form.fields.password.not_equals");
            errors.put("repeatPasswordInvalid", "register.form.fields.password.not_equals");
        }

        if(form.getEmail() == null) {
            errors.put("emailInvalid", "register.form.fields.email.required");
        } else if(!ValidationsUtil.validateString(form.getEmail(), EMAIL_REGEXP)) {
            errors.put("emailInvalid", "register.form.fields.email.invalid");
        }

        return errors;
    }
}
