package by.epam.filmrating.service.validation;

import by.epam.filmrating.model.form.CommentForm;
import by.epam.filmrating.util.ValidationsUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yura on 17.06.2016.
 */
public class CommentFormValidator implements FormValidator<CommentForm> {

    private static final String FILM_COMMENT_REGEXP = "[^<>]+";

    @Override
    public Map<String, String> validate(CommentForm form) {
        Map<String, String> errors = new HashMap<>();
        if(form.getText() == null) {
            errors.put("commentTextInvalid", "add_comment.form.fields.comment_text.required");
        } else if(!ValidationsUtil.validateString(form.getText(), FILM_COMMENT_REGEXP)) {
            errors.put("commentTextInvalid", "add_comment.form.fields.comment_text.invalid");
        }
        return errors;
    }
}
