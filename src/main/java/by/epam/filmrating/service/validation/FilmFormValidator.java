package by.epam.filmrating.service.validation;

import by.epam.filmrating.model.form.FilmForm;
import by.epam.filmrating.util.ValidationsUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Yura on 17.06.2016.
 */
public class FilmFormValidator implements FormValidator<FilmForm> {

    private static final String FILM_NAME_REGEXP = "^[a-zA-Zа-яА-Я0-9_\\-:\\.\\-—– ]{4,50}$";
    private static final String FILM_DESCRIPTION_REGEXP = "[a-zA-Zа-яА-ЯёЁ0-9_,\\-—:\\.\\r\\n …?!\'\'\"\" «»–]+";


    @Override
    public Map<String, String> validate(FilmForm form) {
        Map<String, String> errors = new HashMap<>();
        if(form.getName() == null) {
            errors.put("nameInvalid", "add_film.form.fields.name.required");
        } else if(!ValidationsUtil.validateString(form.getName(), FILM_NAME_REGEXP)) {
            errors.put("nameInvalid", "add_film.form.fields.name.invalid");
        }
        if(form.getDescription() == null) {
            errors.put("descriptionInvalid", "add_film.form.fields.description.required");
        }
        else if(!ValidationsUtil.validateString(form.getDescription(), FILM_DESCRIPTION_REGEXP)){
            errors.put("descriptionInvalid", "add_film.form.fields.description.invalid");
        }
        if(form.getProducerId() == null) {
            errors.put("producerInvalid", "add_film.form.fields.producer.required");
        }
        if(form.getYear() == null) {
            errors.put("yearInvalid", "add_film.form.fields.year.required");
        }else if(!ValidationsUtil.checkInRangeInteger(form.getYear(), 1930, null)) {
            errors.put("yearInvalid", "add_film.form.fields.year.invalid");
        }
        if(form.getGenreIds().isEmpty()) {
            errors.put("genresInvalid", "add_film.form.fields.genres.required");
        }
        if(form.getActorIds().isEmpty()) {
            errors.put("actorsInvalid", "add_film.form.fields.actors.required");
        }

        return errors;
    }
}
