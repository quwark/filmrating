package by.epam.filmrating.service;

import by.epam.filmrating.domain.User;
import by.epam.filmrating.domain.UserRole;
import by.epam.filmrating.domain.UserStatus;
import by.epam.filmrating.exception.EmailAlreadyExistException;
import by.epam.filmrating.exception.LoginAlreadyExistException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.exception.ValidationException;
import by.epam.filmrating.model.form.UserForm;

import java.util.List;

/**
 * Created by Yura on 07.06.2016.
 */
public interface UserService {

    List<User> getAll() throws ServiceException;
    List<User> getByUserRole(UserRole role) throws ServiceException;
    List<User> getByUserStatus(UserStatus status) throws ServiceException;
    User getById(int userId) throws ServiceException;
    User getByLogin(String login) throws ServiceException;
    void registerUser(UserForm registerForm, String salt) throws ServiceException, ValidationException, LoginAlreadyExistException,
            EmailAlreadyExistException;
    void delete(int userId) throws ServiceException;
    void update(User user) throws  ServiceException;
    void updateRating(Integer userId, Integer newRating) throws ServiceException, ValidationException;
    void changeUserStatus(Integer userId, UserStatus newStatus) throws ServiceException;
    boolean isLoginExist(String login) throws ServiceException;
    boolean isEmailExist(String email) throws ServiceException;
    boolean checkUser(String login, String password, String salt) throws ServiceException;
}
