package by.epam.filmrating.service.impl;

import by.epam.filmrating.dao.UserDao;
import by.epam.filmrating.dao.factory.DaoFactory;
import by.epam.filmrating.dao.factory.DaoManager;
import by.epam.filmrating.domain.User;
import by.epam.filmrating.domain.UserRole;
import by.epam.filmrating.domain.UserStatus;
import by.epam.filmrating.exception.*;
import by.epam.filmrating.model.form.UserForm;
import by.epam.filmrating.service.UserService;
import by.epam.filmrating.service.validation.FormValidator;
import by.epam.filmrating.service.validation.UserFormValidator;
import by.epam.filmrating.util.MD5Util;
import by.epam.filmrating.util.ValidationsUtil;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Yura on 08.06.2016.
 */
public class UserServiceImpl implements UserService {

    public UserServiceImpl() {
    }

    @Override
    public List<User> getAll() throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getUserDao().findAll();
    }

    @Override
    public List<User> getByUserRole(UserRole role) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getUserDao().findByRole(role);
    }

    @Override
    public List<User> getByUserStatus(UserStatus status) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getUserDao().findByStatus(status);

    }

    @Override
    public User getById(int userId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getUserDao().findById(userId);
    }

    @Override
    public User getByLogin(String login) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getUserDao().findByLogin(login);

    }

    @Override
    public void registerUser(UserForm registerForm, String salt) throws ServiceException, ValidationException,
            LoginAlreadyExistException, EmailAlreadyExistException  {
        if(salt == null) {
            salt = "";
        }
        FormValidator<UserForm> validator = new UserFormValidator();
        Map<String, String> errors = validator.validate(registerForm);

        if(errors.size() == 0) {
            DaoManager daoManager = DaoFactory.createDaoManager();
            UserDao userDao = daoManager.getUserDao();

            if(userDao.findByLogin(registerForm.getLogin()) != null) {
                throw new LoginAlreadyExistException("Login is already exist");
            }
            if(userDao.findByEmail(registerForm.getEmail()) != null) {
                throw new EmailAlreadyExistException("Email is already exist");
            }
            User user = new User();
            user.setLogin(registerForm.getLogin());
            user.setPassword(MD5Util.getMD5Hash(salt + registerForm.getPassword()));
            user.setEmail(registerForm.getEmail());
            user.setStatus(UserStatus.ACTIVE);
            user.setRole(UserRole.USER);
            user.setRating(0);
            user.setRegistrationDate(new Date());
            daoManager.getUserDao().create(user);
        } else {
            ValidationException ve = new ValidationException("Can't register user. Some validation exceptions");
            ve.setErrors(errors);
            throw ve;
        }
    }

    @Override
    public void delete(int userId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        daoManager.getUserDao().delete(userId);
    }

    @Override
    public void update(User user) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        daoManager.getUserDao().update(user);
    }

    @Override
    public void updateRating(Integer userId, Integer newRating) throws ServiceException, ValidationException {
        ValidationException ve = new ValidationException();

        if(newRating == null) {
            ve.addError("userRatingInvalid", "change_rating.form.fields.rating.required");
        } else if(!ValidationsUtil.checkInRangeInteger(newRating, 0, 2000)) {
            ve.addError("userRatingInvalid", "change_rating.form.fields.rating.invalid");
        }

        if(!ve.getErrors().isEmpty()) {
            throw ve;
        }
        DaoManager daoManager = DaoFactory.createDaoManager();
        UserDao userDao = daoManager.getUserDao();
        User user;

        if(userId == null || (user = userDao.findById(userId)) == null) {
            throw new ServiceException("This user does't exist");
        }

        user.setRating(newRating);
        userDao.update(user);
    }

    @Override
    public void changeUserStatus(Integer userId, UserStatus newStatus) throws ServiceException {

        if(newStatus == null) {
            throw new ServiceException("Can't set new status");
        }

        DaoManager daoManager = DaoFactory.createDaoManager();
        UserDao userDao = daoManager.getUserDao();
        User user;

        if(userId == null || (user = userDao.findById(userId)) == null) {
            throw new ServiceException("This user does't exist");
        }

        if(user.getStatus() == UserStatus.ACTIVE) {
            user.setStatus(UserStatus.BANNED);
        } else {
            user.setStatus(UserStatus.ACTIVE);
        }
        userDao.update(user);
    }

    @Override
    public boolean isLoginExist(String login) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return (daoManager.getUserDao().findByLogin(login) != null);
    }

    @Override
    public boolean isEmailExist(String email) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return (daoManager.getUserDao().findByEmail(email) != null);
    }

    @Override
    public boolean checkUser(String login, String password, String salt) throws ServiceException {
        if(login == null || login.isEmpty() || password == null || password.isEmpty()) {
            return false;
        }
        if(salt == null) {
            salt = "";
        }

        DaoManager daoManager = DaoFactory.createDaoManager();
        User user = daoManager.getUserDao().findByLogin(login);
        if(user != null) {
            String currentMd5Password = MD5Util.getMD5Hash(salt + password);
            if (currentMd5Password.equals(user.getPassword())) {
                return true;
            }
        }
        return false;
    }
}
