package by.epam.filmrating.service.impl;

import by.epam.filmrating.dao.FilmDao;
import by.epam.filmrating.dao.factory.DaoFactory;
import by.epam.filmrating.dao.factory.DaoManager;
import by.epam.filmrating.domain.Film;
import by.epam.filmrating.exception.DaoException;
import by.epam.filmrating.exception.FilmAlreadyExistException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.exception.ValidationException;
import by.epam.filmrating.model.form.FilmForm;
import by.epam.filmrating.service.FilmService;
import by.epam.filmrating.service.validation.FilmFormValidator;
import by.epam.filmrating.service.validation.FormValidator;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Yura on 11.06.2016.
 */
public class FilmServiceImpl implements FilmService {

    @Override
    public void add(FilmForm addFilmForm) throws ServiceException, ValidationException, FilmAlreadyExistException {
        FormValidator<FilmForm> validator = new FilmFormValidator();
        Map<String, String> errors = validator.validate(addFilmForm);

        if(!errors.isEmpty()) {
            ValidationException ve = new ValidationException("Film form entity is not valid");
            ve.setErrors(errors);
            throw ve;
        }

        DaoManager daoManager = DaoFactory.createDaoManager();
        FilmDao filmDao = daoManager.getFilmDao();
        if(filmDao.getFilmByNameAndYear(addFilmForm.getName(), addFilmForm.getYear()) != null) {
            throw new FilmAlreadyExistException("Film is already exits");
        }

        Film film = new Film();
        film.setName(addFilmForm.getName());
        film.setDescription(addFilmForm.getDescription());
        film.setYear(addFilmForm.getYear());
        film.setImgURL(addFilmForm.getImgURL());
        film.setAddedDate(new Date());


        try {
            daoManager.beginTransaction();

            filmDao.create(film);

            filmDao.addGenresToFilm(film.getId(), addFilmForm.getGenreIds());
            filmDao.addPersonToFilm(film.getId(), addFilmForm.getProducerId(), 2);
            filmDao.addPersonsToFilm(film.getId(), addFilmForm.getActorIds(), 1);
            daoManager.commit();
        } catch (DaoException e) {
            daoManager.rollback();
            throw new ServiceException("Can't add film", e);
        } finally {
            daoManager.beginTransaction();
        }
    }

    @Override
    public void update(FilmForm filmForm) throws ServiceException, ValidationException, FilmAlreadyExistException {
        FormValidator<FilmForm> validator = new FilmFormValidator();
        Map<String, String> errors = validator.validate(filmForm);

        if(!errors.isEmpty()) {
            ValidationException ve = new ValidationException("Film form entity is not valid");
            ve.setErrors(errors);
            throw ve;
        }

        DaoManager daoManager = DaoFactory.createDaoManager();
        FilmDao filmDao = daoManager.getFilmDao();

        Film film;
        if(filmForm.getId() == null || (film = filmDao.findById(filmForm.getId())) == null) {
            throw new ServiceException("Film does't exist");
        }

        if(filmDao.getFilmByNameAndYear(filmForm.getName(), filmForm.getYear()) != null) {
            throw new FilmAlreadyExistException("Film is already exits");
        }

        film.setName(filmForm.getName());
        if(filmForm.getImgURL() !=  null) {
            film.setImgURL(filmForm.getImgURL());
        }

        film.setDescription(filmForm.getDescription());
        film.setYear(filmForm.getYear());

        try {
            daoManager.beginTransaction();
            filmDao.update(film);

            filmDao.removeAllGenresFromFilm(filmForm.getId());
            filmDao.removeAllPersonsFromFilm(filmForm.getId());

            filmDao.addGenresToFilm(film.getId(), filmForm.getGenreIds());
            filmDao.addPersonToFilm(film.getId(), filmForm.getProducerId(), 2);
            filmDao.addPersonsToFilm(film.getId(), filmForm.getActorIds(), 1);
            daoManager.commit();
        } catch (DaoException e) {
            daoManager.rollback();
            throw new ServiceException("Can't update film with id: " + filmForm.getId(), e);
        } finally {
            daoManager.endTransaction();
        }
    }

    @Override
    public void delete(Integer filmId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        FilmDao filmDao = daoManager.getFilmDao();
        if(filmId == null || filmDao.findById(filmId) == null) {
            throw new ServiceException("Film doesn't exist");
        }
        daoManager.getFilmDao().delete(filmId);
    }

    @Override
    public List<Film> getAll() throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getFilmDao().findAll();
    }

    @Override
    public Film getById(int filmId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getFilmDao().findById(filmId);
    }

    @Override
    public List<Film> getFilmsByGenreId(int genreId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getFilmDao().findByGenre(genreId);
    }

    @Override
    public List<Film> getFilmsByPersonId(int personId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getFilmDao().findByPerson(personId);
    }
}
