package by.epam.filmrating.service.impl;

import by.epam.filmrating.dao.factory.DaoFactory;
import by.epam.filmrating.dao.factory.DaoManager;
import by.epam.filmrating.domain.Genre;
import by.epam.filmrating.exception.DaoException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.service.GenreService;
import org.apache.log4j.Logger;

import java.util.List;

/**
 * Created by Yura on 11.06.2016.
 */
public class GenreServiceImpl implements GenreService {

    @Override
    public List<Genre> getAll() throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getGenreDao().findAll();
    }

    @Override
    public Genre getById(int genreId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getGenreDao().findById(genreId);
    }

    @Override
    public List<Genre> getGenresByFilmId(int filmId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getGenreDao().findGenresByFilm(filmId);

    }
}
