package by.epam.filmrating.service.impl;

import by.epam.filmrating.dao.FilmDao;
import by.epam.filmrating.dao.UserDao;
import by.epam.filmrating.dao.factory.DaoFactory;
import by.epam.filmrating.dao.factory.DaoManager;
import by.epam.filmrating.domain.UserComment;
import by.epam.filmrating.exception.DaoException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.exception.ValidationException;
import by.epam.filmrating.model.form.CommentForm;
import by.epam.filmrating.service.UserCommentService;
import by.epam.filmrating.service.validation.CommentFormValidator;
import by.epam.filmrating.service.validation.FormValidator;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Yura on 12.06.2016.
 */
public class UserCommentServiceImpl implements UserCommentService {

    @Override
    public void add(CommentForm commentForm) throws ServiceException, ValidationException {
        FormValidator<CommentForm> validator = new CommentFormValidator();
        Map<String, String> errors = validator.validate(commentForm);
        if(!errors.isEmpty()) {
            ValidationException ve = new ValidationException();
            ve.setErrors(errors);
            throw ve;
        }

        DaoManager daoManager = DaoFactory.createDaoManager();
        UserDao userDao = daoManager.getUserDao();
        FilmDao filmDao = daoManager.getFilmDao();
        if(commentForm.getUserId() == null || userDao.findById(commentForm.getUserId()) == null) {
            throw new ServiceException("User doesn't exist");
        }
        if(commentForm.getFilmId() == null || filmDao.findById(commentForm.getFilmId()) == null) {
            throw new ServiceException("Film doesn't exist");
        }

        UserComment userComment = new UserComment();
        userComment.setFilmId(commentForm.getFilmId());
        userComment.setUserId(commentForm.getUserId());
        userComment.setText(commentForm.getText());
        userComment.setCommentTime(new Date());

        daoManager.getUserCommentDao().create(userComment);
    }

    @Override
    public void update(UserComment userComment) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        daoManager.getUserCommentDao().update(userComment);
    }

    @Override
    public void delete(int commentId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        daoManager.getUserCommentDao().delete(commentId);
    }

    @Override
    public UserComment getById(int commentId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getUserCommentDao().findById(commentId);
    }

    @Override
    public List<UserComment> getUserCommentsByFilmId(int filmId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getUserCommentDao().findUserCommentsByFilm(filmId);
    }

    @Override
    public List<UserComment> getUserCommentsByUserId(int userId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getUserCommentDao().findUserCommentsByUser(userId);
    }
}
