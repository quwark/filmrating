package by.epam.filmrating.service.impl;

import by.epam.filmrating.dao.PersonDao;
import by.epam.filmrating.dao.factory.DaoFactory;
import by.epam.filmrating.dao.factory.DaoManager;
import by.epam.filmrating.domain.Person;
import by.epam.filmrating.exception.DaoException;
import by.epam.filmrating.exception.PersonAlreadyExistException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.exception.ValidationException;
import by.epam.filmrating.model.form.PersonForm;
import by.epam.filmrating.service.PersonService;
import by.epam.filmrating.service.validation.FormValidator;
import by.epam.filmrating.service.validation.PersonFormValidator;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Map;

/**
 * Created by Yura on 11.06.2016.
 */
public class PersonServiceImpl implements PersonService {

    @Override
    public Person getById(int id) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getPersonDao().findById(id);
    }

    @Override
    public List<Person> getAll() throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getPersonDao().findAll();
    }

    @Override
    public List<Person> getPersonsByFilmId(int filmId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getPersonDao().findPersonsByFilm(filmId);
    }

    @Override
    public List<Person> getPersonByFilmAndRole(int filmId, int roleId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getPersonDao().findPersonByFilmAndRole(filmId, roleId);
    }

    @Override
    public void create(PersonForm personForm) throws ServiceException, ValidationException, PersonAlreadyExistException {
        FormValidator<PersonForm> validator = new PersonFormValidator();
        Map<String, String> errors = validator.validate(personForm);

        if(!errors.isEmpty()) {
            ValidationException ve = new ValidationException();
            ve.setErrors(errors);
            throw ve;
        }

        DaoManager daoManager = DaoFactory.createDaoManager();
        PersonDao personDao = daoManager.getPersonDao();

        if(personDao.findByFirstAndLastName(personForm.getFirstName(), personForm.getLastName()) != null) {
            throw new PersonAlreadyExistException("Person is already exist");
        }
        Person person = new Person();
        person.setFirstName(personForm.getFirstName());
        person.setLastName(personForm.getLastName());
        person.setBirthDate(personForm.getBirthDate());
        personDao.create(person);
    }

    @Override
    public void update(PersonForm personForm) throws ServiceException, ValidationException, PersonAlreadyExistException {
        FormValidator<PersonForm> validator = new PersonFormValidator();
        Map<String, String> errors = validator.validate(personForm);

        if(!errors.isEmpty()) {
            ValidationException ve = new ValidationException();
            ve.setErrors(errors);
            throw ve;
        }

        DaoManager daoManager = DaoFactory.createDaoManager();
        PersonDao personDao = daoManager.getPersonDao();

        Person personByName = personDao.findByFirstAndLastName(personForm.getFirstName(), personForm.getLastName());

        Person personToSave = new Person();
        personToSave.setFirstName(personForm.getFirstName());
        personToSave.setLastName(personForm.getLastName());
        personToSave.setBirthDate(personForm.getBirthDate());

        if(personForm.getId() == null) {
            if(personByName == null) {
                personDao.create(personToSave);
            } else {
                throw new PersonAlreadyExistException("Person is already exist");
            }
        } else if(personForm.getId() != null) {
            Person personById = personDao.findById(personForm.getId());
            if(personByName == null || personById.getId() == personByName.getId()) {
                personToSave.setId(personForm.getId());
                personDao.update(personToSave);
            } else {
                throw new PersonAlreadyExistException("Person is already exist");
            }
        }
    }

    @Override
    public void delete(int id) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        daoManager.getPersonDao().delete(id);
    }
}
