package by.epam.filmrating.service.impl;

import by.epam.filmrating.dao.FilmDao;
import by.epam.filmrating.dao.UserDao;
import by.epam.filmrating.dao.UserMarkDao;
import by.epam.filmrating.dao.factory.DaoFactory;
import by.epam.filmrating.dao.factory.DaoManager;
import by.epam.filmrating.domain.User;
import by.epam.filmrating.domain.UserMark;
import by.epam.filmrating.exception.DaoException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.exception.ValidationException;
import by.epam.filmrating.model.form.UserMarkForm;
import by.epam.filmrating.service.UserMarkService;
import by.epam.filmrating.service.validation.FormValidator;
import by.epam.filmrating.service.validation.UserMarkFormValidator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Yura on 13.06.2016.
 */
public class UserMarkServiceImpl implements UserMarkService {

    @Override
    public void add(UserMarkForm userMarkForm, int minCount, int step) throws ServiceException,
            ValidationException {
        FormValidator<UserMarkForm> validator = new UserMarkFormValidator();
        Map<String, String> errors = validator.validate(userMarkForm);
        if(!errors.isEmpty()) {
            ValidationException ve = new ValidationException();
            ve.setErrors(errors);
            throw ve;
        }

        DaoManager daoManager = DaoFactory.createDaoManager();
        UserDao userDao = daoManager.getUserDao();
        FilmDao filmDao = daoManager.getFilmDao();
        UserMarkDao userMarkDao = daoManager.getUserMarkDao();
        if(userMarkForm.getUserId() == null || userDao.findById(userMarkForm.getUserId()) == null) {
            throw new ServiceException("User doesn't exist");
        }
        if(userMarkForm.getFilmId() == null || filmDao.findById(userMarkForm.getFilmId()) == null) {
            throw new ServiceException("Film doesn't exist");
        }
        if(userMarkDao.findUserMarkByFilmAndUser(userMarkForm.getFilmId(), userMarkForm.getUserId()) != null) {
            throw new ServiceException("This user is already put mark");
        }

        UserMark userMark = new UserMark();
        userMark.setUserId(userMarkForm.getUserId());
        userMark.setFilmId(userMarkForm.getFilmId());
        userMark.setMark(userMarkForm.getUserMark());

        try {
            daoManager.beginTransaction();
            userMarkDao.create(userMark);
            calculateUserRatingByFilmId(userMark.getFilmId(), minCount, step);
            daoManager.commit();
        } catch (DaoException e) {
            daoManager.rollback();
            throw new ServiceException("Can't create user mark", e);
        } finally {
            daoManager.endTransaction();
        }
    }

    @Override
    public void update(UserMark userMark) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        daoManager.getUserMarkDao().update(userMark);
    }

    @Override
    public List<UserMark> getUserMarksByFilmId(int filmId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getUserMarkDao().findUserMarksByFilm(filmId);
    }

    @Override
    public boolean isUserAvailableToPutMark(int filmId, int userId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        return daoManager.getUserMarkDao().findUserMarkByFilmAndUser(filmId, userId) == null;
    }

    @Override
    public float getAverageMarkByFilmId(int filmId) throws ServiceException {
        DaoManager daoManager = DaoFactory.createDaoManager();
        List<UserMark> userMarkList = daoManager.getUserMarkDao().findUserMarksByFilm(filmId);
        return getAverageMark(userMarkList);
    }

    private void calculateUserRatingByFilmId(int filmId, int min, int step) throws DaoException {
        List<UserMark> userMarkList = DaoFactory.createDaoManager().getUserMarkDao().findUserMarksByFilm(filmId);

        if(userMarkList.size() >= min) {
            List<UserMark> unaccountedMarks = new ArrayList<>();
            for(UserMark mark : userMarkList) {
                if(!mark.isConsider()) {
                    unaccountedMarks.add(mark);
                }
            }

            if(unaccountedMarks.size() == userMarkList.size() ||
                    userMarkList.size() - unaccountedMarks.size() >= step) {
                calculatePointsForUsers(unaccountedMarks);
            }
        }
    }

    private void calculatePointsForUsers(List<UserMark> userMarks) throws DaoException {
        if(userMarks.size() == 0) {
            return;
        }
        DaoManager daoManager = DaoFactory.createDaoManager();
        UserMarkDao userMarkDao = daoManager.getUserMarkDao();
        UserDao userDao = daoManager.getUserDao();

        float avgMark = getAverageMark(userMarkDao.findUserMarksByFilm(userMarks.get(0).getFilmId()));
        for(UserMark userMark : userMarks) {
            User user = userDao.findById(userMark.getUserId());
            int point = getPointForUser(userMark.getMark(), avgMark);
            int newUserRating = user.getRating() + point;
            if(newUserRating < 0) {
                newUserRating = 0;
            }

            user.setRating(newUserRating);
            userDao.update(user);
            userMark.setConsider(true);
            userMarkDao.update(userMark);
        }
    }

    private int getPointForUser(int userMark, float filmAvgMark) {
        double dif = Math.abs(filmAvgMark - userMark);
        if(dif <= 0.5) {
            return 3;
        } else if(dif <= 1) {
            return 2;
        } else if(dif <= 1.5) {
            return 1;
        } else if(dif <= 2){
            return 0;
        } else if(dif <= 2.5) {
            return -1;
        } else if(dif <= 3) {
            return -2;
        } else {
            return -3;
        }
    }

    private float getAverageMark(List<UserMark> userMarkList) {
        float filmMark = 0;
        for(UserMark userMark : userMarkList) {
            filmMark += userMark.getMark();
        }
        if(userMarkList.size() != 0) {
            filmMark = filmMark / userMarkList.size();
            filmMark = Math.round(filmMark * 1000) / 1000.f;
        }
        return filmMark;
    }
}
