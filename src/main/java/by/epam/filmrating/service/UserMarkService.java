package by.epam.filmrating.service;

import by.epam.filmrating.domain.UserMark;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.exception.ValidationException;
import by.epam.filmrating.model.form.UserMarkForm;

import java.util.List;

/**
 * Created by Yura on 12.06.2016.
 */
public interface UserMarkService {

    void add(UserMarkForm userMarkForm, int minCount, int step) throws ServiceException, ValidationException;
    void update(UserMark userMark) throws ServiceException;
    List<UserMark> getUserMarksByFilmId(int filmId) throws ServiceException;
    boolean isUserAvailableToPutMark(int filmId, int userId) throws ServiceException;
    float getAverageMarkByFilmId(int filmId) throws ServiceException;

}
