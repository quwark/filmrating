package by.epam.filmrating.service;

import by.epam.filmrating.domain.Genre;
import by.epam.filmrating.exception.ServiceException;

import java.util.List;

/**
 * Created by Yura on 11.06.2016.
 */
public interface GenreService {

    List<Genre> getAll() throws ServiceException;
    Genre getById(int genreId) throws ServiceException;
    List<Genre> getGenresByFilmId(int filmId) throws ServiceException;
}
