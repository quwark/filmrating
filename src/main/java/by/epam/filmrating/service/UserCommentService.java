package by.epam.filmrating.service;

import by.epam.filmrating.domain.UserComment;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.exception.ValidationException;
import by.epam.filmrating.model.form.CommentForm;

import java.util.List;

/**
 * Created by Yura on 12.06.2016.
 */
public interface UserCommentService {

    void add(CommentForm commentForm) throws ServiceException, ValidationException;
    void update(UserComment userComment) throws ServiceException;
    void delete(int commentId) throws ServiceException;
    UserComment getById(int commentId) throws ServiceException;
    List<UserComment> getUserCommentsByFilmId(int filmId) throws ServiceException;
    List<UserComment> getUserCommentsByUserId(int userId) throws ServiceException;
}
