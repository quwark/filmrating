package by.epam.filmrating.service;

import by.epam.filmrating.domain.Film;
import by.epam.filmrating.exception.FilmAlreadyExistException;
import by.epam.filmrating.exception.ServiceException;
import by.epam.filmrating.exception.ValidationException;
import by.epam.filmrating.model.form.FilmForm;

import java.util.List;

/**
 * Created by Yura on 11.06.2016.
 */
public interface FilmService {
    void add(FilmForm addFilmForm) throws ServiceException, ValidationException, FilmAlreadyExistException;
    void update(FilmForm filmForm) throws ServiceException, ValidationException, FilmAlreadyExistException;
    void delete(Integer filmId) throws ServiceException;
    List<Film> getAll() throws ServiceException;
    Film getById(int filmId) throws ServiceException;
    List<Film> getFilmsByGenreId(int genreId) throws ServiceException;
    List<Film> getFilmsByPersonId(int personId) throws ServiceException;
}
